package org.heed.opensearch.crawling;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.property.PropertyService;
import org.heed.openapps.scheduling.SchedulingService;
import org.heed.opensearch.crawling.seeding.CrawlSeedingService;
import org.heed.opensearch.crawling.extraction.CrawlExtractionService;
import org.heed.opensearch.crawling.jobs.CrawlingJob;
import org.joda.time.DateTime;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.google.common.base.Stopwatch;


public class ScheduledCrawlingService implements CrawlingService, ApplicationContextAware  {
	private static final Log log = LogFactory.getLog(ScheduledCrawlingService.class);
	private EntityService entityService;
	private PropertyService propertyService;
	private SchedulingService schedulingService;
	
	private SeedGenerator generator;
	private CrawlSeedingService seedService;
	private CrawlExtractionService extractionService;
	
	private long seedingDelay = 15;
	private long crawlingDelay = 15;
	
	public static final int MAX_URLS_PER_DEFINITION = 250;
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		seedService = (CrawlSeedingService)ctx.getBean(CrawlSeedingService.class);		
		extractionService = (CrawlExtractionService)ctx.getBean(CrawlExtractionService.class);
		schedulingService = (SchedulingService)ctx.getBean(SchedulingService.class);
		
		generator = new SeedGenerator("Crawling Seed Generator");
		generator.start();
	}
	public void initialize() {
		if(propertyService.hasProperty("proxyHost") && propertyService.hasProperty("proxyPort")) {
			System.setProperty("socksProxyHost", propertyService.getPropertyValue("proxyHost"));
			System.setProperty("socksProxyPort", propertyService.getPropertyValue("proxyPort"));
		}
		
	}
	public void shutdown() {
		
	}
	
	public void pause(String id) {
		
	}
	public void unpause(String id) {
		
	}
	
	@Override
	public CrawlDefinition getCrawlDefinition(long id) throws Exception {
		Entity entity = entityService.getEntity(id);
		CrawlDefinition definition = new CrawlDefinition(entity);
		return definition;
	}
	@Override
	public List<CrawlDefinition> getCrawlDefinitions(String view, int frequency) throws Exception {
		//if(frequency > 0) delay = frequency * 1000;
		List<CrawlDefinition> definitions = new ArrayList<CrawlDefinition>();
		EntityQuery query = new EntityQuery(CrawlingModel.DEFINITION);
		
		try {
			EntityResultSet entities = entityService.search(query);
			for(Entity entity : entities.getResults()) {
				CrawlDefinition definition = new CrawlDefinition(entity);
								
				if(definition.getCrawlType() != null) {					
					if(definition.getCrawlType().equals("idrange")) {
						if(definition.getEnd() - definition.getStart() < MAX_URLS_PER_DEFINITION) {
							definition.setMessage(null);
							if(!definition.getStatus().equals(CrawlDefinition.STATE_PAUSED)) {
								for(int i=definition.getStart(); i <= definition.getEnd(); i++) {
									Seed seedUrl = seedService.getSeed(definition.getUrl()+i);
									if(seedUrl != null) {
										seedUrl.setDefinition(definition.getId());
										seedUrl.setLastCrawl(definition.getLastCrawl());
										seedUrl.setUser(definition.getUser());
										if(view.equals("all") || (view.equals("hits") && seedUrl.getStatus() != null && seedUrl.getStatus().equals("hit"))) {
											definition.getSeeds().add(seedUrl);
										}
									}
								} 
							}
						} else definition.setMessage("definition count exceeds the maximum "+MAX_URLS_PER_DEFINITION);
					} else if(definition.getCrawlType().equals("standard")) {
						Seed seedUrl = seedService.getSeed(definition.getUrl());
						if(seedUrl != null) {
							seedUrl.setDefinition(definition.getId());
							seedUrl.setLastCrawl(definition.getLastCrawl());
							seedUrl.setUser(definition.getUser());
							if(view.equals("all") || (view.equals("hits") && seedUrl.getStatus() != null && seedUrl.getStatus().equals("hit"))) {
								definition.getSeeds().add(seedUrl);
							}
						} else {
							definition.setStatus(CrawlDefinition.STATE_PAUSED);
							Entity definitionEntity = entityService.getEntity(definition.getId());
							definitionEntity.addProperty(CrawlingModel.STATUS, CrawlDefinition.STATE_PAUSED);							
							definitionEntity.addProperty(CrawlingModel.MESSAGE, "invalid seed url");
							entityService.updateEntity(definitionEntity);
						}
					}
				}
				definitions.add(definition);
			}
			//for(Crawler crawler : crawlers) {
				//System.out.println(crawler.getName()+" is alive:"+crawler.isAlive()+" and is interrupted:"+crawler.isInterrupted());
			//}
		} catch(Exception e) {
			e.printStackTrace();
		}		
		return definitions;
	}
	
	class SeedGenerator extends Thread {
		boolean paused = false;
				
		public SeedGenerator(String name) {
			setName(name);			
		}
		
		public void run() {
	        while(true) {
	        	synchronized(this) {
	        		if(!paused) {
	        			Stopwatch stopWatch = Stopwatch.createStarted();
	        			try {
	        				sleep(1000 * seedingDelay);//n second pause between cycles
	        				long currentTime = System.currentTimeMillis();
	        				List<CrawlDefinition> definitions = getCrawlDefinitions("all", 0);
	        				int count = 0;
	        				for(CrawlDefinition definition : definitions) {
	        					if(definition.getFrequency() != null) {
	        						long seedDelay = getSeedDelay(definition.getFrequency());
        							long timeExpired = currentTime - definition.getLastCrawl();
	        						if(definition.getFrequency().equals("daily") && definition.getTime1() != null && definition.getTime1().length() > 0) {
	        							String[] time = definition.getTime1().split(":");
	        							if(time.length == 2) {
	        								int hours = Integer.parseInt(time[0]);
	        								int minutes = Integer.parseInt(time[1]);
	        								DateTime current = new DateTime();
	        								DateTime fireTime = new DateTime(current.getYear(), current.getMonthOfYear(), current.getDayOfMonth(), hours, minutes, 0, 0);
	        								if(fireTime.isBeforeNow() && (fireTime.isAfter(definition.getLastCrawl()) || fireTime.isEqual(definition.getLastCrawl()))) {
	        									seedDelay = 0;
	        									definition.setStatus(CrawlDefinition.STATE_RUNNING);
	        									log.info("fire time "+fireTime.toString()+" wait for "+(fireTime.getMillis() - current.getMillis())+" ms");
	        								}
	        							}
	        						} 
	        							        							        							
	        						if(timeExpired >= seedDelay && !definition.getStatus().equals(CrawlDefinition.STATE_PAUSED)) {
	        							Entity definitionEntity = entityService.getEntity(definition.getId());
    									definitionEntity.addProperty(CrawlingModel.LAST_CRAWL, currentTime);
    									definition.setLastCrawl(currentTime);
    									
    									if(definition.getFrequency().equals("demand")) {
    										definitionEntity.addProperty(CrawlingModel.STATUS, CrawlDefinition.STATE_PAUSED);
    										definition.setStatus(CrawlDefinition.STATE_PAUSED);
    									}
    									entityService.updateEntity(definitionEntity);
	        							
        								int seedCount = 0;
        								for(Seed seed : definition.getSeeds()) {
        									if(seed.getDocument() == 0 || seed.isUpdate()) {
        								
        										//schedule our job here
        										CrawlingJob job = new CrawlingJob(seed, seedService, extractionService, crawlingDelay);
        										schedulingService.run(job);
        								
        										log.info(getName()+" generated seed for "+seed.getUrl()+" in "+stopWatch);
        										count++;
        										seedCount++;
        									}	
        								}
        								if(seedCount > 0) {
        									log.info("generated "+seedCount+" seeds for : "+definition.getUrl());        									
        								}
        							}	        						
	        					} 
	        				}
	        				
	        				log.info(getName()+" generated "+count+" seeds in "+stopWatch);
	        				stopWatch.stop();
	        			} catch(Exception e) {
	        				e.printStackTrace();
	        			}	        			
	        		}
	        	}
	        }
		}
		public void pause() { this.paused = true; }
	    public void unpause() { this.paused = false; }
	}
	
	protected void recordSeed(Seed seed, HttpResponse response) throws Exception {
		Entity seedEntity = new Entity(CrawlingModel.SEED);
		seedEntity.addProperty(ContentModel.DOCUMENT_URL, seed.getUrl());
		seedEntity.addProperty(CrawlingModel.MESSAGE, response.getStatusLine().getReasonPhrase());
		seedEntity.addProperty(CrawlingModel.STATUS, response.getStatusLine().getStatusCode());
		entityService.addEntity(seedEntity);
	}
	protected long getSeedDelay(String frequency) {
		if(frequency.equals("demand")) return 0;
		if(frequency.equals("daily")) return 1000 * 60 * 60 * 24;
		if(frequency.equals("12hours")) return 1000 * 60 * 60 * 12;
		if(frequency.equals("4hours")) return 1000 * 60 * 60 * 4;
		if(frequency.equals("hourly")) return 1000 * 60 * 60;
		if(frequency.equals("30minutes")) return 1000 * 60 * 30;
		if(frequency.equals("15minutes")) return 1000 * 60 * 15;
		if(frequency.equals("10minutes")) return 1000 * 60 * 10;
		if(frequency.equals("5minutes")) return 1000 * 60 * 5;
		if(frequency.equals("1minute")) return 1000 * 60 * 1;
		if(frequency.equals("30seconds")) return 1000 * 30;
		return 0;
	}
	
	public void setEntityService(EntityService service) {
		this.entityService = service;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	public void setSeedingDelay(long seedingDelay) {
		this.seedingDelay = seedingDelay;
	}
	
}
