package org.heed.opensearch.crawling.http;

import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

import org.heed.opensearch.crawling.HttpComponent;


public class CommonsHttpClientComponent implements HttpComponent {

	
	public HttpResponse fetch(String urlStr, String queryString, Map<String,String> headers) {
		BasicStatusLine statusLine = new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), 200, "OK");
    	HttpResponse response = new BasicHttpResponse(statusLine);
    	
    	GetMethod get = new GetMethod(urlStr);
    	if(headers != null) {
    		for(String key : headers.keySet()) {
    			get.addRequestHeader(key, headers.get(key));
    		}
    	}
    	if(queryString != null) get.setQueryString(queryString);
    	//CrawlURI curi = (queryString.length() > 0) ? toCrawlURI(url+"?"+queryString) : toCrawlURI(url);
    	try {
			HttpClient client = new HttpClient();
			int respCode = client.executeMethod(get);
			if(respCode != 200) {
				Thread.sleep(5000);
				respCode = client.executeMethod(get);
				if(respCode != 200) System.out.println("http error :"+respCode+" - "+urlStr);
			}
			ByteArrayEntity entity = new ByteArrayEntity(get.getResponseBody());
    	    response.setEntity(entity);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			get.releaseConnection();
		}
		response.setStatusCode(get.getStatusCode());
		return response;
	}
}
