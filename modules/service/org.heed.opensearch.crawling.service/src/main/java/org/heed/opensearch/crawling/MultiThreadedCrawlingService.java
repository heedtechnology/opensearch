package org.heed.opensearch.crawling;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.property.PropertyService;
import org.heed.opensearch.crawling.seeding.CrawlSeedingService;
import org.heed.opensearch.crawling.extraction.CrawlExtractionService;
import org.heed.opensearch.crawling.http.URLConnectionComponent;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.google.common.base.Stopwatch;
import com.google.common.io.ByteStreams;


public class MultiThreadedCrawlingService implements CrawlingService, ApplicationContextAware  {
	private static final Log log = LogFactory.getLog(MultiThreadedCrawlingService.class);
	private static final SimpleDateFormat fmt = new SimpleDateFormat("h:mm:ss a");
	private EntityService entityService;
	private PropertyService propertyService;
	
	private HttpComponent http = new URLConnectionComponent();
	private SeedGenerator generator;
	private CrawlSeedingService seedService;
	private CrawlExtractionService extractionService;
				
	private int queueCapacity = 100000;
	private int numCrawlers = 1;
	private long seedingDelay = 15;
	private long crawlingDelay = 15;
	
	private Crawler[] crawlers = new Crawler[numCrawlers];
	private BlockingQueue<Seed> queue = new ArrayBlockingQueue<Seed>(queueCapacity);
	private Map<String, Long> domains = new HashMap<String,Long>();
				
	public static final int MAX_URLS_PER_DEFINITION = 250;
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		seedService = (CrawlSeedingService)ctx.getBean(CrawlSeedingService.class);		
		extractionService = (CrawlExtractionService)ctx.getBean(CrawlExtractionService.class);
		
		generator = new SeedGenerator("Crawling Seed Generator");
		for(int i=0; i < numCrawlers; i++) {
			crawlers[i] = new Crawler("CrawlerThread-"+i);
			crawlers[i].start();
		}
		generator.start();
	}
	public void initialize() {
		if(propertyService.hasProperty("proxyHost") && propertyService.hasProperty("proxyPort")) {
			System.setProperty("socksProxyHost", propertyService.getPropertyValue("proxyHost"));
			System.setProperty("socksProxyPort", propertyService.getPropertyValue("proxyPort"));
		}		
	}
	public void shutdown() {
		
	}
	
	public void pause(String id) {
		
	}
	public void unpause(String id) {
		
	}
	
	@Override
	public CrawlDefinition getCrawlDefinition(long id) throws Exception {
		Entity entity = entityService.getEntity(id);
		CrawlDefinition definition = new CrawlDefinition(entity);
		return definition;
	}
	@Override
	public List<CrawlDefinition> getCrawlDefinitions(String view, int frequency) throws Exception {
		//if(frequency > 0) delay = frequency * 1000;
		List<CrawlDefinition> definitions = new ArrayList<CrawlDefinition>();
		EntityQuery query = new EntityQuery(CrawlingModel.DEFINITION);
		
		try {
			EntityResultSet entities = entityService.search(query);
			for(Entity entity : entities.getResults()) {
				CrawlDefinition definition = new CrawlDefinition(entity);
								
				if(definition.getCrawlType() != null) {					
					if(definition.getCrawlType().equals("idrange")) {
						if(definition.getEnd() - definition.getStart() < MAX_URLS_PER_DEFINITION) {
							definition.setMessage(null);
							if(!definition.getStatus().equals(CrawlDefinition.STATE_PAUSED)) {
								for(int i=definition.getStart(); i <= definition.getEnd(); i++) {
									Seed seedUrl = seedService.getSeed(definition.getUrl()+i);
									if(seedUrl != null) {
										seedUrl.setDefinition(definition.getId());
										seedUrl.setLastCrawl(definition.getLastCrawl());
										if(view.equals("all") || (view.equals("hits") && seedUrl.getStatus() != null && seedUrl.getStatus().equals("hit"))) {
											definition.getSeeds().add(seedUrl);
										}
									}
								} 
							}
						} else definition.setMessage("definition count exceeds the maximum "+MAX_URLS_PER_DEFINITION);
					} else if(definition.getCrawlType().equals("standard")) {
						Seed seedUrl = seedService.getSeed(definition.getUrl());
						if(seedUrl != null) {
							seedUrl.setDefinition(definition.getId());
							seedUrl.setLastCrawl(definition.getLastCrawl());
							if(view.equals("all") || (view.equals("hits") && seedUrl.getStatus() != null && seedUrl.getStatus().equals("hit"))) {
								definition.getSeeds().add(seedUrl);
							}
						}
					}
				}
				definitions.add(definition);
			}
			//for(Crawler crawler : crawlers) {
				//System.out.println(crawler.getName()+" is alive:"+crawler.isAlive()+" and is interrupted:"+crawler.isInterrupted());
			//}
		} catch(Exception e) {
			e.printStackTrace();
		}		
		return definitions;
	}
	
	class SeedGenerator extends Thread {
		boolean paused = false;
				
		public SeedGenerator(String name) {
			setName(name);			
		}
		
		public void run() {
	        while(true) {
	        	synchronized(this) {
	        		if(!paused) {
	        			Stopwatch stopWatch = Stopwatch.createStarted();
	        			try {
	        				sleep(1000 * seedingDelay);//n second pause between cycles
	        				long currentTime = System.currentTimeMillis();
	        				List<CrawlDefinition> definitions = getCrawlDefinitions("all", 0);
	        				int count = 0;
	        				for(CrawlDefinition definition : definitions) {
	        					long seedDelay = getSeedDelay(definition.getFrequency());
	        					long timeExpired = currentTime - definition.getLastCrawl();
	        					if(timeExpired >= seedDelay && !definition.getStatus().equals(CrawlDefinition.STATE_PAUSED)) {
	        						int seedCount = 0;
	        						for(Seed seed : definition.getSeeds()) {
	        							if(!queue.contains(seed) && (seed.getDocument() == 0 || seed.isUpdate())) {
	        								queue.add(seed);	        							
		        		        			log.info(getName()+" generated seed for "+seed.getUrl()+" in "+stopWatch);
		        		        			count++;
		        		        			seedCount++;
	        							}	
	        						}
	        						if(seedCount > 0) {
	        							log.info("generated "+seedCount+" seeds for : "+definition.getUrl());
	        							Entity definitionEntity = entityService.getEntity(definition.getId());
	        							definitionEntity.addProperty(CrawlingModel.LAST_CRAWL, currentTime);
	        						
	        							if(definition.getFrequency().equals("demand")) {
	        								definitionEntity.addProperty(CrawlingModel.STATUS, CrawlDefinition.STATE_PAUSED);
	        								definition.setStatus(CrawlDefinition.STATE_PAUSED);
	        							}

	        							entityService.updateEntity(definitionEntity);
	        						}						
	        					} 
	        				}
	        				//Report on crawler threads state
	        				StringWriter writer = new StringWriter();
	        				for(Crawler thread : crawlers) {
	        					writer.append(thread.getName()+":"+thread.getState().name()+",");
	        				}
	        				log.info(getName()+" generated "+count+" seeds in "+stopWatch+" "+writer.toString());
	        				stopWatch.stop();
	        			} catch(Exception e) {
	        				e.printStackTrace();
	        			}	        			
	        		}
	        	}
	        }
		}
		public void pause() { this.paused = true; }
	    public void unpause() { this.paused = false; }
	}
	class Crawler extends Thread {
		private boolean paused = false;
	    
		public Crawler(String name) {
			setName(name);
		}
	    public void run() {
	        while(true) {
	        	synchronized(this) {
	        		if(!paused) {
	        			Seed seed = null;
	        			long currentTime = System.currentTimeMillis();
	        			
	        			try {
	        				seed = queue.take();
	        				Long lastDomainRequest = domains.get(seed.getDomain());
	        				if(lastDomainRequest != null) {
	        					if(currentTime - lastDomainRequest < crawlingDelay * 1000) {
	        						queue.put(seed);
	        						seed = null;
	        					}
	        				}
	        				//sleep(seed.getDelay());//n second pause between cycles
	        			} catch(InterruptedException e) {
	        				e.printStackTrace();
	        			}
	        			if(seed != null && (seed.getDocument() == 0 || seed.isUpdate())) {
	        				domains.put(seed.getDomain(), currentTime);
	        				Stopwatch stopWatch = Stopwatch.createStarted();
	        				try {	 
	        					log.info("http fetch "+seed.getUrl());
	        					HttpResponse response = null;
	        					try {	        						
	        						if(seed.getUrl().contains("?")) {
	        							String[] parts = seed.getUrl().split("\\?");
	        							response = http.fetch(parts[0], parts[1], null);
	        						} else {
	        							response = http.fetch(seed.getUrl(), "", null);
	        						}
	        					} catch(Exception e) {
	        						log.error("problem fetching : "+seed.getUrl());
	        						recordSeed(seed, response);
	        					}
	        					if(response != null && response.getEntity() != null) {
	        						byte[] content = ByteStreams.toByteArray(response.getEntity().getContent());
	        							        						  
	        						List<Seed> seeds = seedService.process(seed, content);
	        						for(Seed u : seeds) {
	        							u.setDefinition(seed.getDefinition());
	        							queue.add(u);
	        						}
		        				
	        						extractionService.process(seed, content);    				
		        				        						        					
	        					} else {
	        						//seed.setStatus(String.valueOf(status.getStatusCode()));
	        						//if(status.getReasonPhrase() != null) seed.setTitle(status.getReasonPhrase());
	        						//else seed.setTitle("address not found, please check your url");
	        					}
	        					        					
	        				} catch(Exception e) {
	        					//seed.setMessage("404");
	        					seed.setLastCrawl(currentTime);
	        					seed.setMessage(e.getMessage());
	        					e.printStackTrace();
	        				}
	        				seed.setLastCrawl(currentTime);
	        				seed.setMessage(fmt.format(new Date(currentTime)));
	        				stopWatch.stop();
	        				log.info("processed "+seed.getUrl()+" in "+stopWatch+" "+queue.size()+" in queue...");
	        			}
	        		}
	        	}
	        }
	    }
	    public void pause() { this.paused = true; }
	    public void unpause() { this.paused = false; }
	}
	protected void recordSeed(Seed seed, HttpResponse response) throws Exception {
		Entity seedEntity = new Entity(CrawlingModel.SEED);
		seedEntity.addProperty(ContentModel.DOCUMENT_URL, seed.getUrl());
		seedEntity.addProperty(CrawlingModel.MESSAGE, response.getStatusLine().getReasonPhrase());
		seedEntity.addProperty(CrawlingModel.STATUS, response.getStatusLine().getStatusCode());
		entityService.addEntity(seedEntity);
	}
	protected long getSeedDelay(String frequency) {
		if(frequency.equals("demand")) return 0;
		if(frequency.equals("daily")) return 1000 * 60 * 60 * 24;
		if(frequency.equals("12hours")) return 1000 * 60 * 60 * 12;
		if(frequency.equals("4hours")) return 1000 * 60 * 60 * 4;
		if(frequency.equals("hourly")) return 1000 * 60 * 60;
		if(frequency.equals("30minutes")) return 1000 * 60 * 30;
		if(frequency.equals("15minutes")) return 1000 * 60 * 15;
		if(frequency.equals("10minutes")) return 1000 * 60 * 10;
		if(frequency.equals("5minutes")) return 1000 * 60 * 5;
		if(frequency.equals("1minute")) return 1000 * 60 * 1;
		if(frequency.equals("30seconds")) return 1000 * 30;
		return 0;
	}
	
	public void setEntityService(EntityService service) {
		this.entityService = service;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	public void setSeedingDelay(long seedingDelay) {
		this.seedingDelay = seedingDelay;
	}
	public void setNumCrawlers(int numCrawlers) {
		this.numCrawlers = numCrawlers;
	}
	public void setQueueCapacity(int queueCapacity) {
		this.queueCapacity = queueCapacity;
	}
	
}
