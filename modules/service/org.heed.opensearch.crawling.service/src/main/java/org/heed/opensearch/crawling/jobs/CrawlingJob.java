package org.heed.opensearch.crawling.jobs;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.heed.openapps.scheduling.ExecutionContext;
import org.heed.openapps.scheduling.JobSupport;
import org.heed.opensearch.crawling.HttpComponent;
import org.heed.opensearch.crawling.Seed;
import org.heed.opensearch.crawling.extraction.CrawlExtractionService;
import org.heed.opensearch.crawling.http.URLConnectionComponent;
import org.heed.opensearch.crawling.seeding.CrawlSeedingService;

import com.google.common.base.Stopwatch;
import com.google.common.io.ByteStreams;


public class CrawlingJob extends JobSupport {
	private static final long serialVersionUID = -5165116087963939873L;
	private static final Log log = LogFactory.getLog(CrawlingJob.class);
	private static final SimpleDateFormat fmt = new SimpleDateFormat("h:mm:ss a");
	private HttpComponent http = new URLConnectionComponent();
	
	private Seed rootSeed;
	private CrawlSeedingService seedService;
	private CrawlExtractionService extractionService;
	
	private int queueCapacity = 100000;
	private long crawlingDelay = 15;
	private int numCrawlers = 1;
	
	private Crawler[] crawlers = new Crawler[numCrawlers];
	private BlockingQueue<Seed> queue = new ArrayBlockingQueue<Seed>(queueCapacity);
	private Map<String, Long> domains = new HashMap<String,Long>();
	
	
	public CrawlingJob(Seed seed, CrawlSeedingService seedService, CrawlExtractionService extractionService, long crawlingDelay) {
		this.rootSeed = seed;
		this.seedService = seedService;
		this.extractionService = extractionService;
		this.crawlingDelay = crawlingDelay;
		
		for(int i=0; i < numCrawlers; i++) {
			crawlers[i] = new Crawler("CrawlerThread-"+i);
			crawlers[i].start();
		}
	}
	
	@Override
	public void execute(ExecutionContext context) {
		super.execute(context);
		long currentTime = System.currentTimeMillis();
		
		rootSeed.setLastCrawl(currentTime);
		Stopwatch stopWatch = Stopwatch.createStarted();
		
		queue.add(rootSeed);
		
		while(!queue.isEmpty()) {
			setLastMessage("processing crawl job : "+queue.size()+" seeds in queue");
		}
		
		rootSeed.setMessage(fmt.format(new Date(currentTime)));
		stopWatch.stop();
		log.info("crawled "+rootSeed.getUrl()+" in "+stopWatch);
		
	}
	
	class Crawler extends Thread {
		private boolean paused = false;
	    
		public Crawler(String name) {
			setName(name);
		}
	    public void run() {
	        while(true) {
	        	synchronized(this) {
	        		if(!paused) {
	        			Seed seed = null;
	        			long currentTime = System.currentTimeMillis();
	        			
	        			try {
	        				seed = queue.take();
	        				Long lastDomainRequest = domains.get(seed.getDomain());
	        				if(lastDomainRequest != null) {
	        					if(currentTime - lastDomainRequest < crawlingDelay * 1000) {
	        						queue.put(seed);
	        						seed = null;
	        					}
	        				}
	        				//sleep(seed.getDelay());//n second pause between cycles
	        			} catch(InterruptedException e) {
	        				e.printStackTrace();
	        			}
	        			Stopwatch stopWatch = Stopwatch.createStarted();
	        			if(seed != null && (seed.getDocument() == 0 || seed.isUpdate())) {
	        				domains.put(seed.getDomain(), currentTime);
	        				seed.setUser(rootSeed.getUser());
	        				try {	 
	        					log.info("http fetch "+seed.getUrl());
	        					HttpResponse response = null;
	        					try {	        						
	        						if(seed.getUrl().contains("?")) {
	        							String[] parts = seed.getUrl().split("\\?");
	        							response = http.fetch(parts[0], parts[1], null);
	        						} else {
	        							response = http.fetch(seed.getUrl(), "", null);
	        						}
	        					} catch(Exception e) {
	        						log.error("problem fetching : "+seed.getUrl());
	        						//recordSeed(seed, response);
	        					}
	        					if(response != null && response.getEntity() != null) {
	        						byte[] content = ByteStreams.toByteArray(response.getEntity().getContent());
	        							        						  
	        						List<Seed> seeds = seedService.process(seed, content);
	        						for(Seed u : seeds) {
	        							u.setDefinition(seed.getDefinition());
	        							queue.add(u);
	        						}
		        				
	        						extractionService.process(seed, content);    				
		        				        						        					
	        					} else {
	        						//seed.setStatus(String.valueOf(status.getStatusCode()));
	        						//if(status.getReasonPhrase() != null) seed.setTitle(status.getReasonPhrase());
	        						//else seed.setTitle("address not found, please check your url");
	        					}
	        					        					
	        				} catch(Exception e) {
	        					//seed.setMessage("404");
	        					seed.setLastCrawl(currentTime);
	        					seed.setMessage(e.getMessage());
	        					e.printStackTrace();
	        				}
	        				seed.setLastCrawl(currentTime);
	        				seed.setMessage(fmt.format(new Date(currentTime)));
	        				stopWatch.stop();
	        				log.info("processed "+seed.getUrl()+" in "+stopWatch+" "+queue.size()+" in queue...");
	        			} else if(seed != null) {
	        				stopWatch.stop();
	        				log.info("skipping processing "+seed.getUrl()+" in "+stopWatch+" "+queue.size()+" in queue...");
	        			}
	        		}
	        	}
	        }
	    }
	    public void pause() { this.paused = true; }
	    public void unpause() { this.paused = false; }
	}
}
