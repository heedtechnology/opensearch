package org.heed.opensearch.crawling.http;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.heed.opensearch.crawling.HttpComponent;


public class URLConnectionComponent implements HttpComponent {

	
	public HttpResponse fetch(String urlStr, String queryString, Map<String,String> headers) {
		BasicStatusLine statusLine = new BasicStatusLine(new ProtocolVersion("HTTP", 1, 1), 200, "OK");
    	HttpResponse response = new BasicHttpResponse(statusLine);
    	try {
    		String value = (queryString != null && queryString.length() > 0) ? urlStr+"?"+queryString : urlStr;
    		URL url = new URL(value);
    		URLConnection connection = url.openConnection();
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[16384];
			while ((nRead = connection.getInputStream().read(data, 0, data.length)) != -1) {
			  buffer.write(data, 0, nRead);
			}
			buffer.flush();
			ByteArrayEntity entity = new ByteArrayEntity(buffer.toByteArray());
    	    response.setEntity(entity);
    	    if(connection instanceof HttpURLConnection) {
        		HttpURLConnection httpConnection = (HttpURLConnection) connection;
        		int code = httpConnection.getResponseCode();
        		response.setStatusCode(code);
        	}
		} catch(Exception e) {
			response.setStatusCode(404);
			if(e.getMessage().equals("SOCKS server general failure")) response.setReasonPhrase("General server connection failure, please check the url.");
			e.printStackTrace();
		}
		return response;
	}
}
