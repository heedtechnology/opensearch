package org.heed.opensearch.search.parse;

import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.Parsers;
import org.codehaus.jparsec.Scanners;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.pattern.CharPredicate;
import org.heed.openapps.util.NumberUtility;
import org.heed.opensearch.search.Definition;
import org.heed.opensearch.search.Dictionary;
import org.heed.opensearch.search.Token;
import org.heed.opensearch.search.dictionary.AttributeDefinition;
import org.heed.opensearch.search.dictionary.LexNode;
import org.heed.opensearch.search.dictionary.NumericDefinition;
import org.heed.opensearch.search.dictionary.NumericLexNode;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * User: Babar
 * Date: 10/23/13
 * Time: 5:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class DefinitionParser {

    private final Dictionary dictionary;

    private final LexNode root = new LexNode();

    private TreeMap<String,Definition> textToDefinitionMap = new TreeMap<String,Definition>();

    private TreeSet<Integer> definitionLengths = new TreeSet<Integer>();

    public DefinitionParser(Dictionary dictionary) throws Exception {
        this.dictionary = dictionary;
        for (Definition def : dictionary.getDefinitions()){
                loadDefinition(def);
                if(!definitionLengths.contains(def.getName().length()))
                    definitionLengths.add(def.getName().length());

                textToDefinitionMap.put(def.getName(),def);
        }
    }

    private void loadDefinition(Definition def) throws Exception {
        LexNode node = root;
        for(int i=0; i < def.getName().length(); i++) {
            char ch = def.getName().charAt(i);
            LexNode n = node.getChild(ch);
            if(n != null) node = n;
            else {
                if(def.getName().charAt(i) == '*') {
                    if(def instanceof AttributeDefinition) {
                        if(((AttributeDefinition)def).isNumeric())
                            n = new NumericLexNode(node);
                    }
                } else n = new LexNode(def.getName().charAt(i), node, def.isCaseSensitive());
                if(n != null) node = n;
            }
            if(i == def.getName().length()-1) {
                n.addDefinition(def);
                if(NumberUtility.isInteger(def.getName()) && def.getType() != Token.YEAR)
                    n.addDefinition(new NumericDefinition(def.getName()));
            }
        }
    }

    public Map<String,Object> getWordMap() {
        return new Map<String, Object>() {
            @Override
            public Object map(String s) {
                return textToDefinitionMap.get(s);
            }
        };
    }

    public Parser<String> getBackingParser(){
        return Parsers.or(new DefinitionParsers(definitionLengths, root)).source();
    }

    private static Parser<LexNode> definitionParser(final LexNode root, final int definitionLength) {

       return definitionParser(root, 0, definitionLength);
    }

    private static Parser<LexNode> definitionParser(final LexNode node, int currentIndex, int definitionLength){
        return Scanners.isChar(new IsDefinitionCharacter(node)).source()
                    .map(new DefinitionCharacterLexNodeMap(node))
                    .next(new LexNodeParserMap(currentIndex, definitionLength));
    }

    private static class DefinitionCharacterLexNodeMap implements Map<String, LexNode> {
        private final LexNode node;

        public DefinitionCharacterLexNodeMap(LexNode node) {
            this.node = node;
        }

        @Override
        public LexNode map(String s) {
            return node.getChild(s.charAt(0));
        }
    }

    private static class LexNodeParserMap implements Map<LexNode, Parser<LexNode>> {
        private final int currentIndex;
        private final int definitionLength;

        public LexNodeParserMap(int currentIndex, int definitionLength) {
            this.currentIndex = currentIndex;
            this.definitionLength = definitionLength;
        }

        @Override
        public Parser<LexNode> map(final LexNode node) {
            if(node.hasDefinition() && currentIndex == definitionLength -1)
                return Parsers.always().retn(node);
            return definitionParser(node, currentIndex + 1, definitionLength);
        }
    }

    private static class IsDefinitionCharacter implements CharPredicate {
        private final LexNode node;

        public IsDefinitionCharacter(LexNode node) {
            this.node = node;
        }

        @Override
        public boolean isChar(char c) {
            return node.childExists(c);
        }
    }

    private static class DefinitionParsers implements Iterable<Parser<LexNode>> {
        private final TreeSet definitionLengths;
        private final LexNode root;

        private DefinitionParsers(TreeSet definitionLengths, LexNode root) {
            this.definitionLengths = definitionLengths;
            this.root = root;
        }

        @Override
        public Iterator<Parser<LexNode>> iterator() {
            return new Iterator<Parser<LexNode>>() {

                Iterator<Integer> definitionSizeIterator = definitionLengths.descendingIterator();

                public boolean hasNext() {
                    return definitionSizeIterator.hasNext();
                }

                @Override
                public Parser<LexNode> next() {
                    return definitionParser(root, definitionSizeIterator.next());
                }

                @Override
                public void remove() {}
            };
        }

        ;
    }

    private class TokenDefinitionTokenMap implements Map<org.codehaus.jparsec.Token, org.codehaus.jparsec.Token> {
        @Override
        public org.codehaus.jparsec.Token map(org.codehaus.jparsec.Token token) {
            return new org.codehaus.jparsec.Token(token.index(),token.length(),textToDefinitionMap.get(token.value().toString()));
        }
    }
//
//
//    private Parser<Void> seperatorScanner() {
//        return whitespacesScanner().or(Scanners.string("//"));
//    }
//
//    private Parser<Void> whitespacesScanner() {
//        return Scanners.WHITESPACES;
//    }
//
//    private Parser<Token> operatorTokenizer() {
//        return Terminals.operators("[", "]", "(", ")", ":").tokenizer().map(new Map<String,Token>() {
//
//            @Override
//            public Token map(String s) {
//                switch (s){
//                    case "[" : return new Token(TokenTypes.LeftBracket);
//                    case "]" : return new Token(TokenTypes.RightBracket);
//                    case "(" : return new Token(TokenTypes.LeftParentheses);
//                    case ")" : return new Token(TokenTypes.RightParentheses);
//                    case ":"  : return new Token(TokenTypes.Colon);
//                    default: throw new AssertionError(s);
//                }
//
//            }
//        });
//    }
//
//    private Parser<Token> categoryTokenizer() {
//        return Terminals.("[","]","(",")",":").tokenizer().map(new Map<String,Token>() {
//
//            @Override
//            public Token map(String s) {
//                switch (s){
//                    case "[" : return new Token(TokenTypes.LeftBracket);
//                    case "]" : return new Token(TokenTypes.RightBracket);
//                    case "(" : return new Token(TokenTypes.LeftParentheses);
//                    case ")" : return new Token(TokenTypes.RightParentheses);
//                    case ":"  : return new Token(TokenTypes.Colon);
//                    default: throw new AssertionError(s);
//                }
//
//            }
//        });
//    }
//
//    private Parser<List<Token>> queryTokeninzer(){
//
//        return  queryTermTokeninzer().sepBy1(
//                Parsers.or(
//                        whitespacesScanner().many(),
//                        Scanners.string("//")));
//    }
//
//    private Parser<Token> queryTermTokeninzer(){
//
//        return Parsers.or(
//                subQueryTokeninzer(),
//                queryConditionTokenizer(),
//                getBackingParser());
//    }
//
//    private Parser<Token> queryConditionTokenizer() {
//
//        Parser<?> conditionRightSide = Parsers.or(
//                Parsers.between(Scanners.isChar('['),Scanners.ANY_CHAR.source().many1(),Scanners.isChar(']')),
//                Scanners.WHITESPACES.not().many1().source());
//
//        return Parsers.or(
//                Scanners.string("source_assoc:").followedBy(conditionRightSide).source().map(new Map<String, Token>() {
//                    @Override
//                    public Token map(String s) {
//                        return new Token("source_assoc",s,Token.ATTR);
//                    }
//                }),
//                Scanners.string("name:").followedBy(conditionRightSide).source().map(new Map<String, Token>() {
//                    @Override
//                    public Token map(String s) {
//                        return  new Token("name",s,Token.NAME);
//                    }
//                }),
//                Scanners.string("subj:").followedBy(conditionRightSide).source().map(new Map<String, Token>() {
//                    @Override
//                    public Token map(String s) {
//                        return new Token("subj",s,Token.SUBJ);
//                    }
//                }),
//                Scanners.string("path:").followedBy(conditionRightSide).source().map(new Map<String, Token>() {
//                    @Override
//                    public Token map(String s) {
//                        return new Token("path",s,Token.PATH);
//                    }
//                }),
//                Scanners.string("id:").followedBy(conditionRightSide).source().map(new Map<String, Token>() {
//                    @Override
//                    public Token map(String s) {
//                        return new Token("id",s,Token.ID);
//                    }
//                }),
//                Scanners.string("parent_id:").followedBy(conditionRightSide).source().map(new Map<String, Token>() {
//                    @Override
//                    public Token map(String s) {
//                        return new Token("parent_id",s,Token.ID);
//                    }
//                }));
//    }
//
//    private Parser<Token> subQueryTokeninzer(){
//
//        return Parsers.between(
//                Scanners.isChar('(').followedBy(Scanners.WHITESPACES.many()),
//                queryTokeninzer().map(new Map<List<Token>, Token>() {
//                    @Override
//                    public Token map(List<Token> tokens) {
//                        Token token = new Token();
//                        token.setSubTokens(tokens);
//                        token.setType(Token.MULT);
//                        return token;
//                    }
//                }),
//                Scanners.WHITESPACES.many1().followedBy(Scanners.isChar(')')));
//    }
}
