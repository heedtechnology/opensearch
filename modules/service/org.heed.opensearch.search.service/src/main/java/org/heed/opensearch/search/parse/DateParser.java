package org.heed.opensearch.search.parse;

import org.codehaus.jparsec.*;
import org.codehaus.jparsec.Token;
import org.codehaus.jparsec.functors.*;
import org.codehaus.jparsec.functors.Map;
import org.heed.opensearch.search.parse.TryResult;
import org.heed.opensearch.search.parse.ValueGetter;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by babar on 1/26/14.
 */
public class DateParser {

    public static final String[] monthsArray = new String[]{"january", "february", "march", "april",
            "may", "june", "july", "august", "september", "october", "november", "december"};

    static final String[] months = addArrays(monthsArray, monthAbbreviation(monthsArray));

    static final String dateFieldName = "date";

    static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyy");

    static final String[] monthDaySuffixArray = new String[]{"st", "nd", "rd", "th"};

    static final String[] weekdays = new String[]{"monday", "tuesday", "wednesday",
            "thursday", "friday", "saturday", "sunday"};
    static final String[] emptyStringArray = new String[0];

    static final String[] dateQueryKeywords = new String[]{"today", "yesterday", "days", "week", "weeks", "month", "months", "year", "years",
            "ago", "last", "next", "before", "after", "prior", "to", "between", "and"};

    static Parser<Date> dateParser(Terminals terminals) {

        Parser<org.codehaus.jparsec.Token> dateDelimiter = Parsers.or(terminals.token(","),terminals.token(":"));

        final Parser<DateBuilder> monthParser = terminals.token(months).map(new Map<org.codehaus.jparsec.Token, DateBuilder>() {
            @Override
            public DateBuilder map(org.codehaus.jparsec.Token token) {
                Tokens.Fragment fragment = (Tokens.Fragment) token.value();

                SimpleDateFormat monthParser = new SimpleDateFormat("MMM");

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(monthParser.parse(fragment.text(), new ParsePosition(0)));

                return new DateBuilder(null, calendar.get(Calendar.MONTH), null);
            }
        });

        final Parser<DateBuilder> weekdayParser = terminals.token(weekdays).map(new Map<org.codehaus.jparsec.Token, DateBuilder>() {
            @Override
            public DateBuilder map(org.codehaus.jparsec.Token token) {
                Tokens.Fragment fragment = (Tokens.Fragment) token.value();

                Calendar parsedCalendar = Calendar.getInstance();
                SimpleDateFormat dayParser = new SimpleDateFormat("EEE");

                parsedCalendar.setTime(dayParser.parse(fragment.text(), new ParsePosition(0)));

                return new DateBuilder(new DayField(true, parsedCalendar.get(Calendar.DAY_OF_WEEK)), null, null);
            }
        });

        final Parser<DateBuilder> ordinalMonthDayParser = Parsers.sequence(Terminals.IntegerLiteral.PARSER,
                terminals.token(monthDaySuffixArray), new Map2<String, org.codehaus.jparsec.Token, DateBuilder>() {
            @Override
            public DateBuilder map(String numberText, org.codehaus.jparsec.Token token2) {

                final int number = Integer.parseInt(numberText);

                if(number > 0 && number <=31 && ((Tokens.Fragment) token2.value()).text().equals(monthSuffix(number)))
                    return new DateBuilder(new DayField(false,number), null, null);

                return null;
            }
        });

        final Parser<DateBuilder> monthDayParser = Parsers.or(
                ordinalMonthDayParser,
                Terminals.IntegerLiteral.PARSER.map(new Map<String, DateBuilder>() {
                    @Override
                    public DateBuilder map(String numberText) {

                        final int number = Integer.parseInt(numberText);

                        if (number > 0 && number <= 31)
                            return new DateBuilder(new DayField(false, number), null, null);

                        return null;
                    }
                }));

        final Parser<DateBuilder> yearParser = Terminals.IntegerLiteral.PARSER.map(new Map<String, DateBuilder>() {
            @Override
            public DateBuilder map(String year) {

                int yearNumber = Integer.parseInt(year);

                if(yearNumber < 1000)
                    yearNumber +=2000;

                return new DateBuilder(null, null, yearNumber);
            }
        });

        return Parsers.or(
                multiFormatDateParser(monthParser, monthDayParser, yearParser, dateDelimiter),
                multiFormatDateParser(monthParser, monthDayParser, dateDelimiter),
                multiFormatDateParser(monthParser, yearParser, dateDelimiter),
                dateIntervalParser(monthParser), dateIntervalParser(weekdayParser),
                dateIntervalParser(ordinalMonthDayParser), dateIntervalParser(yearParser));

    }

    static Parser<Iterable<Date>> numericDateParser(Terminals terminals) {

        Parser<org.codehaus.jparsec.Token> numericDateDelimiter = Parsers.or(terminals.token(","), terminals.token("\\"), terminals.token("/"),
                terminals.token("-"), terminals.token(":"));

        return Parsers.sequence(
                Terminals.IntegerLiteral.PARSER.followedBy(numericDateDelimiter.optional()),
                Terminals.IntegerLiteral.PARSER.followedBy(numericDateDelimiter.optional()),
                Terminals.IntegerLiteral.PARSER, new Map3<String, String, String, Iterable<Date>>() {
            @Override
            public Iterable<Date> map(String s, String s1, String s2) {

                List<Integer> numbers = Arrays.asList(
                        Integer.parseInt(s), Integer.parseInt(s1), Integer.parseInt(s2));

                final Predicate<Integer> isMonthPredicate = new Predicate<Integer>() {
                    @Override
                    public boolean apply(Integer value) {
                        return value <= 12;
                    }
                };

                final Predicate<Integer> isMonthDayPredicate = new Predicate<Integer>() {
                    @Override
                    public boolean apply(Integer value) {
                        return value <= 31;
                    }
                };

                Set<Date> dateSet = new HashSet<Date>();

                for (int month : find(numbers, isMonthPredicate))
                    for (int day : find(remove(numbers, month, null), isMonthDayPredicate))
                        for (int year : remove(numbers, month, day))
                            dateSet.add(makeDate(day, month, year));

                return dateSet;
            }
        });
    }

    private static List<Integer> find(List<Integer> values, Predicate<Integer> predicate) {

        List<Integer> foundValues = new ArrayList<Integer>();

        for (int val : values)
            if (predicate.apply(val)) foundValues.add(val);

        return foundValues;
    }

    private static List<Integer> remove(List<Integer> values, Integer value, Integer value1) {

        List<Integer> newList = new ArrayList<Integer>();

        int index = values.indexOf(value);
        int index1 = values.indexOf(value1);

        for (int i = 0; i < values.size(); i++)
            if (i != index && i != index1) newList.add(values.get(i));

        return newList;
    }

    private static Date makeDate(int day, int month, int year) {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month-1);

        return calendar.getTime();
    }

    private static Parser<Date> multiFormatDateParser(Parser<DateBuilder> monthParser, Parser<DateBuilder> dayParser,
                                                      Parser<DateBuilder> yearParser, Parser<org.codehaus.jparsec.Token> dateDelimiter) {

        @SuppressWarnings("unchecked")
        List<Parser<DateBuilder>> dateIntervalParsers = Arrays.asList(
                monthParser, dayParser, yearParser);

        LinkedList<Parser<Tuple3<DateBuilder, DateBuilder, DateBuilder>>> multiFormatDateParsers =
                new LinkedList<Parser<Tuple3<DateBuilder, DateBuilder, DateBuilder>>>();

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    if (i != j && i != k && j != k)
                        multiFormatDateParsers.add(Parsers.tuple(
                                dateIntervalParsers.get(i).followedBy(dateDelimiter.optional()),
                                dateIntervalParsers.get(j).followedBy(dateDelimiter.optional()),
                                dateIntervalParsers.get(k)));


        return Parsers.or(multiFormatDateParsers).map(new Map<Tuple3<DateBuilder, DateBuilder, DateBuilder>, Date>() {
            @Override
            public Date map(Tuple3<DateBuilder, DateBuilder, DateBuilder> dateBuilderTriple) {

                Calendar calendar = Calendar.getInstance();

                filterNulls(Arrays.asList(dateBuilderTriple.a.getDayField(),
                        dateBuilderTriple.b.getDayField(), dateBuilderTriple.c.getDayField())).setFieldOnCalendar(calendar);

                calendar.set(Calendar.MONTH, filterNulls(new Integer[]{dateBuilderTriple.a.getMonth(),
                        dateBuilderTriple.b.getMonth(), dateBuilderTriple.c.getMonth()
                }));

                calendar.set(Calendar.YEAR, filterNulls(new Integer[]{dateBuilderTriple.a.getYear(),
                        dateBuilderTriple.b.getYear(), dateBuilderTriple.c.getYear()
                }));

                return calendar.getTime();
            }
        });
    }

    private static Parser<Date> multiFormatDateParser(Parser<DateBuilder> parser, Parser<DateBuilder> parser1,
                                                      Parser<Token> dateDelimiter) {

        @SuppressWarnings("unchecked")
        List<Parser<DateBuilder>> dateIntervalParsers = Arrays.asList(
                parser, parser1);

        LinkedList<Parser<Pair<DateBuilder, DateBuilder>>> multiFormatDateParsers =
                new LinkedList<Parser<Pair<DateBuilder, DateBuilder>>>();

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                if (i != j)
                    multiFormatDateParsers.add(Parsers.pair(
                            dateIntervalParsers.get(i).followedBy(dateDelimiter.optional()),
                            dateIntervalParsers.get(j)));


        return Parsers.or(multiFormatDateParsers).map(new Map<Pair<DateBuilder, DateBuilder>, Date>() {
            @Override
            public Date map(Pair<DateBuilder, DateBuilder> dateBuilderPair) {

                final Calendar calendar = Calendar.getInstance();

                tryExtractingNonNullValueFromList(Arrays.asList(dateBuilderPair.a.getDayField(),dateBuilderPair.b.getDayField()))
                        .useValueIfAny(new ValueGetter<DateField>() {
                            @Override
                            public void getValue(DateField value) {
                                value.setFieldOnCalendar(calendar);
                            }
                        });

                tryExtractIntegerFromArray(new Integer[]{dateBuilderPair.a.getMonth(),
                        dateBuilderPair.b.getMonth()
                }).useValueIfAny(new ValueGetter<Integer>() {
                    @Override
                    public void getValue(Integer value) {
                        calendar.set(Calendar.MONTH, value);
                    }
                });

                tryExtractIntegerFromArray(new Integer[]{dateBuilderPair.a.getYear(),
                        dateBuilderPair.b.getYear()
                }).useValueIfAny(new ValueGetter<Integer>() {
                    @Override
                    public void getValue(Integer value) {
                        calendar.set(Calendar.YEAR, value);
                    }
                });

                return calendar.getTime();
            }
        });
    }

    private static Parser<Date> dateIntervalParser(Parser<DateBuilder> parser) {

        return parser.map(new Map<DateBuilder, Date>() {
            @Override
            public Date map(DateBuilder dateBuilder) {

                final Calendar calendar = Calendar.getInstance();

                DateField dayField = dateBuilder.getDayField();
                Integer month = dateBuilder.getMonth();
                Integer year = dateBuilder.getYear();

                if (dayField == null && month == null && year == null)
                    return null;

                if (dayField != null)
                    dayField.setFieldOnCalendar(calendar);

                if (month != null)
                    calendar.set(Calendar.MONTH, month);

                if (year != null)
                    calendar.set(Calendar.YEAR, year);

                return calendar.getTime();
            }
        });
    }

    private static TryResult<Integer> tryExtractIntegerFromArray(Integer[] integers) {
        Integer val = null;

        for (Integer i : integers)
            if (i != null) {
                assert (val == null);
                val = i;
            }

        return new TryResult<Integer>(val);
    }

    private static <E> TryResult<E> tryExtractingNonNullValueFromList(List<E> values) {
        E val = null;

        for (E i : values)
            if (i != null) {
                assert (val == null);
                val = i;
            }

        return new TryResult<E>(val);
    }

    private static int filterNulls(Integer[] integers) {

        final TryResult<Integer> tryResult = tryExtractIntegerFromArray(integers);

        final Integer val = tryResult.getValueOrElse((Integer) null);
        assert (val != null);
        return val;
    }

    private static <E> E filterNulls(List<E> objects) {

        for (E object : objects)
            if(object != null)
                return object;

        return null;
    }

    private static String[] monthAbbreviation(String[] monthsArray) {
        String[] abbreviations = new String[monthsArray.length];

        for (int i = 0; i < monthsArray.length; i++)
            abbreviations[i] = monthsArray[i].substring(0, 3);

        return abbreviations;
    }

    static String[] addArrays(String[]... stringArrays) {

        int length = 0;

        for (String[] stringArray : stringArrays)
            length += stringArray.length;

        String[] result = new String[length];

        length = 0;

        for (String[] stringArray : stringArrays) {
            System.arraycopy(stringArray, 0, result, length, stringArray.length);
            length += stringArray.length;
        }

        return result;
    }

    public static String monthSuffix(int i) {

        final String[] suffixArray = new String[]{"th", "st", "nd",
                "rd", "th", "th", "th", "th", "th", "th"};

        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return "th";
            default:
                return suffixArray[i % 10];
        }
    }

    private static interface Predicate<T> {

        boolean apply(T value);
    }

    private static final class DateBuilder {

        private final DayField dayField;

        private final Integer month;

        private final Integer year;

        private DateBuilder(DayField dayField, Integer month, Integer year) {
            this.dayField = dayField;
            this.month = month;
            this.year = year;
        }

        public DateField getDayField() {
            return dayField;
        }

        public Integer getMonth() {
            return month;
        }

        public DateBuilder setMonth(Integer month) {
            return new DateBuilder(null, month, null);
        }

        public Integer getYear() {
            return year;
        }

        public DateBuilder setYear(Integer year) {
            return new DateBuilder(null, null, year);
        }
    }

    private static abstract class DateField{

        private final int id;
        private final int value;

        private DateField(int id, int value) {
            this.id = id;
            this.value = value;
        }

        public void setFieldOnCalendar(Calendar calendar){
            calendar.set(id,value);
        }
    }

    private static final class DayField extends DateField{

        private DayField(boolean isDayOfWeek, int value) {
            super(isDayOfWeek ? Calendar.DAY_OF_WEEK:Calendar.DAY_OF_MONTH, value);
        }
    }
}
