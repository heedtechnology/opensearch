package org.heed.opensearch.search.parse;

import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.util.Version;
import org.codehaus.jparsec.*;
import org.codehaus.jparsec.error.ParserException;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.functors.*;
import org.codehaus.jparsec.pattern.CharPredicates;
import org.codehaus.jparsec.pattern.Patterns;
import org.heed.openapps.QName;
import org.heed.openapps.User;
import org.heed.opensearch.search.Clause;
import org.heed.opensearch.search.Definition;
import org.heed.opensearch.search.Dictionary;
import org.heed.opensearch.search.parse.Function;
import org.heed.opensearch.search.Parameter;
import org.heed.opensearch.search.parse.QueryParser;
import org.heed.opensearch.search.SearchQuery;
import org.heed.opensearch.search.SearchRequest;

import java.util.*;

public class LuceneParser implements QueryParser {
	
    private static final org.apache.lucene.queryParser.QueryParser luceneQueryParser = new
            org.apache.lucene.queryParser.QueryParser(Version.LUCENE_34,"content",
            new SimpleAnalyzer(Version.LUCENE_34));

	private Dictionary dictionary;

    private static final Set<String> fieldNames = new HashSet<String>(
            Arrays.asList("source_assoc", "name", "subj", "path", "id", "parent_id"));

    private static final Set<String> ops = new HashSet<String>(Arrays.asList("[", "]", "(", ")", ":", "/", "\\", ",","-"));

    private static final Terminals operatorsAndKeywords = Terminals.caseInsensitive(Parsers.or(
            Scanners.IDENTIFIER,
            Scanners.pattern(Patterns.isChar(CharPredicates.IS_ALPHA_NUMERIC), "alpha_numeric").many().source()),
            ops.toArray(DateParser.emptyStringArray),
            DateParser.addArrays(DateParser.dateQueryKeywords,fieldNames.toArray(DateParser.emptyStringArray),
                    DateParser.months, DateParser.weekdays,DateParser.monthDaySuffixArray));


    private static final Terminals numbers = Terminals.caseInsensitive(
            Parsers.sequence(Scanners.isChar('0').many(), Scanners.DEC_INTEGER),
            DateParser.emptyStringArray, DateParser.emptyStringArray, new Map<String, Object>() {
        @Override
        public Object map(String s) {
            return Tokens.fragment(s, Tokens.Tag.INTEGER);
        }
    });


    private Parser<Query> queryParser;
    private String defaultFields;

    public LuceneParser() {

	}

	public void initialize() {

	}
	
	@Override
	public SearchQuery parse(SearchRequest request) {
		SearchQuery searchQuery = new SearchQuery(SearchQuery.TYPE_LUCENE);

        String requestQuery = request.getQuery();

        Query query = null;
        if(requestQuery == null || requestQuery.isEmpty())
            query = new MatchAllDocsQuery();
        else {
            try {
                assert (queryParser != null);
                query = queryParser.parse(requestQuery);

            }catch (ParserException e){

                searchQuery.addParserException(e);

                try {
                  query = luceneQueryParser.parse(requestQuery);
                } catch (ParseException parserException) {
                    searchQuery.addParserException(parserException);
                }
            }
        }

        query = appendFiltersToQuery(request,query);

        if(query != null)
            searchQuery.setValue(query);

		return searchQuery;
	}
	
    private Parser<Query> queryParser() {

        Parser.Reference<Query> queryParserReference = Parser.newReference();

        Parser<Query> queryParser = Parsers.or(definitionParser(),subQueryParser(queryParserReference.lazy()),
                fieldQueryParser(),dateQueryParser(), freeTextQueryParser()).many().map(
            new Map<List<Query>, Query>() {

                @Override
                public Query map(List<Query> queries) {
                    BooleanQuery booleanQuery = new BooleanQuery();

                    for (Query query : queries)
                        booleanQuery.add(query, BooleanClause.Occur.MUST);

                    return booleanQuery;
                }
            }
        );

        queryParserReference.set(queryParser);
        return queryParserReference.get();
    }

    private Parser<Query> definitionParser(){

        return Parsers.token(new TokenMap<Query>() {
            @Override
            public Query map(org.codehaus.jparsec.Token token) {
                if(token.value() instanceof Definition)
                    return DefinitionToQueryMapper.getInstance().map((Definition) token.value());
                return null;
            }
        });
    }

    private Parser<Query> fieldQueryParser(){

        Parser<org.codehaus.jparsec.Token> valueParser = Parsers.token(new TokenMap<org.codehaus.jparsec.Token>() {
            @Override
            public org.codehaus.jparsec.Token map(org.codehaus.jparsec.Token token) {
                Object val = token.value();
                if(val instanceof Tokens.Fragment && ((Tokens.Fragment) val).tag() == Tokens.Tag.IDENTIFIER)
                    return token;

                return null;
            }
        });

        Terminals terminals =operatorsAndKeywords;

        return Parsers.sequence(terminals.token(fieldNames.toArray(DateParser.emptyStringArray)),terminals.token(":"),valueParser,
            new Map3<org.codehaus.jparsec.Token,org.codehaus.jparsec.Token,org.codehaus.jparsec.Token, Query>() {
            @Override
                public Query map(org.codehaus.jparsec.Token token,org.codehaus.jparsec.Token token1,
                                     org.codehaus.jparsec.Token token2) {
                    return new TermQuery(new Term(((Tokens.Fragment)token.value()).text(),
                        ((Tokens.Fragment)token2.value()).text()));
            }
        });
    }

    private Parser<Query> freeTextQueryParser(){

        return Parsers.token(new TokenMap<Query>() {
            @Override
            public Query map(org.codehaus.jparsec.Token token) {

                Object val = token.value();

                if(val instanceof Tokens.Fragment){

                    Tokens.Fragment tokenFragment = (Tokens.Fragment) val;

                    if(tokenFragment.tag() == Tokens.Tag.IDENTIFIER){
                        if(defaultFields == null || defaultFields.isEmpty())
                            return new TermQuery(new Term("content", tokenFragment.text()));

                        String[] fields = defaultFields.split(",");

                        if(fields.length == 1)
                            return  new TermQuery(new Term(defaultFields, tokenFragment.text()));

                        BooleanQuery booleanQuery = new BooleanQuery();

                        for (String field : fields)
                            booleanQuery.add(new TermQuery(new Term(field,tokenFragment.text())), BooleanClause.Occur.SHOULD);

                        return booleanQuery;
                    }
                }

                return null;
            }
        });
    }

    @SuppressWarnings("unchecked")
    private static Parser<Query> dateQueryParser(){

        Terminals terminals = operatorsAndKeywords;

        final Parser<Iterable<Date>> numericDateParser = DateParser.numericDateParser(operatorsAndKeywords);

        final Parser<Iterable<Date>> textDateParser = DateParser.dateParser(operatorsAndKeywords).map(new Map<Date, Iterable<Date>>() {
            @Override
            public Iterable<Date> map(Date date) {
                return Arrays.asList(date);
            }
        });

        final Parser<Iterable<Date>> dateKeywordParser = Parsers.or(
            terminals.token("yesterday").map(new Map<Token, Iterable<Date>>() {
                @Override
                public Iterable<Date> map(Token token) {

                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_MONTH, -1);

                    return Arrays.asList(calendar.getTime());
                }
            }),
            terminals.token("today").map(new Map<Token, Iterable<Date>>() {
                @Override
                public Iterable<Date> map(Token token) {

                    return Arrays.asList(Calendar.getInstance().getTime());
                }
            })
        );

        final Parser<Iterable<Date>> dateParser = Parsers.or(numericDateParser,textDateParser,dateKeywordParser);

        return Parsers.or(
                terminals.token("yesterday").map(new Map<Token, Query>() {
                    @Override
                    public Query map(Token token) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_MONTH, -1);

                        return makeDayQuery(calendar);
                    }
                }),
                terminals.token("today").map(new Map<Token, Query>() {
                    @Override
                    public Query map(Token token) {

                        return makeDayQuery(Calendar.getInstance());
                    }
                }),
                Parsers.sequence(Terminals.IntegerLiteral.PARSER,terminals.token("days"),
                        terminals.token("ago"),new Map3<String, Token, Token, Query>() {
                    @Override
                    public Query map(String n, Token token, Token d) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.DAY_OF_MONTH,-Integer.parseInt(n));
                        return makeDayQuery(calendar);
                    }
                }),
                Parsers.sequence(terminals.token("last"),terminals.token("week"),new Map2<Token, Token, Query>() {
                    @Override
                    public Query map(Token token, Token token1) {
                        return makeLastXDateIntervalQuery(1, Calendar.getInstance().get(Calendar.DAY_OF_WEEK), Calendar.WEEK_OF_MONTH);
                    }
                }),
                Parsers.sequence(terminals.token("last"),terminals.token("month"),new Map2<Token, Token, Query>() {
                    @Override
                    public Query map(Token token, Token token1) {
                        return makeLastXDateIntervalQuery(1, Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.MONTH);
                    }
                }),
                Parsers.sequence(terminals.token("last"),terminals.token("year"),new Map2<Token, Token, Query>() {
                    @Override
                    public Query map(Token token, Token token1) {
                        return makeLastXDateIntervalQuery(1, Calendar.getInstance().get(Calendar.DAY_OF_YEAR), Calendar.YEAR);
                    }
                }),
                Parsers.sequence(terminals.token("next"),terminals.token("week"),new Map2<Token, Token, Query>() {
                    @Override
                    public Query map(Token token, Token token1) {
                        return makeNextXDateIntervalQuery(1, Calendar.getInstance().get(Calendar.DAY_OF_WEEK), Calendar.WEEK_OF_MONTH);
                    }
                }),
                Parsers.sequence(terminals.token("next"),terminals.token("month"),new Map2<Token, Token, Query>() {
                    @Override
                    public Query map(Token token, Token token1) {
                        return makeNextXDateIntervalQuery(1, Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.MONTH);
                    }
                }),
                Parsers.sequence(terminals.token("next"),terminals.token("year"),new Map2<Token, Token, Query>() {
                    @Override
                    public Query map(Token token, Token token1) {
                        return makeNextXDateIntervalQuery(1, Calendar.getInstance().get(Calendar.DAY_OF_YEAR), Calendar.YEAR);
                    }
                }),
                Parsers.sequence(terminals.token("last"),Terminals.IntegerLiteral.PARSER,
                        terminals.token("days"),new Map3<Token, String, Token, Query>() {
                    @Override
                    public Query map(Token token,String n, Token d) {
                        return makeLastXDateIntervalQuery(Integer.parseInt(n), 1, Calendar.DAY_OF_MONTH);
                    }
                }),
                Parsers.sequence(terminals.token("last"),Terminals.IntegerLiteral.PARSER,
                        terminals.token("weeks"),new Map3<Token, String, Token, Query>() {
                    @Override
                    public Query map(Token token,String n, Token d) {
                        return makeLastXDateIntervalQuery(Integer.parseInt(n),
                                Calendar.getInstance().get(Calendar.DAY_OF_WEEK), Calendar.WEEK_OF_MONTH);
                    }
                }),
                Parsers.sequence(terminals.token("last"),Terminals.IntegerLiteral.PARSER,
                        terminals.token("months"),new Map3<Token, String, Token, Query>() {
                    @Override
                    public Query map(Token token,String n, Token d) {
                            return makeLastXDateIntervalQuery(Integer.parseInt(n),
                                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.MONTH);
                    }
                }),
                Parsers.sequence(terminals.token("last"),Terminals.IntegerLiteral.PARSER,
                        terminals.token("years"),new Map3<Token, String, Token, Query>() {
                    @Override
                    public Query map(Token token,String n, Token d) {
                        return makeLastXDateIntervalQuery(Integer.parseInt(n),
                                Calendar.getInstance().get(Calendar.DAY_OF_YEAR), Calendar.YEAR);
                    }
                }),
                Parsers.sequence(terminals.token("before"), dateParser, new Map2<Token, Iterable<Date>, Query>() {
                    @Override
                    public Query map(Token token, Iterable<Date> dates) {

                        final Iterator<Date> dateIterator = dates.iterator();

                        final GetNextDate getNextDate = new GetNextDate(dateIterator);

                        final Function<Query> queryFactory = new Function<Query>() {
                            @Override
                            public Query apply() {
                                return makeLessThanDateQuery(getNextDate.apply());
                            }
                        };

                        return makeQuery(dateIterator, queryFactory);
                    }
                }),
                Parsers.sequence(terminals.token("prior"),terminals.token("to"), dateParser, new Map3<Token,Token, Iterable<Date>, Query>() {
                    @Override
                    public Query map(Token token,Token token1, Iterable<Date> dates) {

                        final Iterator<Date> dateIterator = dates.iterator();

                        final GetNextDate getNextDate = new GetNextDate(dateIterator);

                        final Function<Query> queryFactory = new Function<Query>() {
                            @Override
                            public Query apply() {
                                return makeLessThanDateQuery(getNextDate.apply());
                            }
                        };

                        return makeQuery(dateIterator, queryFactory);
                    }
                }),
                Parsers.sequence(terminals.token("after"), dateParser, new Map2<Token, Iterable<Date>, Query>() {
                    @Override
                    public Query map(Token token, Iterable<Date> dates) {

                        final Iterator<Date> dateIterator = dates.iterator();

                        final GetNextDate getNextDate = new GetNextDate(dateIterator);

                        final Function<Query> queryFactory = new Function<Query>() {
                            @Override
                            public Query apply() {
                                return makeMoreThanDateQuery(getNextDate.apply());
                            }
                        };

                        return makeQuery(dateIterator, queryFactory);
                    }
                }),
                Parsers.sequence(terminals.token("between"), dateParser,terminals.token("and"), dateParser,
                        new Map4<Token, Iterable<Date>, Token, Iterable<Date>, Query>() {
                    @Override
                    public Query map(Token token, Iterable<Date> fromDates, Token token2, Iterable<Date> toDates) {

                        List<Query> dateQueryList = new ArrayList<Query>();

                        for (Date fromDate :fromDates)
                            for (Date toDate : toDates) dateQueryList.add(makeDateRangeQuery(fromDate,toDate));

                        if(dateQueryList.size() ==1)
                            return dateQueryList.get(0);

                        BooleanQuery disjunction = new BooleanQuery();
                        disjunction.setMinimumNumberShouldMatch(1);

                        for (Query dateQuery : dateQueryList)
                            disjunction.add(dateQuery, BooleanClause.Occur.SHOULD);

                        return disjunction;
                    }
                })
        );
    }

    private static Query makeQuery(Iterator<Date> dateIterator, Function<Query> queryFactory) {
        Query query = dateIterator.hasNext() ? queryFactory.apply() : null;

        BooleanQuery disjunction = new BooleanQuery();
        disjunction.setMinimumNumberShouldMatch(1);

        for (;dateIterator.hasNext();){

            if(disjunction == null){
                disjunction = new BooleanQuery();
                disjunction.setMinimumNumberShouldMatch(1);
            }
            disjunction.add(queryFactory.apply(), BooleanClause.Occur.SHOULD);
        }

        return disjunction == null ? disjunction : query;
    }

    private static Query makeMoreThanDateQuery(Date date) {
        return new TermRangeQuery(DateParser.dateFieldName, DateParser.dateFormat.format(date),null,false,true);
    }

    private static Query makeLessThanDateQuery(Date date) {
        return new TermRangeQuery(DateParser.dateFieldName,null, DateParser.dateFormat.format(date),true,false);
    }

    private static Query makeDayQuery(Calendar calendar) {
        return makeDateRangeQuery(calendar, calendar);
    }

    private static Query makeDateRangeQuery(Calendar start, Calendar end){
        return makeDateRangeQuery(start, end, true);
    }

    private static Query makeDateRangeQuery(Calendar start, Calendar end, boolean includeLower){
        return makeDateRangeQuery(start.getTime(), end.getTime(), includeLower);
    }

    private static Query makeDateRangeQuery(Date startTime, Date endTime, boolean includeLower) {
        return new TermRangeQuery(DateParser.dateFieldName, DateParser.dateFormat.format(startTime), DateParser.dateFormat.format(endTime), includeLower, true);
    }

    private static Query makeDateRangeQuery(Date startTime, Date endTime) {
        return makeDateRangeQuery(startTime,endTime,true);
    }

    //TODO bbja improve name of this method
    private static Query makeLastXDateIntervalQuery(int intervalCount, int daysAfterEndOfLastInterval, int dateIntervalType){

        Calendar endOfLastIntervalDate = Calendar.getInstance();
        endOfLastIntervalDate.add(Calendar.DAY_OF_YEAR, -daysAfterEndOfLastInterval);

        Calendar startDate = Calendar.getInstance();
        startDate.setTime(endOfLastIntervalDate.getTime());
        startDate.add(dateIntervalType, -intervalCount);

        return makeDateRangeQuery(startDate, endOfLastIntervalDate, false);
    }

    //TODO bbja improve name of this method
    private static Query makeNextXDateIntervalQuery(int intervalCount, int daysAfterEndOfLastInterval, int dateIntervalType){

        Calendar startOfNextIntervalDate = Calendar.getInstance();
        startOfNextIntervalDate.add(Calendar.DAY_OF_YEAR, -daysAfterEndOfLastInterval);
        startOfNextIntervalDate.add(dateIntervalType,1);

        Calendar endDate = Calendar.getInstance();
        endDate.setTime(startOfNextIntervalDate.getTime());
        endDate.add(dateIntervalType, intervalCount);

        return makeDateRangeQuery(startOfNextIntervalDate, endDate, false);
    }

    private Parser<Query> subQueryParser(Parser<Query> queryParser) {
        return queryParser.between(operatorsAndKeywords.token("("), operatorsAndKeywords.token(")"));
    }

    private Query appendFiltersToQuery(SearchRequest request, Query query) {

        User user = request.getUser();

        BooleanQuery booleanQuery = null;

        if(user != null){
            booleanQuery = booleanQuery == null ?  new BooleanQuery() : booleanQuery;
            booleanQuery.add(new TermQuery(new Term("target_assoc",String.valueOf(user.getId()))), BooleanClause.Occur.MUST);
        }

        QName qname = request.getQname();

        if(qname != null){
            booleanQuery = booleanQuery == null ?  new BooleanQuery() : booleanQuery;
            booleanQuery.add(new TermQuery(new Term("qname", qname.toString())), BooleanClause.Occur.MUST);
        }

        if(request.getClauses() != null && !request.getClauses().isEmpty()){
            booleanQuery = booleanQuery == null ?  new BooleanQuery() : booleanQuery;

            for (Clause clause : request.getClauses()){

                BooleanQuery clauses = new BooleanQuery();
                List<Parameter> parameterList = clause.getParameters();

                assert(parameterList.size() ==1);

                Parameter firstParameter = parameterList.get(0);

                if(clause.getOperator() == Clause.OPERATOR_NOT)
                    clauses.add(new TermQuery( new Term(firstParameter.getName(),
                            firstParameter.getValue())), BooleanClause.Occur.MUST_NOT);
                else{
                    assert(parameterList.size() ==2);
                    assert(clause.getOperator() == Clause.OPERATOR_AND || clause.getOperator() == Clause.OPERATOR_OR);

                    BooleanClause.Occur occur = clause.getOperator() == Clause.OPERATOR_AND ?
                            BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD;

                    clauses.add(new TermQuery( new Term(firstParameter.getName(),
                            firstParameter.getValue())), occur);

                    Parameter secondParameter = parameterList.get(2);

                    clauses.add(new TermQuery( new Term(secondParameter.getName(),
                            secondParameter.getValue())), occur);

                    if(occur == BooleanClause.Occur.SHOULD)
                        clauses.setMinimumNumberShouldMatch(1);
                }

                booleanQuery.add(clauses, BooleanClause.Occur.MUST);
            }
        }

        if(request.getParameters() != null && !request.getParameters().isEmpty()){

            booleanQuery = booleanQuery == null ?  new BooleanQuery() : booleanQuery;

            for (Parameter parameter : request.getParameters())
                booleanQuery.add(new TermQuery(new Term(parameter.getName(),parameter.getValue())),
                        BooleanClause.Occur.MUST);
        }

        if(query == null )
            return booleanQuery;

        if(booleanQuery == null)
            return query;

        booleanQuery.add(query, BooleanClause.Occur.MUST);

        return booleanQuery;

    }

    public Dictionary getDictionary() {
		return dictionary;
	}

    public void setDictionary(Dictionary dictionary) {
		this.dictionary = dictionary;

        DefinitionParser definitionParser = null;
        try {

            definitionParser = new DefinitionParser(dictionary);

            Terminals definitions = Terminals.caseInsensitive(definitionParser.getBackingParser(),
                    DateParser.emptyStringArray, DateParser.emptyStringArray, definitionParser.getWordMap());

            Parser<?> tokenizer = Parsers.or(definitions.tokenizer(),numbers.tokenizer(), operatorsAndKeywords.tokenizer());

            queryParser = queryParser().from(tokenizer,Scanners.isChar(' ').optional());

        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public DefinitionParser getTokenizer() {
        return null;
//		return tokenizer;
	}

	public void setDefaultFields(String[] defaultFields) {
//		this.defaultFields = defaultFields;
	}

    public void setParser(Object lucene) {

//		if(lucene instanceof org.apache.lucene.queryParser.QueryParser)
//			this.lucene = (org.apache.lucene.queryParser.QueryParser)lucene;
    }

	public void setDefaultFields(String defaultFields) {
		this.defaultFields = defaultFields;
	}

    private static final class GetNextDate implements Function<Date>{

        private final Iterator<Date> iterator;

        private GetNextDate(Iterator<Date> iterator) {
            this.iterator = iterator;
        }

        @Override
        public Date apply() {
            assert iterator.hasNext();
            return iterator.next();
        }
    }
}
