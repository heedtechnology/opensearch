package org.heed.opensearch.search.parse;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.heed.openapps.util.NumberUtility;
import org.heed.opensearch.search.Clause;
import org.heed.opensearch.search.Definition;
import org.heed.opensearch.search.parse.Function;
import org.heed.opensearch.search.Parameter;
import org.heed.opensearch.search.parse.TryResult;
import org.heed.opensearch.search.dictionary.AttributeDefinition;
import org.heed.opensearch.search.dictionary.CategoryDefinition;
import org.heed.opensearch.search.dictionary.CompoundDateDefinition;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Babar
 * Date: 11/17/13
 * Time: 1:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class DefinitionToQueryMapper {

    private static final Map<Class<? extends Definition>,DefinitionToQueryMap> definitionsMap =
            new HashMap<Class<? extends Definition>, DefinitionToQueryMap>(){{
                put(AttributeDefinition.class,new DefinitionToTermQueryMap());
                put(CategoryDefinition.class,new CategoryToQueryMap());
            }};

    private DefinitionToQueryMapper(){}

    public static DefinitionToQueryMapper getInstance(){
        return new DefinitionToQueryMapper();
    }

    public Query map(Definition definition) {

        assert definitionsMap.containsKey(definition.getClass());

        return definitionsMap.get(definition.getClass()).map(definition);
    }

    abstract static class DefinitionToQueryMap {
        abstract Query map(Definition definition);
    }

    static class DefinitionToTermQueryMap extends DefinitionToQueryMap {

        @Override
        TermQuery map(Definition definition) {
            return new TermQuery(new Term(definition.getName(),definition.getValue()));
        }
    }

    static class CategoryToQueryMap extends DefinitionToQueryMap {

        @Override
        Query map(Definition definition) {
            CategoryDefinition categoryDefinition = (CategoryDefinition) definition;

            List<String> ids = categoryDefinition.getIds();

            if(ids.size() == 1)
                return new TermQuery(new Term("path", ids.get(0)));

            BooleanQuery booleanQuery = new BooleanQuery();
            booleanQuery.setMinimumNumberShouldMatch(1);
            for(String id : ids)
                booleanQuery.add(new TermQuery(new Term("path",id)), BooleanClause.Occur.SHOULD);

            return booleanQuery;
        }
    }

    static class CompoundDateToQueryMap extends DefinitionToQueryMap{

        @Override
        Query map(Definition definition) {

            CompoundDateDefinition def = (CompoundDateDefinition)definition;

            final String[] chunks = def.getValue().split(" ");

            final boolean[] isFirstChunkMonth= new boolean[]{true};

            String firstChunk = "";

            if(chunks.length > 0)
                firstChunk = tryParseMonth(chunks[0]).getValueOrElse(new Function<String>() {
                    @Override
                    public String apply() {
                        isFirstChunkMonth[0] =false;
                        for(String chunk : chunks)
                            if(NumberUtility.isInteger(chunk))
                                return chunk;
                        return "";
                    }
                });

            BooleanQuery booleanQuery = new BooleanQuery();
            booleanQuery.setMinimumNumberShouldMatch(1);

            BooleanClause.Occur occur = BooleanClause.Occur.SHOULD;

            for(Clause clause : def.getClauses())
                for(Parameter param : clause.getParameters())
                    if(param.getName().equals("date")){

                        if(isFirstChunkMonth[0])
                            booleanQuery.add(new TermRangeQuery(param.getValue(),
                                    chunks[1] + firstChunk + "01", chunks[1] + firstChunk + "31",true,true), occur);
                        else booleanQuery.add(new TermRangeQuery(
                                param.getValue(), firstChunk + "0101", firstChunk + "1231",true,true), occur);

                    }else booleanQuery.add(new TermQuery(new Term(param.getName(),param.getValue())),occur);

            return booleanQuery;
        }

        private static TryResult<String> tryParseMonth(String value){

            if(value.equals("december")) return new TryResult<String>("12");
            if(value.equals("november"))  return new TryResult<String>( "11");
            if(value.equals("october"))  return new TryResult<String>( "10");
            if(value.equals("september"))  return new TryResult<String>( "09");
            if(value.equals("august"))  return new TryResult<String>( "08");
            if(value.equals("july"))  return new TryResult<String>( "07");
            if(value.equals("june"))  return new TryResult<String>( "06");
            if(value.equals("may"))  return new TryResult<String>( "05");
            if(value.equals("april"))  return new TryResult<String>( "04");
            if(value.equals("march"))  return new TryResult<String>( "03");
            if(value.equals("february"))  return new TryResult<String>( "02");
            if(value.equals("january"))  return new TryResult<String>( "01");

            return new TryResult<String>();
        }
    }

}
