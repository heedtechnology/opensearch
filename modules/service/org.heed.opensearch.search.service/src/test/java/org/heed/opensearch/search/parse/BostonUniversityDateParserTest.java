package org.heed.opensearch.search.parse;

import junit.framework.TestCase;

import java.util.Calendar;
import java.util.Date;

import org.heed.openapps.search.parsing.data.DateParser;

public class BostonUniversityDateParserTest extends TestCase {

    public void testParse() throws Exception {

        DateParser bostonUniversityDateParser = new DateParser();

        assertTrue(dateEquals(bostonUniversityDateParser.parse("2014/08/19"), august19th2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("August 19, 2014"), august19th2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("8/19/14"), august19th2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("8/19/2014"), august19th2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("2014 August"), august1st2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("2014 August 19"), august19th2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("19 August 2014"), august19th2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("2014"), january1st2014()));
//        assertTrue(dateEquals(bostonUniversityDateParser.parse("[2014]"), january1st2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("August, 2014"), august1st2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("8/2014"), august1st2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("8/14"), august1st2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse(" 08. 19, 2014"), august19th2014()));
        assertTrue(dateEquals(bostonUniversityDateParser.parse("08 2014"), august1st2014()));
        
        assertTrue(dateEquals(bostonUniversityDateParser.parse("3/3/2009"), getDate(2009, Calendar.MARCH, 3)));
        //assertTrue(dateEquals(bostonUniversityDateParser.parse("9/11/00"), getDate(2000, Calendar.SEPTEMBER, 11)));
        //assertTrue(dateEquals(bostonUniversityDateParser.parse("Oct 9, 1960"), getDate(1960, Calendar.OCTOBER, 9)));
       // assertTrue(dateEquals(bostonUniversityDateParser.parse("1980 September 19"), getDate(1980, Calendar.SEPTEMBER, 19)));
        
    }

    private static boolean dateEquals(Date date1, Date date2) {
        if(date1 == date2)
            return true;

        if(date1 == null || date2 == null)
            return false;

        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        calendar1.setTime(date1);
        calendar2.setTime(date2);

        if(calendar1.get(Calendar.DAY_OF_MONTH) != calendar2.get(Calendar.DAY_OF_MONTH))
            return false;

        if(calendar1.get(Calendar.MONTH) != calendar2.get(Calendar.MONTH))
            return false;

        if(calendar1.get(Calendar.YEAR) != calendar2.get(Calendar.YEAR))
            return false;

        return true;
    }
    
    private static Date getDate(int year, int month, int day) {
    	Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        return calendar.getTime();
    }
    private static Date august19th2014() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2014, Calendar.AUGUST, 19);
        return calendar.getTime();
    }

    private static Date january1st2014() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2014, Calendar.JANUARY, 1);
        return calendar.getTime();
    }

    private static Date august1st2014() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2014, Calendar.AUGUST, 1);
        return calendar.getTime();
    }
}