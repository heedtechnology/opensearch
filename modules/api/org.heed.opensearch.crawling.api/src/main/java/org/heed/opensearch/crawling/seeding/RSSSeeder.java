package org.heed.opensearch.crawling.seeding;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.util.XMLUtility;
import org.heed.opensearch.crawling.Seed;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;



public class RSSSeeder extends CrawlSeedingComponent {
	private static final Log log = LogFactory.getLog(RSSSeeder.class);
	private static final SimpleDateFormat fmt = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
	private String contains;
	
	//Tue, 26 Nov 2013 10:18:33 EST
	
	@Override
	public boolean canSeed(Seed seed) {
		if(contains != null) return seed.getUrl().contains(contains);
		return false;
	}
	
	public List<Seed> seed(Seed seed, byte[] content) throws CrawlSeedingException {
		List<Seed> seeds = new ArrayList<Seed>();
		try {
			RSSImportHandler handler = new RSSImportHandler();
			XMLUtility.SAXParse(false, new ByteArrayInputStream(content), handler);
            
			for(NewsItem item : handler.getNews()) {
				Seed s = seedService.getSeed(item.URL);
				s.setTitle(item.Title);
				s.setSummary(item.Description);
				
				s.setDefinition(seed.getDefinition());
				
				long timestamp = 0;
				try {
					timestamp = fmt.parse(item.Date).getTime();
				} catch(Exception e) {
					log.error("unable to parse date : "+item.Date);
				}
				s.setTimestamp(timestamp);
				
				seeds.add(s);
				
			}
			
		} catch(Exception e) {
			log.error("unable to extract : "+seed.getUrl());
		}
		return seeds;
	}

	public void setContains(String contains) {
		this.contains = contains;
	}
	
	public class RSSImportHandler extends DefaultHandler {
	    //How many RSS news items should we load before stopping.
	    private int maximumResults=100;
	    /*How many elements should we allow before stopping the parse
	      this stops giant files from breaking the server.*/
	    private static final int MAX_ELEMENTS=500;
	    //Keep track of the current element count.      
	    private int ecount=0;
	    //Keep track of the current news item count.
	    private int rcount=0;
	    private String Url="";//Url to parse.
	    //String to store parsed data to.
	    private String output="<i>Error parsing RSS feed.</i>";
	    //Current string being parsed.
	    private String currentText="";
	    //Current RSS News Item.
	    private NewsItem NI=null;
	    //ArrayList of all current News Items.
	    private ArrayList<NewsItem> news = new ArrayList<NewsItem>();
	    //Has the RSS feed's description been set yet?
	    boolean dSet=false;
		
	    public void startDocument () {} 

	    //Called when the end of the XML file is reached.
	    public void endDocument () {
	        /*If we have a partially parsed news item throw it
	          into our array.*/
	        if(NI!=null){
	            rcount++;
	            news.add(NI);
	        }
	    } 

	    //Called when we begin parsing the XML file.
	    public void startElement (String uri, String name, String qName, Attributes atts) throws SAXException {
	        //qName contains the non-URI name of the XML element.
	        if(qName.equals("item")){
	            if(NI!=null){
	                //We've fetched another news item.
	                rcount++;
	                //Add it to our ArrayList.
	                news.add(NI);
	                if(rcount==maximumResults){
	                //Maximum results have been reached.
	                    throw new SAXException("\nLimit reached.");
	                }
	            }
	            //Create a new NewsItem to add data to.
	            NI=new NewsItem();
	        }
	    } 
	    //We've reached the end of an XML element.
	    public void endElement (String uri, String name, String qName) throws SAXException {
	        //Wait for the title information.
	        if(qName.equals("title")&&output.equals("<i>Error parsing RSS feed.</i>")){
	            output="<h3>"+currentText+" (<a href=\""+Url+"\">RSS</a>)</h3><hr />";
	        }else if(qName.equals("description")&&NI==null&&!dSet){
	        	/*Add the description of the RSS feed to our
	              output if it hasn't yet been parsed.*/
	            output+="<p>"+cleanup(currentText)+"</p>";
	            dSet=true;
	        }if(qName.equals("title")&&NI!=null){//Are we parsing a news item?
	            NI.setTitle(cleanup(currentText));
	        }else if(qName.equals("link")&&NI!=null)
	            NI.setURL(cleanup(currentText));
	        else if(qName.equals("pubDate")&&NI!=null)
	            NI.setDate(cleanup(currentText));
	        else if(qName.equals("description")&&NI!=null)
	            NI.setDescription(cleanup(currentText));
	        //Make sure we don't attempt to parse too long of a document.
	        currentText="";
	        ecount++;
	        if(ecount>MAX_ELEMENTS)
	            throw new SAXException("\nLimit reached");
	    } 
	    //Parse characters from the current element we're parsing.
	    public void characters (char ch[], int start, int length) {
	        for(int i=start;i<start+length;i++){
	            currentText+=ch[i];
	        }
	    }
	    protected String cleanup(String in) {
	    	return in.replace("\n", "").trim();
	    }
		public ArrayList<NewsItem> getNews() {
			return news;
		}
	}
	public class NewsItem {
	    private String Title="";
	    private String URL="";
	    private String Description="";
	    private String Date="";
	        //Constructor.
	    public NewsItem(){} 
        //Set the title of the NewsItem.
	    public void setTitle(String Title){
	        this.Title=Title;
	    } 
        //Set the URL (Link) of the NewsItem.
	    public void setURL(String URL){
	        this.URL=URL;
	    } 
        //Set the description (summary) of the news item.
	    public void setDescription(String Description){
	        this.Description=Description;
	    } 
	    public void setDate(String Date){
	        this.Date=Date;
	    } 
        //Return an HTML representation of our news story.
	    public String toString(){
	        String returnMe="";
	        returnMe+="<a href=\""+URL+"\">"+Title+"</a><br />";
	        returnMe+="<i>"+Date+"</i>";
	        returnMe+="<p>"+Description+"</p>";
	        return(returnMe);
	    }
	}
}
