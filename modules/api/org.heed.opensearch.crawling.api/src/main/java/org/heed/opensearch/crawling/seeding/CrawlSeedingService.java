package org.heed.opensearch.crawling.seeding;
import java.util.List;

import org.apache.http.HttpResponse;
import org.heed.opensearch.crawling.Seed;


public interface CrawlSeedingService {
	
	List<Seed> process(Seed seed, byte[] content) throws Exception;
	Seed getSeed(String url);
	void updateSeed(Seed seed);
	void saveSeed(Seed seed, HttpResponse response) throws Exception;
	String getDomain(String url) throws Exception;
	
}
