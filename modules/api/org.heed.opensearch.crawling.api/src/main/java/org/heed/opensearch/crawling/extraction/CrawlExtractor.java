package org.heed.opensearch.crawling.extraction;

import org.heed.opensearch.crawling.Seed;

public interface CrawlExtractor {

	boolean canExtract(Seed seed);
	void extract(Seed seed, byte[] content) throws CrawlExtractionException;
	
}
