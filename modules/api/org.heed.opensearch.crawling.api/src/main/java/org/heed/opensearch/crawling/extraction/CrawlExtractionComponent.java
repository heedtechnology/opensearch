package org.heed.opensearch.crawling.extraction;

import org.heed.openapps.entity.EntityService;
import org.heed.opensearch.crawling.Seed;
import org.heed.opensearch.crawling.seeding.CrawlSeedingService;
import org.heed.opensearch.search.SearchService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class CrawlExtractionComponent implements CrawlExtractor, ApplicationContextAware {
	protected EntityService entityService;
	protected CrawlSeedingService seedService;
	protected DocumentEntityExtractionComponent entityExtractor;
	
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		entityService = (EntityService)ctx.getBean(EntityService.class);
		entityExtractor = new DocumentEntityExtractionComponent(entityService, (SearchService)ctx.getBean(SearchService.class));
		entityExtractor.setPersistToFile(false);
		
		seedService = (CrawlSeedingService)ctx.getBean(CrawlSeedingService.class);	
	}
	
	public abstract boolean canExtract(Seed seed);
	public abstract void extract(Seed seed, byte[] content) throws CrawlExtractionException;
	
	
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setSeedService(CrawlSeedingService seedService) {
		this.seedService = seedService;
	}
	public void setEntityExtractor(DocumentEntityExtractionComponent entityExtractor) {
		this.entityExtractor = entityExtractor;
	}
	
}
