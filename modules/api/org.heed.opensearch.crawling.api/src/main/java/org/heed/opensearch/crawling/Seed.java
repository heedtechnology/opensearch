package org.heed.opensearch.crawling;


public class Seed {
	private String id;
	private long definition;
	private long document;
	private long link;
	private String url;
	private String domain;
	private String contentType = "text";
	private String status = "";
	private String summary = "";
	private String title = "";
	private String message;
	private long lastCrawl = 0;
	private long lastView = 0;
	private long timestamp;
	private String crawlScript;
	private String processScript;
	private long user;
	
	private long delay;
	private int depth;
	private int maxdepth;
	private boolean update = false;
		
	public Seed(String url) {
		this.url = url;
	}
	public Seed(String id, String url) {
		this(url);
		this.id = id;
		
	}
	public Seed(String id, String url, String status, String summary) {
		this(url);
		this.id = id;
		this.status = status;
		this.summary = summary;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getLink() {
		return link;
	}
	public void setLink(long link) {
		this.link = link;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public long getLastCrawl() {
		return lastCrawl;
	}
	public void setLastCrawl(long lastCrawl) {
		this.lastCrawl = lastCrawl;
	}
	public long getLastView() {
		return lastView;
	}
	public void setLastView(long lastView) {
		this.lastView = lastView;
	}
	public String getCrawlScript() {
		return crawlScript;
	}
	public void setCrawlScript(String crawlScript) {
		this.crawlScript = crawlScript;
	}
	public String getProcessScript() {
		return processScript;
	}
	public void setProcessScript(String processScript) {
		this.processScript = processScript;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getDefinition() {
		return definition;
	}
	public void setDefinition(long definition) {
		this.definition = definition;
	}
	public long getDelay() {
		return delay;
	}
	public void setDelay(long delay) {
		this.delay = delay;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public int getMaxdepth() {
		return maxdepth;
	}
	public void setMaxdepth(int maxdepth) {
		this.maxdepth = maxdepth;
	}
	public boolean isUpdate() {
		return update;
	}
	public void setUpdate(boolean update) {
		this.update = update;
	}
	public long getDocument() {
		return document;
	}
	public void setDocument(long document) {
		this.document = document;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public long getUser() {
		return user;
	}
	public void setUser(long user) {
		this.user = user;
	}
	
}
