package org.heed.opensearch.crawling.extraction;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.entity.EntityService;
import org.heed.opensearch.crawling.Seed;
import org.heed.opensearch.crawling.extraction.CrawlExtractionException;
import org.heed.opensearch.crawling.extraction.CrawlExtractionService;
import org.heed.opensearch.crawling.extraction.DocumentEntityExtractionComponent;
import org.heed.opensearch.search.SearchService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class CrawlExtractionServiceImpl implements CrawlExtractionService, ApplicationContextAware {
	private static final Log log = LogFactory.getLog(CrawlExtractionServiceImpl.class);
	private EntityService entityService;
		
	private DocumentEntityExtractionComponent entityExtractor;
	private List<CrawlExtractor> extractors = new ArrayList<CrawlExtractor>();
	private List<String> blacklist = new ArrayList<String>();
	
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		entityService = (EntityService)ctx.getBean(EntityService.class);
		entityExtractor = new DocumentEntityExtractionComponent(entityService, (SearchService)ctx.getBean(SearchService.class));
		entityExtractor.setPersistToFile(false);				
	}
	
	public void process(Seed seed, byte[] content) throws CrawlExtractionException {
		try {			
			boolean processed = false;
			
			for(String val : blacklist) {
				if(seed.getUrl().contains(val)) {
					log.info("skipping extraction of blacklisted document : "+seed.getUrl()+" ");
					return;
				}
			}
			
			for(CrawlExtractor component : extractors) {
				if(component.canExtract(seed)) {
					component.extract(seed, content);
					processed = true;
				}
			}
			
			if(!processed) {
				log.info("No extractor found for : "+seed.getUrl());
			}
						
		} catch(Exception e) {
			log.error("", e);
		}
		
	}

	public void setExtractors(List<CrawlExtractor> extractors) {
		this.extractors = extractors;
	}
	public void setBlacklist(List<String> blacklist) {
		this.blacklist = blacklist;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
		
}
