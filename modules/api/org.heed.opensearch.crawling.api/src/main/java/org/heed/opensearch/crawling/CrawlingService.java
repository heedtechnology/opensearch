package org.heed.opensearch.crawling;

import java.util.List;

public interface CrawlingService {
	
	CrawlDefinition getCrawlDefinition(long id) throws Exception;
	List<CrawlDefinition> getCrawlDefinitions(String view, int frequency)  throws Exception;
	
}
