package org.heed.opensearch.crawling.extraction;

import org.heed.opensearch.crawling.Seed;


public interface CrawlExtractionService {

	
	void process(Seed seed, byte[] content) throws CrawlExtractionException;
	
}
