package org.heed.opensearch.crawling;

import org.heed.openapps.QName;


public class CrawlingModel {
	public static final String CRAWLING_MODEL_1_0_URI = "openapps.org_crawling_1.0";
	
	public static final QName URL = new QName(CRAWLING_MODEL_1_0_URI, "url");
	public static final QName DEFINITION = new QName(CRAWLING_MODEL_1_0_URI, "definition");
	public static final QName DEFINITIONS = new QName(CRAWLING_MODEL_1_0_URI, "definitions");
	public static final QName SEED = new QName(CRAWLING_MODEL_1_0_URI, "seed");
	
	public static final QName DOCUMENT_DELETE = new QName(CRAWLING_MODEL_1_0_URI, "document_delete");
	
	public static final QName CATEGORY = new QName(CRAWLING_MODEL_1_0_URI, "category");
	public static final QName CATEGORIES = new QName(CRAWLING_MODEL_1_0_URI, "categories");
	
	public static final QName CRAWL_TYPE = new QName(CRAWLING_MODEL_1_0_URI, "crawl_type");
	public static final QName PROCESS_TYPE = new QName(CRAWLING_MODEL_1_0_URI, "process_type");
	public static final QName RULE = new QName(CRAWLING_MODEL_1_0_URI, "rule");
	public static final QName STATUS = new QName(CRAWLING_MODEL_1_0_URI, "status");
	public static final QName LAST_CRAWL = new QName(CRAWLING_MODEL_1_0_URI, "last_crawl");
	public static final QName START = new QName(CRAWLING_MODEL_1_0_URI, "start");
	public static final QName END = new QName(CRAWLING_MODEL_1_0_URI, "end");
	public static final QName CRAWL_SCRIPT = new QName(CRAWLING_MODEL_1_0_URI, "crawl_script");
	public static final QName PROCESS_SCRIPT = new QName(CRAWLING_MODEL_1_0_URI, "process_script");
	public static final QName TRAILER = new QName(CRAWLING_MODEL_1_0_URI, "trailer");
	public static final QName FREQUENCY = new QName(CRAWLING_MODEL_1_0_URI, "frequency");
	public static final QName TIME1 = new QName(CRAWLING_MODEL_1_0_URI, "time1");
	
	public static final QName MESSAGE = new QName(CRAWLING_MODEL_1_0_URI, "message");
	
}
