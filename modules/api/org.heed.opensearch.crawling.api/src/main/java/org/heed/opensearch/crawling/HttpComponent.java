package org.heed.opensearch.crawling;

import java.util.Map;

import org.apache.http.HttpResponse;

public interface HttpComponent {

	HttpResponse fetch(String urlStr, String queryString, Map<String,String> headers);
	
}
