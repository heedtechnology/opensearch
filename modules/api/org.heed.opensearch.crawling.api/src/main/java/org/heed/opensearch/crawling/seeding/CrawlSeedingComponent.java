package org.heed.opensearch.crawling.seeding;

import java.util.List;

import org.heed.opensearch.crawling.Seed;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class CrawlSeedingComponent implements ApplicationContextAware {
	protected CrawlSeedingService seedService;
	
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		seedService = (CrawlSeedingService)ctx.getBean(CrawlSeedingService.class);
	}
	
	public abstract boolean canSeed(Seed seed);
	public abstract List<Seed> seed(Seed seed, byte[] content) throws CrawlSeedingException;
	
}
