package org.heed.opensearch.crawling;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.util.NumberUtility;


public class CrawlDefinition {
	private long id;
	private String name;
	private String url;	
	private String status = STATE_PAUSED;	
	private String message;
	private int start;
	private int end;
	private String frequency;
	private String time1;
	private String crawlType;
	private String processType;
	private String crawlTemplate;
	private String processTemplate;
	private String crawlScript;
	private String processScript;
	private int maxpages;
	private int maxdepth;	
	private String comment;
	private String trailer;
	private long lastCrawl;
	private long user;
	
	private List<Seed> seeds = new ArrayList<Seed>();
	
	public static final String STATE_HIT = "hit";
	public static final String STATE_RUNNING = "running";
	public static final String STATE_PAUSED = "paused";
	
	public static final String TYPE_HIGH_VOLUME = "idrange";
	public static final String TYPE_STANDARD = "standard";
	
	public CrawlDefinition() {}
	public CrawlDefinition(Entity entity) {
		String name = entity.getPropertyValue(SystemModel.NAME);
		String url = entity.getPropertyValue(CrawlingModel.URL);
		String type = entity.getPropertyValue(CrawlingModel.CRAWL_TYPE);
		//String rule = entity.getPropertyValue(CrawlingModel.RULE);
		String startStr = entity.getPropertyValue(CrawlingModel.START);
		String endStr = entity.getPropertyValue(CrawlingModel.END);
		String crawlScript = entity.getPropertyValue(CrawlingModel.CRAWL_SCRIPT);
		String processScript = entity.getPropertyValue(CrawlingModel.PROCESS_SCRIPT);
		String trailer = entity.getPropertyValue(CrawlingModel.TRAILER);
		String status = entity.getPropertyValue(CrawlingModel.STATUS);
		String frequency = entity.getPropertyValue(CrawlingModel.FREQUENCY);
		String lastCrawl = entity.getPropertyValue(CrawlingModel.LAST_CRAWL);
		String time1 = entity.getPropertyValue(CrawlingModel.TIME1);
		long user = entity.getUser();
		
		setId(entity.getId());
		setUrl(url);
		setName(name);
		setCrawlType(type);
		if(status != null && status.length() > 0) setStatus(status);
		setTrailer(trailer);
		setCrawlScript(crawlScript);
		setProcessScript(processScript);
		setFrequency(frequency);
		setTime1(time1);
		setUser(user);
		
		if(NumberUtility.isLong(lastCrawl)) setLastCrawl(Long.valueOf(lastCrawl));
		if(NumberUtility.isInteger(startStr)) setStart(Integer.valueOf(startStr));
		if(NumberUtility.isInteger(type)) setEnd(Integer.valueOf(endStr));
	}
	public CrawlDefinition(long id, String url) {
		this.id = id;
		this.url = url;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Seed> getSeeds() {
		return seeds;
	}
	public void setSeeds(List<Seed> seeds) {
		this.seeds = seeds;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCrawlType() {
		return crawlType;
	}
	public void setCrawlType(String crawlType) {
		this.crawlType = crawlType;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public String getCrawlTemplate() {
		return crawlTemplate;
	}
	public void setCrawlTemplate(String crawlTemplate) {
		this.crawlTemplate = crawlTemplate;
	}
	public String getProcessTemplate() {
		return processTemplate;
	}
	public void setProcessTemplate(String processTemplate) {
		this.processTemplate = processTemplate;
	}
	public int getMaxpages() {
		return maxpages;
	}
	public void setMaxpages(int maxpages) {
		this.maxpages = maxpages;
	}
	public int getMaxdepth() {
		return maxdepth;
	}
	public void setMaxdepth(int maxdepth) {
		this.maxdepth = maxdepth;
	}
	public String getCrawlScript() {
		return crawlScript;
	}
	public void setCrawlScript(String crawlScript) {
		this.crawlScript = crawlScript;
	}
	public String getProcessScript() {
		return processScript;
	}
	public void setProcessScript(String processScript) {
		this.processScript = processScript;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getTrailer() {
		return trailer;
	}
	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public long getLastCrawl() {
		return lastCrawl;
	}
	public void setLastCrawl(long lastCrawl) {
		this.lastCrawl = lastCrawl;
	}
	public String getTime1() {
		return time1;
	}
	public void setTime1(String time1) {
		this.time1 = time1;
	}
	public long getUser() {
		return user;
	}
	public void setUser(long user) {
		this.user = user;
	}
	
}
