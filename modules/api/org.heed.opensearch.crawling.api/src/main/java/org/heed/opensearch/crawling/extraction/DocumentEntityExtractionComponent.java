package org.heed.opensearch.crawling.extraction;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.heed.openapps.content.Mimetypes;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.util.IOUtility;
import org.heed.opensearch.crawling.CrawlingModel;
import org.heed.opensearch.crawling.Seed;
import org.heed.opensearch.search.SearchService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class DocumentEntityExtractionComponent extends CrawlExtractionComponent {
	private static final Log log = LogFactory.getLog(DocumentEntityExtractionComponent.class);
	private SearchService searchService;
	
	private PDFTextStripper stripper = null;
	
	private boolean persistToFile = true;
	
	public DocumentEntityExtractionComponent() {}
	public DocumentEntityExtractionComponent(EntityService entityService, SearchService searchService) {
		this.entityService = entityService;
		this.searchService = searchService;
	}
	
	@Override
	public boolean canExtract(Seed seed) {
		return true;
	}
	
	@Override
	public void extract(Seed seed, byte[] content) throws CrawlExtractionException {
		try {
			Entity documentEntity = null;
			long currentTime = System.currentTimeMillis();
		
			EntityQuery query = new EntityQuery(ContentModel.DOCUMENT);
			query.getProperties().add(new Property(new QName("openapps.org_content_1.0", "url_e"), seed.getUrl()));
			EntityResultSet documentEntities = entityService.search(query);
		
			if(documentEntities != null && documentEntities.getResults().size() > 0) {
				documentEntity = documentEntities.getResults().get(0);
				seed.setDocument(documentEntity.getId());
				log.info("found document : "+seed.getUrl()+", preparing to update...");
			} else {			
				documentEntity = new Entity(ContentModel.DOCUMENT);				
				log.info("found no document : "+seed.getUrl()+", preparing to add...");
			}
		
			documentEntity.addProperty(ContentModel.DOCUMENT_URL, seed.getUrl());
			if(seed.getTimestamp() > 0) documentEntity.addProperty(ContentModel.DOCUMENT_TIMESTAMP, seed.getTimestamp());
			else documentEntity.addProperty(ContentModel.DOCUMENT_TIMESTAMP, currentTime);
			
			//documentEntity.addProperty(ContentModel.DOCUMENT_DOMAIN, seed.getDomain());
			
			documentEntity.addProperty(CrawlingModel.LAST_CRAWL, currentTime);
			documentEntity.addProperty(CrawlingModel.DEFINITION, seed.getDefinition());
			
			if(seed.getSummary() != null) {
				documentEntity.addProperty(ContentModel.DOCUMENT_CONTENT, seed.getSummary());
			}
			
			if(seed.getTitle() != null) {
				documentEntity.addProperty(SystemModel.TITLE, seed.getTitle());
			}
			
			if(content != null)
				extract(content, documentEntity);
						
			if(documentEntity.getId() != null) {
				entityService.updateEntity(documentEntity);
			} else {
				if(documentEntity.hasProperty(SystemModel.TITLE) && documentEntity.getPropertyValue(SystemModel.TITLE).length() > 0) {
					entityService.addEntity(documentEntity);
					
					//relation - definition to document
					if(seed.getDefinition() > 0) {
						Association assoc = new Association(ContentModel.DOCUMENTS, seed.getDefinition(), documentEntity.getId());
						entityService.addAssociation(assoc);
						searchService.update(seed.getDefinition());
						log.info("adding definition->document link from "+seed.getDefinition()+" to "+documentEntity.getId());
					}
										
					//relation - user to document
					if(seed.getUser() > 0) {
						Association assoc = new Association(ContentModel.DOCUMENTS, seed.getUser(), documentEntity.getId());
						entityService.addAssociation(assoc);
						searchService.update(seed.getUser());
						log.info("adding user->document link from "+seed.getUser()+" to "+documentEntity.getId());
					}
					
					//relation - incoming link from other Document
					if(seed.getLink() > 0) {
						Association assoc = new Association(ContentModel.DOCUMENTS, seed.getLink(), documentEntity.getId());
						entityService.addAssociation(assoc);
						searchService.update(seed.getLink());
						log.info("adding document link from "+seed.getLink()+" to "+documentEntity.getId());
					}					
					searchService.update(documentEntity.getId());
				}
			}
			
			if(documentEntity != null && documentEntity.getId() > 0) {				
				seed.setDocument(documentEntity.getId());
				
				if(persistToFile && content != null)
					outputFile(documentEntity.getUuid(), content);
			}
		} catch(Exception e) {
			log.error("", e);
			throw new CrawlExtractionException();
		}
	}
	protected void extract(byte[] content, Entity documentEntity) {
		try {
			InputStream is = new ByteArrayInputStream(content);
			Metadata metadata = new Metadata();
			BodyContentHandler ch = new BodyContentHandler();
			AutoDetectParser parser = new AutoDetectParser();

			String mimeType = new Tika().detect(is);
			
			parser.parse(is, ch, metadata, new ParseContext());
			is.close();
			
			documentEntity.addProperty(ContentModel.CONTENT_TYPE, mimeType);
			if(mimeType.equals(Mimetypes.MIMETYPE_HTML) || mimeType.equals(Mimetypes.MIMETYPE_XHTML)) {
				Document document = Jsoup.parse(new ByteArrayInputStream(content), "UTF-8", "");
				if(!documentEntity.hasProperty(SystemModel.TITLE) || documentEntity.getPropertyValue(SystemModel.TITLE).length() == 0) 
					documentEntity.addProperty(SystemModel.TITLE, document.title());
				if(!documentEntity.hasProperty(ContentModel.DOCUMENT_CONTENT) || documentEntity.getPropertyValue(ContentModel.DOCUMENT_CONTENT).length() == 0) {
					documentEntity.addProperty(ContentModel.DOCUMENT_CONTENT, document.text());
				}
			} else if(mimeType.equals(Mimetypes.MIMETYPE_PDF)) {
				pdfExtract(content, documentEntity);
			} else {
				if(!documentEntity.hasProperty(SystemModel.TITLE) || documentEntity.getPropertyValue(SystemModel.TITLE).length() == 0)  
					documentEntity.addProperty(SystemModel.TITLE, metadata.get("title"));
				if(!documentEntity.hasProperty(ContentModel.DOCUMENT_CONTENT) || documentEntity.getPropertyValue(ContentModel.DOCUMENT_CONTENT).length() == 0) {
					documentEntity.addProperty(ContentModel.DOCUMENT_CONTENT, ch.toString());
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	protected void outputFile(String uid, byte[] input) throws IOException {
		if(uid == null) {
			log.error("unable to output file for missing uid");
			return;
		}
		File root = new File("data/db/content");
		if(!root.exists()) root.mkdir();
		root = new File(root, Character.toString(uid.charAt(0)));
		if(!root.exists()) root.mkdir();
		root = new File(root, Character.toString(uid.charAt(1)));
		if(!root.exists()) root.mkdir();
		root = new File(root, Character.toString(uid.charAt(2)));
		if(!root.exists()) root.mkdir();
		root = new File(root, Character.toString(uid.charAt(3)));
		if(!root.exists()) root.mkdir();
		root = new File(root, Character.toString(uid.charAt(4)));
		if(!root.exists()) root.mkdir();
		
		File file = new File(root, uid+".bin");
		if(!file.exists()) file.createNewFile();
		FileOutputStream output = new FileOutputStream(file);
		IOUtility.pipe(new ByteArrayInputStream(input), output);
	}
	protected void pdfExtract(byte[] content, Entity documentEntity) {
		PDDocument pdfDocument = null;
		try {
			pdfDocument = PDDocument.load(new ByteArrayInputStream(content));
			if(pdfDocument.isEncrypted()) {
				try {
					pdfDocument.decrypt("");
				} catch(InvalidPasswordException e) {
					log.error("Error: The document is encrypted.");
				} catch(org.apache.pdfbox.exceptions.CryptographyException e) {
					log.error("", e);
				}	
			}
			
			/* Title didn't work at all well in first set of docs, actually produced character encoding issues
			PDDocumentInformation info = pdfDocument.getDocumentInformation();
			if(info != null) {
        		String title = info.getTitle();
        		documentEntity.addProperty(SystemModel.TITLE, title);
        	}
			 */
			if(!documentEntity.hasProperty(ContentModel.DOCUMENT_CONTENT) || documentEntity.getPropertyValue(ContentModel.DOCUMENT_CONTENT).length() == 0) {
			
				StringWriter writer = new StringWriter();
				if(stripper == null) {
					stripper = new PDFTextStripper();
				} else {
					stripper.resetEngine();
				}
				stripper.writeText(pdfDocument, writer);
				String contents = writer.getBuffer().toString();
        
				documentEntity.addProperty(ContentModel.DOCUMENT_CONTENT, contents);
        
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(pdfDocument != null) 
					pdfDocument.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setPersistToFile(boolean persistToFile) {
		this.persistToFile = persistToFile;
	}
	
}
