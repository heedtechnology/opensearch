package org.heed.opensearch.crawling.seeding;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.bsf.BSFManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.util.FileUtility;
import org.heed.opensearch.crawling.CrawlingContext;
import org.heed.opensearch.crawling.CrawlingModel;
import org.heed.opensearch.crawling.Seed;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;


public class CrawlSeedingServiceImpl implements CrawlSeedingService, ApplicationContextAware {
	private static final Log log = LogFactory.getLog(CrawlSeedingServiceImpl.class);
	private EntityService entityService;
	
	private BSFManager manager = new BSFManager();
	private String crawlRoot = null;
	
	private List<CrawlSeedingComponent> seeders = new ArrayList<CrawlSeedingComponent>();
	
	private final LoadingCache<String, Seed> seedCache = CacheBuilder.newBuilder().maximumSize(1).expireAfterAccess(2, TimeUnit.DAYS)
		.build(	new CacheLoader<String, Seed>() {
			public Seed load(String url) {
				if(!url.startsWith("http://")) url = "http://"+url;
				Seed seed = new Seed(url, url);				
				try {
					seed.setDomain(getDomain(url));		
					EntityQuery query = new EntityQuery(ContentModel.DOCUMENT);
					query.getProperties().add(new Property(new QName("openapps.org_content_1.0", "url_e"), seed.getUrl()));
					
					EntityResultSet documentEntities = entityService.search(query);
					if(documentEntities != null && documentEntities.getResults().size() > 0) {
						Entity documentEntity = documentEntities.getResults().get(0);
						seed.setDocument(documentEntity.getId());
						if(documentEntity.hasProperty(CrawlingModel.LAST_CRAWL)) {
							long lastCrawl = (Long)documentEntity.getProperty(CrawlingModel.LAST_CRAWL).getValue();
							seed.setLastCrawl(lastCrawl);
							//seed.setMessage(fmt.format(new Date(lastCrawl)));
						}
						if(documentEntity.hasProperty(ContentModel.DOCUMENT_CONTENT)) {
							seed.setSummary(documentEntity.getPropertyValue(ContentModel.DOCUMENT_CONTENT));
						}
						if(documentEntity.hasProperty(CrawlingModel.STATUS)) {
							seed.setStatus(documentEntity.getPropertyValue(CrawlingModel.STATUS));
						}
					}
				} catch(Exception e) {
					log.error("", e);
				}
			return seed;
		}
	});	
		
	@Override
	public List<Seed> process(Seed seed, byte[] content) throws Exception {
		List<Seed> seeds = new ArrayList<Seed>();
		
		for(CrawlSeedingComponent component : seeders) {
			if(component.canSeed(seed)) {
				List<Seed> componentSeeds = component.seed(seed, content);
				seeds.addAll(componentSeeds);
				break;
			}
		}
				
		return seeds;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		entityService = (EntityService)ctx.getBean(EntityService.class);
		
		try {
			File file = new File("configuration/templates/crawling", "execution_jsoup.groovy");
			if(file.exists()) crawlRoot = FileUtility.readToString(file);
		
		} catch(Exception e) {
			log.error("", e);
		}
	}
		
	public List<Seed> processScript(Seed seed, byte[] content) throws IOException {
		List<Seed> seeds = new ArrayList<Seed>();
		String crawlScript = null;
		
		Document document = Jsoup.parse(new ByteArrayInputStream(content), "UTF-8", "");
			
		if(crawlRoot != null && seed.getCrawlScript() != null) {
			crawlScript = crawlRoot.replace("{body}", seed.getCrawlScript());
		}
			
			
		if(seed.getCrawlScript() != null) {
			try {
				manager.declareBean("context", document, CrawlingContext.class);
				manager.declareBean("document", document, Document.class);
				manager.declareBean("seed", seed, Seed.class);
				
				manager.exec("groovy", "script.groovy", 0, 0, seed.getCrawlScript());
			
			} catch(Exception e) {
				log.error("", e);
			}
		}
		return seeds;
	}
	public Seed getSeed(String url) {
		try {
			return seedCache.get(url);
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}
	public void updateSeed(Seed seed) {
		try {
			seedCache.put(seed.getUrl(), seed);
		} catch (Exception e) {
			log.error("", e);
		}
	}
	public void saveSeed(Seed seed, HttpResponse response) throws Exception {
		Entity seedEntity = new Entity(CrawlingModel.SEED);
		seedEntity.addProperty(ContentModel.DOCUMENT_URL, seed.getUrl());
		seedEntity.addProperty(CrawlingModel.MESSAGE, response.getStatusLine().getReasonPhrase());
		seedEntity.addProperty(CrawlingModel.STATUS, response.getStatusLine().getStatusCode());
		entityService.addEntity(seedEntity);
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
		
	public String getDomain(String url) throws Exception {
		if(url == null || url.length() < 4) return "";
		URI uri = new URI(url);
		String domain = uri.getHost();
		if(domain != null) {
			domain = domain.replace("google2.", "");
			if(domain.startsWith("www.")) domain = domain.substring(4);
			return domain;
		}
		return "";
	}
	public List<CrawlSeedingComponent> getSeeders() {
		return seeders;
	}
	public void setSeeders(List<CrawlSeedingComponent> seeders) {
		this.seeders = seeders;
	}
	
}
