package org.heed.opensearch.crawling.extraction;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.bsf.BSFManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.util.FileUtility;
import org.heed.opensearch.crawling.CrawlingContext;
import org.heed.opensearch.crawling.CrawlingModel;
import org.heed.opensearch.crawling.Seed;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class BSFExtractionComponent implements CrawlExtractionService {
	private static final Log log = LogFactory.getLog(BSFExtractionComponent.class);
	private BSFManager manager = new BSFManager();
	private String processRoot = null;
	
	
	public BSFExtractionComponent() {
		try {
			File file = new File("configuration/templates/processing", "execution_jsoup.groovy");
			if(file.exists()) processRoot = FileUtility.readToString(file);			
		} catch(Exception e) {
			log.error("", e);
		}
	}
	public void process(Seed seed, byte[] content) throws CrawlExtractionException {
		List<Entity> entities = new ArrayList<Entity>();
		String processScript = null;
		if(processRoot != null && seed.getProcessScript() != null) {
			processScript = processRoot.replace("{body}", seed.getProcessScript());
		}
		
		try {
			Document document = Jsoup.parse(new ByteArrayInputStream(content), "UTF-8", "");
		
			Entity documentEntity = outputDocument(seed, content);
			
			entities.add(documentEntity);
			
			if(seed.getProcessScript() != null) {
				manager.declareBean("context", document, CrawlingContext.class);
				manager.declareBean("document", document, Document.class);
				manager.declareBean("seed", seed, Seed.class);
			
				manager.exec("groovy", "script.groovy", 0, 0, processScript);
			}
		} catch(Exception e) {
			log.error("", e);
		}
		
	}
	
	public Entity outputDocument(Seed seed, byte[] content) throws Exception {
		Document document = Jsoup.parse(new ByteArrayInputStream(content), "UTF-8", "");
		long currentTime = System.currentTimeMillis();
		
		Entity documentEntity = new Entity(ContentModel.DOCUMENT);
		documentEntity.addProperty(ContentModel.DOCUMENT_URL, seed.getUrl());
		documentEntity.addProperty(SystemModel.TITLE, document.title());
		documentEntity.addProperty(ContentModel.DOCUMENT_CONTENT, document.text());
		documentEntity.addProperty(CrawlingModel.LAST_CRAWL, currentTime);
		documentEntity.getTargetAssociations().add(new Association(ContentModel.DOCUMENTS, seed.getDefinition(), 0));
		
		return documentEntity;
	}
}
