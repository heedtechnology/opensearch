package org.heed.opensearch.crawling.data;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.heed.openapps.QName;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.DefaultEntityPersistenceListener;
import org.heed.openapps.util.NumberUtility;
import org.heed.opensearch.crawling.CrawlingModel;


public class CrawlDefinitionPersistenceListener extends DefaultEntityPersistenceListener {
	private static final Log log = LogFactory.getLog(CrawlDefinitionPersistenceListener.class);
	
	
	@Override
	public Entity extractEntity(HttpServletRequest request, QName qname) throws InvalidEntityException {
		Entity entity = super.extractEntity(request, qname);
		String categoryStr = request.getParameter("category");
		
		if(entity != null && categoryStr != null && NumberUtility.isLong(categoryStr)) {
			long categoryid = Long.valueOf(categoryStr);
			Association assoc = entity.getTargetAssociation(CrawlingModel.CATEGORIES);
			if(assoc == null) {
				Entity category = getEntityService().getEntity(categoryid);
				assoc = new Association(CrawlingModel.CATEGORIES, category, entity);
				entity.getTargetAssociations().add(assoc);
			} else if(assoc.getSource() != categoryid) {
				assoc.setSource(categoryid);
			}
		}
		
		return entity;
	}
		
	@Override
	public void onBeforeUpdate(Entity oldValue, Entity newValue) {
		Association assoc = newValue.getTargetAssociation(CrawlingModel.CATEGORIES);
		if(assoc != null) {
			try {
				if(assoc.getId() != null) getEntityService().updateAssociation(assoc);
				else getEntityService().addAssociation(assoc);
			} catch(Exception e) {
				log.error("", e);
			}
		}
	}
}
