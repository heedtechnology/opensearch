package org.heed.opensearch.search.parse;

public final class DateComponents {
	private final RangeItem<OneToTwelveRange> oneToTwelveRangeElement;
	private final RangeItem<OneToThirtyOneRange> oneToThirtyOneRangeElement;
	private final int year;
	
	DateComponents(RangeItem<OneToTwelveRange> oneToTwelveRangeElement,
			RangeItem<OneToThirtyOneRange> oneToThirtyOneRangeElement, int year) {
		super();
		this.oneToTwelveRangeElement = oneToTwelveRangeElement;
		this.oneToThirtyOneRangeElement = oneToThirtyOneRangeElement;
		this.year = year;
	}
}
