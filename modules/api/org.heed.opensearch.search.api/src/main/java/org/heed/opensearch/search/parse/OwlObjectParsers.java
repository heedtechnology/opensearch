package org.heed.opensearch.search.parse;

import org.codehaus.jparsec.Parser;
import org.heed.opensearch.search.LexNodeBase;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA. User: Babar Date: 10/23/13 Time: 5:53 AM To
 * change this template use File | Settings | File Templates.
 */
public final class OWLObjectParsers extends TerminalParsers {
    private static final OWLDataFactory dataFactory = OWLManager.getOWLDataFactory();
    private static final OWLAnnotationProperty rdfsLabelAnnotation = dataFactory.getRDFSLabel();

    private static <Entity extends OWLEntity> void loadEntities(Set<Entity> entities, LexNodeBase<Entity> root,
                                                                OWLOntology owlOntology) {
        for (Entity entity : entities)
            for (OWLAnnotation annotation : entity.getAnnotations(owlOntology, rdfsLabelAnnotation)) {
                final OWLLiteral labelValue = (OWLLiteral) annotation.getValue();
                if (labelValue.hasLang("en"))
                    loadEntity(entity, root, false, labelValue.getLiteral());
            }
	}

	private static  <Entity extends OWLEntity> void loadEntity(Entity namedObject, LexNodeBase<Entity> root,
                                                       boolean isCaseSensitive, String keyword) {
        loadData(namedObject, root, isCaseSensitive, keyword);
	}

    public static Parser<OWLClass> classParser(OWLOntology owlOntology) {
        LexNodeBase<OWLClass> root = new LexNodeBase<OWLClass>();
        loadEntities(owlOntology.getClassesInSignature(), root, owlOntology);

        return createScanner(root, "OwlClass");
    }

    public static Parser<OWLObjectProperty> propertyParser(OWLOntology owlOntology) {
        LexNodeBase<OWLObjectProperty> root = new LexNodeBase<OWLObjectProperty>();
        loadEntities(owlOntology.getObjectPropertiesInSignature(), root, owlOntology);

        return createScanner(root, "OwlProperty");
    }

    public static Parser<OWLNamedIndividual> createIndividualParser(OWLOntology ontology, IRI individualClassIRI) {
        LexNodeBase<OWLNamedIndividual> root = new LexNodeBase<OWLNamedIndividual>();
        Set<OWLNamedIndividual> entities = new TreeSet<OWLNamedIndividual>();

         for (OWLIndividual owlIndividual:dataFactory.getOWLClass(individualClassIRI).getIndividuals(ontology))
            if(owlIndividual.isNamed()) entities.add(owlIndividual.asOWLNamedIndividual());

        loadEntities(entities, root, ontology);

        return createScanner(root, String.format("OwlIndividual:%s", individualClassIRI.getFragment()));
    }
}
