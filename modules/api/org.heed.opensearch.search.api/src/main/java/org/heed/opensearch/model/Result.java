package org.heed.opensearch.model;
import java.io.Serializable;
import java.util.Map;


public class Result implements Serializable {
	private static final long serialVersionUID = 7242025275543439917L;
	private String id;
	private String url;
	private String name = "";
	private String description = "";
	private String contentType = "";
		
	
	public Result() {}
	public Result(String id, String name, String description, String contentType) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.contentType = contentType;
	}
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}	
	public void setData(Map<String,String> data) {
		
	}
}
