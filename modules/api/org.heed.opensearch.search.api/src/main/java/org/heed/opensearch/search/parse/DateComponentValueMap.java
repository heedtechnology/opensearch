package org.heed.opensearch.search.parse;

import org.codehaus.jparsec.functors.Map;

public final class DateComponentValueMap implements
		Map<String, DateComponentValue> {
	private final String pattern;
	
	public DateComponentValueMap(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public DateComponentValue map(String from) {
		return new DateComponentValue(from, pattern);
	}
}
