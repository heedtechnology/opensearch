package org.heed.opensearch.search.parse;

import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.Scanners;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.pattern.Pattern;
import org.heed.opensearch.search.LexNodeBase;

/**
 * Created by babar on 2/15/15.
 */
public abstract class TerminalParsers {

    protected static <Terminal> Parser<Terminal> createScanner(LexNodeBase<Terminal> root, String terminalName) {
        final TerminalPattern<Terminal> terminalPattern = new TerminalPattern<Terminal>(root);
        return Scanners.pattern(terminalPattern, terminalName).map(new Map<Void, Terminal>() {
            @Override
            public Terminal map(Void aVoid) {
                return terminalPattern.currentNode.getDatas().get(0);
            }
        });
    }

    protected static <Data> void loadData(Data data, LexNodeBase<Data> root,
                                          boolean isCaseSensitive, String keyword) {
        CharSequence charSeq = keyword;
        LexNodeBase<Data> node = root;

        for (int i = 0; i < charSeq.length(); i++) {
            char ch = charSeq.charAt(i);
            LexNodeBase<Data> child = node.getChild(ch);
            node = child == null ? node.addChild(ch, false) : child;
        }

        node.addData(data);
    }

    private static class TerminalPattern<Terminal> extends Pattern {
        private LexNodeBase<Terminal> currentNode;

        public TerminalPattern(LexNodeBase<Terminal> root) {
            currentNode = root;
        }

        @Override
        public int match(CharSequence src, int begin, int end) {
            for (int i = begin; i < end; i++) {
                char ch = src.charAt(i);
                if (currentNode.childExists(ch))
                    currentNode = currentNode.getChild(ch);
                else return match(i-1, end);
            }
            return match(begin, end);
        }

        private int match(int begin, int end) {
            assert (begin != -1 || !currentNode.hasData());
            return currentNode.hasData() ? (end - begin) +1 : MISMATCH;
        }
    }
}
