package org.heed.archiveManger.parse;

import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.functors.Map;
import org.heed.archiveManger.parse.ast.SubjectIndividual;
import org.heed.opensearch.search.parse.OWLObjectParsers;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import java.io.File;

/**
 * Created by babar on 2/15/15.
 */
public final class ArchiveManagerOntologyObjectParsers {
    private final OWLOntology archiveManagerOntology;

    public ArchiveManagerOntologyObjectParsers(File archiveManagerOntologyFile) throws OWLOntologyCreationException {
        archiveManagerOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(archiveManagerOntologyFile);
    }

    public Parser<SubjectIndividual> subjectIndividualParser() {
        return OWLObjectParsers.createIndividualParser(archiveManagerOntology,
                IRI.create("http://www.w3.org/2002/07/owl#openapps_org_classification_1_0_subject")).map(
                new Map<OWLNamedIndividual, SubjectIndividual>() {
                    @Override
                    public SubjectIndividual map(OWLNamedIndividual owlNamedIndividual) {
                        return new SubjectIndividual(owlNamedIndividual.getIRI().getFragment());
                    }
                }
        );
    }
}
