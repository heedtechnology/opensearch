package org.heed.opensearch.search.parse;

final class RangeItem<T extends Range> {

	private final T range;
	private final int index;
	
	RangeItem(T range, int index) {
		super();
		this.range = range;
		this.index = index;
	}
	
	public int getValue() {
		return range.get(index);
	}
}
