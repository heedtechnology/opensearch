package org.heed.archiveManger.parse.ast;

/**
 * Created by babar on 2/21/15.
 */
public final class SubjectIndividual {
    private final String source;

    public SubjectIndividual(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }
}
