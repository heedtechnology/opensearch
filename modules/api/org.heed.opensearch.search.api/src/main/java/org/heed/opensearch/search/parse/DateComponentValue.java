package org.heed.opensearch.search.parse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class DateComponentValue {
	private final String value;
	private final String pattern;

	public DateComponentValue(String value, String pattern) {
		super();
		this.value = value;
		this.pattern = pattern;
	}

	public String getValue() {
		return value;
	}
	
	public Calendar SetOnCalendar(){
		return SetOnCalendar(new SimpleDateFormat(pattern));
	}
	
	private Calendar SetOnCalendar(SimpleDateFormat simpleDateFormat) {
		try {
			simpleDateFormat.parse(value);
		} catch (ParseException e) {
			assert(false);
			e.printStackTrace();
		}
		return simpleDateFormat.getCalendar();
	}

	public void SetOnCalendar(Calendar calendar){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		simpleDateFormat.setCalendar(calendar);
		SetOnCalendar(simpleDateFormat);
	}
}
