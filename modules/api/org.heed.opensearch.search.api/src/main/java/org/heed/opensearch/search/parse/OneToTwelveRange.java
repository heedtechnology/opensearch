package org.heed.opensearch.search.parse;

public class OneToTwelveRange extends Range {
	public static final OneToTwelveRange instance = new OneToTwelveRange();
	
	private OneToTwelveRange() {
		super(1, 12);
	}
}
