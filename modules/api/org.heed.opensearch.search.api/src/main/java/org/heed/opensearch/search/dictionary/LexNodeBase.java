package org.heed.opensearch.search.dictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by babar on 2/3/15.
 */
public class LexNodeBase<Data> {
    protected char value;
    protected LexNodeBase parent;
    protected Vector<LexNodeBase<Data>> children;
    protected boolean caseSensitive = false;
    private List<Data> datas = new ArrayList<Data>();

    public LexNodeBase() {
        children = new Vector<LexNodeBase<Data>>();
    }

    public LexNodeBase(char value, LexNodeBase<Data> parent, boolean isCaseSensitive) {
        caseSensitive = isCaseSensitive;
        this.parent = parent;
        this.value = value;
    }

    public char getValue() {
        return value;
    }

    public String getDefinedValue() {
        LexNodeBase node = this;
        StringBuffer value = new StringBuffer();
        while(node.getValue() != '\u0000') {
            value.append(node.getValue());
            node = node.getParent();
        }
        return value.reverse().toString();
    }

    public boolean childExists(char value) {
        for(int i=0; i < children.size(); i++) {
            if(children.get(i).getValue() == value) return true;
        }
        return false;
    }

    public LexNodeBase getChild(char value) {
        for(int i=0; i < children.size(); i++) {
            LexNodeBase child = children.get(i);
            if(child.match(value))
                return child;
        }
        return null;
    }

    public boolean match(char value) {
        if(!isCaseSensitive()) {
            if(Character.toUpperCase(getValue()) == value || Character.toLowerCase(getValue()) == value)
                return true;
        } else {
            if(getValue() == value) return true;
        }
        return false;
    }

    public LexNodeBase<Data> addChild(char value, boolean isCaseSensitive) {
        return  addChild(value, isCaseSensitive, null);
    }

    public LexNodeBase<Data> addChild(char value, boolean isCaseSensitive, Data data) {
        LexNodeBase<Data> node = new LexNodeBase<Data>(value, this, isCaseSensitive);
        children.add(node);
        if(data != null) node.addData(data);
        return node;
    }

    public Vector<LexNodeBase<Data>> getChildren() {
        return children;
    }

    public LexNodeBase getParent() {
        return parent;
    }

    public void setParent(LexNodeBase<Data> parent) {
        this.parent = parent;
    }

    public boolean hasData() {
        return datas.size() > 0;
    }

    public void addData(Data data) {
        datas.add(data);
    }

    public List<Data> getDatas() {
        return datas;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }

    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    public boolean equals(Object obj) {
        if(obj instanceof LexNodeBase && ((LexNodeBase)obj).getValue() == value) return true;
        return false;
    }
}
