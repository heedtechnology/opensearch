package org.heed.opensearch.search.parse;

import java.util.Calendar;
import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.functors.Map;

public final class DateComponentsToCalendarParserMap implements
		Map<DateComponentValue, Parser<Calendar>> {
	private final Parser<Calendar> nextDateComponentParser;

	public DateComponentsToCalendarParserMap(Parser<Calendar> nextDateValueParser) {
		super();
		this.nextDateComponentParser = nextDateValueParser;
	}

	@Override
	public Parser<Calendar> map(final DateComponentValue firstDateComponent) {
		return nextDateComponentParser.map(new Map<Calendar, Calendar>() {

			@Override
			public Calendar map(Calendar calendar) {
				firstDateComponent.SetOnCalendar(calendar);
				return calendar;
			}

		});
	}
}
