package org.heed.opensearch.search.parse;

import org.codehaus.jparsec.*;
import org.codehaus.jparsec.functors.*;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.pattern.CharPredicates;
import org.codehaus.jparsec.pattern.Pattern;
import org.codehaus.jparsec.pattern.Patterns;

import java.text.SimpleDateFormat;
import java.util.*;

class DateParsers {
	private static final String YearFormat = "yyyy";
	private static final String DayFormat = "mm";
	private static final String WeekDayFormat = "EEEE";
	private static final String MonthFormat = "MMM";

	public static final String[] monthsArray = new String[] { "january",
			"february", "march", "april", "may", "june", "july", "august",
			"september", "october", "november", "december" };

	static final String[] months = addArrays(monthsArray,
			monthAbbreviation(monthsArray));

	static final String dateFieldName = "date";

	static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyy");

	static final String[] monthDaySuffixArray = new String[] { "st", "nd",
			"rd", "th" };

	static final String[] weekdays = new String[] { "monday", "tuesday",
			"wednesday", "thursday", "friday", "saturday", "sunday" };
	static final String[] emptyStringArray = new String[0];

	static final String[] dateQueryKeywords = new String[] { "today",
			"yesterday", "days", "week", "weeks", "month", "months", "year",
			"years", "ago", "last", "next", "before", "after", "prior", "to",
			"between", "and" };

	static Parser<Date> textDateParser() {
		Parser<Void> dateDelimiter = Parsers.or(Scanners.isChar(','),
				Scanners.isChar(':'), Scanners.WHITESPACES);

		final Pattern[] monthPatternArray = new Pattern[months.length];
		final Pattern[] weekDayPatternArray = new Pattern[weekdays.length];

		for (int i = 0; i < monthPatternArray.length; i++) {
			monthPatternArray[i] = Patterns.string(months[i]);
		}

		for (int i = 0; i < weekDayPatternArray.length; i++) {
			weekDayPatternArray[i] = Patterns.string(weekdays[i]);
		}

		final Parser<DateComponentValue> monthParser = Scanners
				.pattern(Patterns.or(monthPatternArray), "month").source()
				.map(new DateComponentValueMap(MonthFormat));

		final Parser<DateComponentValue> weekdayParser = Scanners
				.pattern(Patterns.or(weekDayPatternArray), "weekday").source()
				.map(new DateComponentValueMap(WeekDayFormat));

		final Parser<DateComponentValue> monthDayParser = Scanners
				.pattern(monthDayPattern(), "monthDay").source().peek()
				.next(new OrdinalMonthDayParserMap())
				.map(new DateComponentValueMap(DayFormat));

		final Parser<DateComponentValue> yearParser = Scanners
				.pattern(Patterns.isChar(CharPredicates.IS_DIGIT).repeat(4),
						"year").source()
				.map(new DateComponentValueMap(YearFormat));
		
		return monthParser.followedBy(dateDelimiter.optional()).ifelse(nextDateComponentParser(
        		monthDayParser.followedBy(dateDelimiter.optional()).ifelse(nextDateComponentParser(
        				yearParser.map(new LastDateComponentToCalendarMap())),
        				yearParser.followedBy(dateDelimiter.optional()).next(nextDateComponentParser(monthDayParser.map(new LastDateComponentToCalendarMap())))
        		)),
        		monthDayParser.followedBy(dateDelimiter.optional()).ifelse(nextDateComponentParser(
        				monthParser.followedBy(dateDelimiter.optional()).ifelse(
        						yearParser.map(new LastDateComponentToCalendarMap()),
        						yearParser.followedBy(dateDelimiter.optional()).next(nextDateComponentParser(monthParser.map(new LastDateComponentToCalendarMap())))
        				)),
        				yearParser.followedBy(dateDelimiter.optional()).next(nextDateComponentParser(
        						monthParser.followedBy(dateDelimiter.optional()).ifelse(
        								monthDayParser.map(new LastDateComponentToCalendarMap()),
        								monthDayParser.followedBy(dateDelimiter.optional()).next(monthParser.map(new LastDateComponentToCalendarMap()))
        						)
        				))
        		)
       ).map(new CalendarToDateMap());
	}

	private static Pattern monthDayPattern() {
		return Patterns.or(Patterns.sequence(
				Patterns.isChar(CharPredicates.range('0', '2')).optional(),
				Patterns.isChar(CharPredicates.range('1', '9'))), Patterns
				.string("10"), Patterns.string("20"), Patterns.string("30"),
				Patterns.string("31"));
	}

	static DateComponentsToCalendarParserMap nextDateComponentParser(
			Parser<Calendar> nextDateValueParser) {
		return new DateComponentsToCalendarParserMap(nextDateValueParser);
	}

	static Parser<DateComponents> numericDateParser() {

		char[] numericDateDelimters = new char[] { ',', '\\', '/', '-', ':' };

		final Parser<Void> monthParser = Scanners.pattern(Patterns.or(
				Patterns.sequence(Patterns.isChar('0'),
						Patterns.isChar(CharPredicates.range('1', '9'))),
				Patterns.stringCaseInsensitive("10"),
				Patterns.stringCaseInsensitive("11"),
				Patterns.stringCaseInsensitive("12")), "numericMonth");

		final Parser<Void> yearParser = Scanners
				.pattern(Patterns.isChar(CharPredicates.IS_DIGIT).some(4),
						"numericYear");
		final Parser<Void> dayParser = Scanners.pattern(monthDayPattern(),
				"numericMonthDay");

		LinkedList<Parser<DateComponents>> parsers = new LinkedList<Parser<DateComponents>>();
				
		for (char c : numericDateDelimters) {
			parsers.add(numericDateSequence(dayParser, monthParser,
					yearParser, Scanners.isChar(c)));
			
			parsers.add(numericDateSequence(yearParser, monthParser,
					dayParser, Scanners.isChar(c)));
			
			parsers.add(numericDateSequence(monthParser, dayParser,
					yearParser, Scanners.isChar(c)));
		}

		return Parsers.or(parsers);
	}

	private static Parser<DateComponents> numericDateSequence(
			Parser<Void> firstDateComponentParser,
			Parser<Void> secondDateComponentParser,
			Parser<Void> thirdDateComponentParser, Parser<Void> delimeter) {

		return Parsers.sequence(firstDateComponentParser.source().followedBy(delimeter),
				secondDateComponentParser.source().followedBy(delimeter),
				thirdDateComponentParser.source().followedBy(delimeter),
				new Map3<String, String, String, DateComponents>() {

					@Override
					public DateComponents map(String firstDateComponent, String secondDateComponent, String thirdDateComponent) {
						int[] dateComponents = new int [] {
								Integer.parseInt(firstDateComponent),
								Integer.parseInt(secondDateComponent),
								Integer.parseInt(thirdDateComponent)
						};
						
						int oneToTwelveRangeIndex = -1;
						int oneToThirtyOneRangeIndex = -1;
						int yearIndex =-1;

						final OneToTwelveRange oneToTwelveRange = OneToTwelveRange.instance;
						final OneToThirtyOneRange oneToThirtyOneRange = OneToThirtyOneRange.instance;
						
						for (int i = 0; i < dateComponents.length; i++) {
							if(oneToTwelveRangeIndex == -1 && oneToTwelveRange.isInRange(dateComponents[i]))
								oneToThirtyOneRangeIndex = i;
							else if (oneToThirtyOneRangeIndex == -1 && oneToThirtyOneRange.instance.isInRange(dateComponents[i]))
								oneToThirtyOneRangeIndex = i;
							else yearIndex =i;
						}
						
						return new DateComponents(
								new RangeItem<OneToTwelveRange>(oneToTwelveRange, oneToTwelveRange.getValueIndex(dateComponents[oneToTwelveRangeIndex])),
								new RangeItem<OneToThirtyOneRange>(oneToThirtyOneRange, oneToThirtyOneRange.getValueIndex(dateComponents[oneToThirtyOneRangeIndex])),
								dateComponents[yearIndex]);
					}
				});
	}

	private static String[] monthAbbreviation(String[] monthsArray) {
		String[] abbreviations = new String[monthsArray.length];

		for (int i = 0; i < monthsArray.length; i++)
			abbreviations[i] = monthsArray[i].substring(0, 3);

		return abbreviations;
	}

	static String[] addArrays(String[]... stringArrays) {

		int length = 0;

		for (String[] stringArray : stringArrays)
			length += stringArray.length;

		String[] result = new String[length];

		length = 0;

		for (String[] stringArray : stringArrays) {
			System.arraycopy(stringArray, 0, result, length, stringArray.length);
			length += stringArray.length;
		}

		return result;
	}

	static String lookUpOrdinalNumberSuffix(int i) {

		final String[] suffixArray = new String[] { "th", "st", "nd", "rd",
				"th", "th", "th", "th", "th", "th" };

		switch (i % 100) {
		case 11:
		case 12:
		case 13:
			return "th";
		default:
			return suffixArray[i % 10];
		}
	}

	private static final class OrdinalMonthDayParserMap implements
			Map<String, Parser<String>> {
		@Override
		public Parser<String> map(String from) {
			return Parsers.constant(from).followedBy(
					Parsers.constant(lookUpOrdinalNumberSuffix(Integer.parseInt(from)))
							.optional());
		}
	}

	private static final class CalendarToDateMap implements Map<Calendar, Date> {
		@Override
		public Date map(Calendar from) {
			return from.getTime();
		}
	}
}
