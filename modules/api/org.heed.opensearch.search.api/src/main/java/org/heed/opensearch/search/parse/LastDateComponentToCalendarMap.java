package org.heed.opensearch.search.parse;

import java.util.Calendar;
import org.codehaus.jparsec.functors.Map;

public final class LastDateComponentToCalendarMap implements Map<DateComponentValue, Calendar> {

	@Override
	public Calendar map(DateComponentValue lastComponentValue) {
		return lastComponentValue.SetOnCalendar();
	}
}
