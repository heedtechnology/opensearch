package org.heed.opensearch.search.parse;

public final class OneToThirtyOneRange extends Range {

	public static final OneToThirtyOneRange instance = new OneToThirtyOneRange(); 
	
	private OneToThirtyOneRange() {
		super(1,32);
	}
}
