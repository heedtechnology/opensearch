package org.heed.opensearch.search.parse;

public class Range {
	private final int from;
	private final int length;

	Range(int from, int length) {
		super();
		this.from = from;
		this.length = length;
	}

	public int get(int index) {
		if (isIndexInRange(index))
			throw new IndexOutOfBoundsException();
		return from + index;
	}

	public boolean isInRange(int value){
		return value >= from &&  value <= from + length;
	}
	
	private boolean isIndexInRange(int index) {
		return index >= 0 && index < length;
	}

	public int getValueIndex(int value) {
		return value - from;
	}
}
