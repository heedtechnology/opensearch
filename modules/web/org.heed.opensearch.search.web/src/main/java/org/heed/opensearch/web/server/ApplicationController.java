package org.heed.opensearch.web.server;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.security.Role;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.security.User;
import org.heed.openapps.theme.ThemeVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


@Controller
public class ApplicationController {
	@Autowired protected SecurityService securityService;
	
	@RequestMapping(value="", method = RequestMethod.GET)
    public ModelAndView manager(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("smartclient");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		if(user.isGuest()) return new ModelAndView(new RedirectView("/login"));
		parms.put("templates", getTemplates());
		parms.put("user", getUser(user));
		parms.put("user_roles", getUserRoles(user));
    	return new ModelAndView("home").addAllObjects(parms);
	}
	
	protected String getTemplates() {
		StringBuffer out = new StringBuffer("{");
		File templateDir = new File("configuration/templates/processing");
		for(File template : templateDir.listFiles()) {
			if(!template.isDirectory() && !template.getName().equals("execution_jsoup.groovy")) {
				out.append("'"+template.getName()+"':'"+template.getName().replace(".groovy","")+"',");
			}
		}
		if(out.length() > 2) return out.substring(0, out.length()-1).toString()+"}";
		else return "{}";
	}
	protected String getUser(User user) {
		StringBuffer out = new StringBuffer();
		out.append("{");
		out.append("'username':'"+user.getUsername()+"',");
		if(user.getFullName() != null) out.append("'fullname':'"+user.getFullName()+"',");
		out.replace(out.length()-1, out.length(), "}");
		if(out.length() > 2) return out.toString();
		else return "{}";
	}
	protected String getUserRoles(User user) {
		StringBuffer out = new StringBuffer();
		out.append("[");
		for(Role role : user.getRoles()) {
			String role_name = role.getName();
			out.append("'"+role_name+"',");
		}
		out.replace(out.length()-1, out.length(), "]");
		if(out.length() > 2) return out.toString();
		else return "[]";
	}
}
