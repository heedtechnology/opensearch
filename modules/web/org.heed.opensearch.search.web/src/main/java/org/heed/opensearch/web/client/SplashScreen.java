package org.heed.opensearch.web.client;

import com.smartgwt.client.types.ImageStyle;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.layout.VLayout;

public class SplashScreen extends VLayout {
	private Img loadingImage;
	
	public SplashScreen() {
		setWidth100();
        setHeight100();
        setBorder("1px solid #A6ABB4");
                
        loadingImage = new Img("/theme/images/loading.gif", 100, 100);  
        loadingImage.setImageType(ImageStyle.NORMAL); 
        loadingImage.setLeft(550);
        loadingImage.setTop(300);
        loadingImage.setVisible(true);
        addChild(loadingImage);
	}
	
	public void loading() {
		loadingImage.show();
		show();
	}
}
