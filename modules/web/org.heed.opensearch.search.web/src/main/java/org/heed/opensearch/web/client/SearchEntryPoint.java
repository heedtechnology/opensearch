package org.heed.opensearch.web.client;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.opensearch.web.client.search.SearchModule;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.VLayout;


public class SearchEntryPoint extends VLayout implements EntryPoint {
	private static final int height = 900;
	private static final int width = 1200;
			
	private SearchModule searchModule;
	//private BrowserWindow browserWindow;
	
	
	public void onModuleLoad() {
		setHeight(height);  
        setWidth(width);
        		
        Canvas canvas = new Canvas();
        addMember(canvas);
                
        searchModule = new SearchModule();
        canvas.addChild(searchModule);
        
        //browserWindow = new BrowserWindow();
        
        setHtmlElement(DOM.getElementById("gwt"));
        setPosition(Positioning.RELATIVE);
        draw();
        
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.CRAWLING_VIEW)) {
	        		searchModule.hide();
	        		
	        	} else if(event.isType(EventTypes.SEARCH_VIEW)) {
	        		
	        		searchModule.show();
	        	} else if(event.isType(SearchEventTypes.DETAIL)) {
	        		//browserWindow.setUrl(event.getRecord().getAttribute("url"));
	        		//browserWindow.show();
	        		String url = event.getRecord().getAttribute("url");
	        		com.google.gwt.user.client.Window.open(url, "_blank", null);
	        	}
	        }
        });
        Record record = new Record();
        record.setAttribute("query", "");
        EventBus.fireEvent(new OpenAppsEvent(EventTypes.SEARCH, record));
	}
		
}
