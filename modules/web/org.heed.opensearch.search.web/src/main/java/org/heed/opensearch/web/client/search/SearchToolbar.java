package org.heed.opensearch.web.client.search;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.opensearch.web.client.EventTypes;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;


public class SearchToolbar extends Toolbar {
	private ImgButton addButton;
	private ImgButton deleteButton;
	private ImgButton pauseButton;
	private ImgButton startButton;
	private SelectItem viewItem;
	private SelectItem frequencyItem;
	private SelectItem soundItem;
	private CheckboxItem highlightItem;
	
	
	public SearchToolbar() {
		super(30);
		setBorder("1px solid #A7ABB4");
		
        SearchForm searchForm = new SearchForm();
        searchForm.setWidth(500);
        //searchForm.setMargin(5);
        addToLeftCanvas(searchForm);
        
				
		addButton("crawl", "/theme/images/icons32/crawl.png", "Crawling", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.CRAWLING_VIEW));
			}  
		});
		
		/*
		addButton("delete", "/theme/images/icons32/delete.png", "Delete", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.DELETE));
			}  
		});
		*/
	}
	public void select(Record record) {
		String localName = record.getAttribute("localName");
		String status = record.getAttribute("status");
		if(localName.equals("def")) {
			if(status.equals("paused")) {
				pauseButton.hide();
				startButton.show();
			} else if(status.equals("running")) {
				pauseButton.show();
				startButton.hide();
			} else {
				startButton.show();
			}
			addButton.show();
			deleteButton.show();
		} else if(localName.equals("url")) {
			pauseButton.hide();
			startButton.hide();
			addButton.hide();
			deleteButton.hide();
		}
	}
	public String getView() {
		return viewItem.getValueAsString();
	}
	public boolean getHightlight() {
		return highlightItem.getValueAsBoolean();
	}
	public String getFrequency() {
		return frequencyItem.getValueAsString();
	}
	public String getSound() {
		return soundItem.getValueAsString();
	}
	
}
