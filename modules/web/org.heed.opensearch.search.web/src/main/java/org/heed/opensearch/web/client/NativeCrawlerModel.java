package org.heed.opensearch.web.client;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeCrawlerModel {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getTemplates() { 
		JavaScriptObject obj = getNativeTemplates();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeTemplates() /*-{
        return $wnd.templates;
	}-*/;
}
