package org.heed.opensearch.web.client.search;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;

public class SearchForm extends DynamicForm {

	
	
	public SearchForm() {
		setWidth(800);
		setNumCols(6);
		setMargin(1);
		
		final TextItem queryItem = new TextItem("query");
		queryItem.setShowTitle(false);
		queryItem.setWidth(300);
		//queryItem.setColSpan(2);
		queryItem.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					Record record = new Record();
					if(queryItem.getValue() != null) record.setAttribute("query", queryItem.getValueAsString());
					else record.setAttribute("query", "");
					EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.SEARCH, record));
				}				
			}
			
		});
		
		
		ButtonItem submitItem = new ButtonItem("submit", "Search");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
				
		submitItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = new Record();
				if(queryItem.getValue() != null) record.setAttribute("query", queryItem.getValueAsString());
				else record.setAttribute("query", "");
				EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.SEARCH, record));
			}			
		});
		
		setFields(queryItem,submitItem);
	}
}
