package org.heed.opensearch.web.client;

public class EventTypes extends org.heed.openapps.gwt.client.EventTypes {
		
	public static final int UPDATE = 100;
	public static final int VIEW = 101;
	public static final int PAUSE = 102;
	
	public static final int SELECT_CRAWLURL = 105;
	public static final int START = 106;
	public static final int VIEW_HITS = 107;
	public static final int CHANGE_SOUND = 108;
	
	public static final int CRAWLING_VIEW = 120;
	public static final int ADD_DEFINITION = 121;
	public static final int SELECT_DEFINITION = 122;
	public static final int UPDATE_DEFINITION = 123;
	
	public static final int SEARCH_VIEW = 130;
}
