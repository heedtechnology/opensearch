<%@ page contentType="text/xml" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
response.setContentType("text/xml");
response.setHeader("Cache-Control", "no-cache");
response.setHeader("pragma","no-cache");
%>
<response>
	<status><c:out value="${status}" /></status>
	<message></message>
	<data>
		<c:forEach items="${crawls}" var="crawl">
			<node id="<c:out value="${crawl.id}" />" localName="def" parent="null" >
				<status><c:out value="${crawl.status}" /></status>
				<type><c:out value="${crawl.type}" /></type>
				<url><c:out value="${crawl.url}" /></url>
				<start><c:out value="${crawl.start}" /></start>
				<end><c:out value="${crawl.end}" /></end>
				<rule><c:out value="${crawl.rule}" /></rule>
				<template><c:out value="${crawl.template}" /></template>
				<trailer><c:out value="${crawl.trailer}" /></trailer>
			</node>
			<c:forEach items="${crawl.urls}" var="url">
				<node id="<c:out value="${url.id}" />" localName="url" parent="<c:out value="${crawl.id}" />" isFolder="false">
					<url><c:out value="${url.url}" /></url>
					<status><c:out value="${url.status}" /></status>
					<title><c:out value="${url.title}" /></title>
					<summary><c:out value="${url.summary}" /></summary>
					<lastCrawl><c:out value="${url.lastCrawlString}" /></lastCrawl>
				</node>
			</c:forEach>
		</c:forEach>
	</data>
</response>