package org.heed.opensearch.crawling.client.crawling;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class DocumentDS extends RestDataSource {
	private static DocumentDS instance = null;  
	  
    public static DocumentDS getInstance() {  
        if (instance == null) {  
            instance = new DocumentDS("documentDS");  
        }  
        return instance;  
    }
    
	public DocumentDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Name");
        
        Map<String,Object> criteria = new HashMap<String,Object>();
		criteria.put("qname", "{openapps.org_search_1.0}document");
		setDefaultParams(criteria);

        List<DataSourceTextField> fields = new ArrayList<DataSourceTextField>();
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        fields.add(idField);
        
        DataSourceTextField nameField = new DataSourceTextField("name", "Name");    
        fields.add(nameField);
        
        DataSourceTextField urlField = new DataSourceTextField("url", "URL");
        fields.add(urlField);
        
        DataSourceTextField statusField = new DataSourceTextField("status", "Status");
        LinkedHashMap<String,String> status = new LinkedHashMap<String,String>();
        status.put("active", "Active");
        status.put("removed", "Removed");
        status.put("deleted", "Deleted");
        statusField.setValueMap(status);
        fields.add(statusField);
                
        DataSourceTextField[] fitems = new DataSourceTextField[fields.size()];
		setFields(fields.toArray(fitems));
        
        setAddDataURL("/service/entity/create.json");
		setFetchDataURL("/service/entity/search.json");
		setRemoveDataURL("/service/entity/remove.json");
		setUpdateDataURL("/service/entity/update.json");
	}
	
}
