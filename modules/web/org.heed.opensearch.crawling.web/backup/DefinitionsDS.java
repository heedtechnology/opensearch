package org.heed.opensearch.crawling.client.crawling;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class DefinitionsDS extends RestDataSource {
	private static DefinitionsDS instance = null;  
	  
    public static DefinitionsDS getInstance() {  
        if (instance == null) {  
            instance = new DefinitionsDS("definitionsDS");  
        }  
        return instance;  
    }
    
	public DefinitionsDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Name");
        
        Map<String,Object> criteria = new HashMap<String,Object>();
		criteria.put("qname", "{openapps.org_crawling_1.0}definition");
		setDefaultParams(criteria);

        List<DataSourceTextField> fields = new ArrayList<DataSourceTextField>();
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        fields.add(idField);
        
        DataSourceTextField nameField = new DataSourceTextField("name", "Name");    
        fields.add(nameField);
        
        DataSourceTextField urlField = new DataSourceTextField("url", "URL");
        fields.add(urlField);
        
        DataSourceTextField statusField = new DataSourceTextField("status", "Status");
        LinkedHashMap<String,String> status = new LinkedHashMap<String,String>();
        status.put("paused", "Paused");
        status.put("running", "Running");
        status.put("hit", "Hit");
        statusField.setValueMap(status);
        fields.add(statusField);
        
        DataSourceTextField typeField = new DataSourceTextField("crawl_type", "Type");
        LinkedHashMap<String,String> yesno = new LinkedHashMap<String,String>();
        yesno.put("idrange", "ID Range");
        yesno.put("standard", "Standard");
        typeField.setValueMap(yesno);
        fields.add(typeField);
        
        DataSourceTextField frequencyField = new DataSourceTextField("frequency", "Frequency");
        LinkedHashMap<String,String> frequency = new LinkedHashMap<String,String>();
        frequency.put("demand", "On Demand");
        frequency.put("30seconds", "30 seconds");
        frequency.put("1minute", "1 minute");
        frequency.put("5minutes", "5 minutes");
        frequency.put("10minutes", "10 minutes");
        frequency.put("15minutes", "15 minutes");
        frequency.put("30minutes", "30 minutes");
        frequency.put("hourly", "Hourly");
        frequency.put("daily", "Daily");
        frequencyField.setValueMap(frequency);
        fields.add(frequencyField);
        
        DataSourceTextField crawlField = new DataSourceTextField("crawl_template", "Crawl Template");
        LinkedHashMap<String,String> crawl = new LinkedHashMap<String,String>();
        //crawl.put("idrange", "ID Range");
        //crawl.put("standard", "Standard Crawl");
        crawlField.setValueMap(crawl);
        fields.add(crawlField);
        
        DataSourceTextField processField = new DataSourceTextField("process_template", "Process Template");
        LinkedHashMap<String,String> process = new LinkedHashMap<String,String>();
        process.put("", "");
        process.put("fda", "F.D.A.");
        process.put("genzyme", "Genzyme");
        process.put("lilly", "Eli Lilly");
        processField.setValueMap(process);
        fields.add(processField);
        
        DataSourceTextField[] fitems = new DataSourceTextField[fields.size()];
		setFields(fields.toArray(fitems));
        
        setAddDataURL("/service/entity/create.json");
		setFetchDataURL("/service/entity/search.json");
		setRemoveDataURL("/service/entity/remove.json");
		setUpdateDataURL("/service/entity/update.json");
	}
	
}
