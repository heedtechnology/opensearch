package org.heed.opensearch.crawling.client.crawling;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.opensearch.crawling.client.BrowserWindow;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.types.TitleOrientation;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class CrawlingModule extends VLayout {
	private DateTimeFormat fmt = DateTimeFormat.getFormat("MMM dd yyyy, hh:mm aaa");
	//private NativeCrawlerModel model = new NativeCrawlerModel();
	private static final int width = 1200;
	private DefinitionsDS definitionDS = DefinitionsDS.getInstance();
	private DocumentDS documentDS = DocumentDS.getInstance();
	
	private Toolbar toolbar;
	private ListGrid grid;
	private DynamicForm form1;
	private DynamicForm form2;
	private DynamicForm form3;
	private ListGrid resultsGrid;	
	private BrowserWindow browserWindow;
	
	private List<FormItem> allItems = new ArrayList<FormItem>();
	
	public CrawlingModule() {
		VLayout mainLayout = new VLayout();
		mainLayout.setHeight100();  
		mainLayout.setWidth(width);
		
		toolbar = new Toolbar(30);
		toolbar.setBorder("1px solid #A7ABB4");
		toolbar.setMargin(1);
		mainLayout.addMember(toolbar);
		
		toolbar.addButton("new", "/theme/images/icons32/add.png", "Add Crawl Definition", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				form1.editNewRecord();
				form2.editNewRecord();
			}  
		});
		
		HLayout gridBar = new HLayout();
		gridBar.setWidth100();
		gridBar.setHeight(20);
		gridBar.setBorder("1px solid #A7ABB4");
		gridBar.setBackgroundColor("#FAFAFA");
		mainLayout.addMember(gridBar);
		
		ImgButton refreshButton = new ImgButton();
		refreshButton.setSrc("/theme/images/icons16/refresh.png");
		refreshButton.setPrompt("Refresh");
		refreshButton.setWidth(16);
		refreshButton.setHeight(16);
		refreshButton.setShowDown(false);
		refreshButton.setShowRollOver(false);
		refreshButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				grid.fetchData();
			}			
		});
		gridBar.addMember(refreshButton);
		
		grid = new ListGrid() {
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
                if(fieldName.equals("iconField")) {  
                    HLayout recordCanvas = new HLayout(8);  
                    recordCanvas.setHeight100();
                    recordCanvas.setWidth100();
                    recordCanvas.setAlign(Alignment.CENTER);  
                    
                    ImgButton statusImg = new ImgButton();  
                    statusImg.setShowDown(false);  
                    statusImg.setShowRollOver(false);  
                    statusImg.setLayoutAlign(Alignment.CENTER);  
                    if(record.getAttribute("status") == null || record.getAttribute("status").equals("paused")) {
                    	statusImg.setSrc("/theme/images/icons16/control_play_blue.png");  
                    	statusImg.setPrompt("Start");
                    	statusImg.addClickHandler(new ClickHandler() {  
                            public void onClick(ClickEvent event) {  
                                 //Record record = form.getValuesAsRecord();
                                 //record.setAttribute("id", record.getAttribute("id"));
                                 record.setAttribute("status", "running");
                                 definitionDS.updateData(record);
                            }  
                        });
                    } else {
                    	statusImg.setSrc("/theme/images/icons16/control_pause_blue.png");  
                        statusImg.setPrompt("Pause");
                        statusImg.addClickHandler(new ClickHandler() {  
                            public void onClick(ClickEvent event) {  
                            	//Record record = form.getValuesAsRecord();
                            	//record.setAttribute("id", record.getAttribute("id"));
                                record.setAttribute("status", "paused");
                                definitionDS.updateData(record);
                            }  
                        });
                    }
                    statusImg.setHeight(16);  
                    statusImg.setWidth(16);
                    recordCanvas.addMember(statusImg);
                    
                    ImgButton editImg = new ImgButton();  
                    editImg.setShowDown(false);  
                    editImg.setShowRollOver(false);  
                    editImg.setLayoutAlign(Alignment.CENTER);  
                    editImg.setSrc("/theme/images/icons16/delete.png");  
                    editImg.setPrompt("Delete Definition");  
                    editImg.setHeight(16);  
                    editImg.setWidth(16);  
                    editImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {  
                             removeData(record);
                        }  
                    });
                    recordCanvas.addMember(editImg);
                    return recordCanvas;
                }
                return null;
			}
		};
		grid.addRecordClickHandler(new RecordClickHandler() {  
            public void onRecordClick(RecordClickEvent event) {  
                form1.reset();
                form2.reset(); 
                form1.editSelectedData(grid); 
                form2.editSelectedData(grid);
                try {
        			Record source_associations = event.getRecord().getAttributeAsRecord("source_associations");
        			if(source_associations != null) {
        				Record notes = source_associations.getAttributeAsRecord("documents");
        				if(notes != null) {
        					resultsGrid.setData(notes.getAttributeAsRecordArray("node"));
        				} else resultsGrid.setData(new Record[0]);
        			}
        		} catch(ClassCastException e) {
        			resultsGrid.setData(new Record[0]);
        		}
            }  
        });  
		grid.setWidth100();
		grid.setHeight(350);
		grid.setShowResizeBar(true);
		grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
        grid.setShowFilterEditor(true);
        grid.setFilterByCell(true);
        grid.setFilterOnKeypress(true);
        grid.setAutoFitData(Autofit.VERTICAL);
		grid.setFixedRecordHeights(false);
		grid.setWrapCells(true);
		grid.setDataSource(definitionDS);
		grid.setAutoFetchData(true);
		grid.setFields(getGridItems());
		mainLayout.addMember(grid);
        
		TabSet tabset = new TabSet();
		tabset.setWidth100();
		tabset.setHeight(500);
		mainLayout.addMember(tabset);
		
		Tab editTab = new Tab("Edit");
		VLayout formLayout = new VLayout();
		formLayout.setWidth100();
		formLayout.setHeight100();
		editTab.setPane(formLayout);
		tabset.addTab(editTab);
		
		HLayout topFormLayout = new HLayout();
		topFormLayout.setWidth100();
		formLayout.addMember(topFormLayout);
		
		form1 = new DynamicForm();
		form1.setWidth100();
		form1.setHeight100();
		form1.setMargin(5);
		form1.setNumCols(4);
		form1.setCellPadding(10);
		form1.setDataSource(definitionDS);
		form1.setAutoFetchData(false);
		form1.setFields(getForm1Fields());
		topFormLayout.addMember(form1);
		
		form2 = new DynamicForm();
		form2.setWidth100();
		form2.setHeight100();
		form2.setMargin(10);
		form2.setNumCols(6);
		form2.setCellPadding(5);
		form2.setDataSource(definitionDS);
		form2.setAutoFetchData(false);
		form2.setFields(getForm2Fields());
		topFormLayout.addMember(form2);
		
		form3 = new DynamicForm();
		form3.setWidth100();
		form3.setHeight100();
		form3.setMargin(10);
		form3.setNumCols(8);
		form3.setCellPadding(5);
		form3.setDataSource(definitionDS);
		form3.setAutoFetchData(false);
		form3.setFields(getForm3Fields());
		formLayout.addMember(form3);
		
		Tab resultsTab = new Tab("Results");
		resultsGrid = new ListGrid() {
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
                if(fieldName.equals("iconField")) {  
                    HLayout recordCanvas = new HLayout(8);  
                    recordCanvas.setHeight100();
                    recordCanvas.setWidth100();
                    recordCanvas.setAlign(Alignment.CENTER);
                    
                    ImgButton viewImg = new ImgButton();  
                    viewImg.setShowDown(false);  
                    viewImg.setShowRollOver(false);  
                    viewImg.setLayoutAlign(Alignment.CENTER);  
                    viewImg.setSrc("/theme/images/icons16/zoom.png");  
                    viewImg.setPrompt("View Document");  
                    viewImg.setHeight(16);  
                    viewImg.setWidth(16);  
                    viewImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {
                        	browserWindow.setUrl(record.getAttribute("url"));
                        	browserWindow.show();
                        }  
                    });
                    recordCanvas.addMember(viewImg);
                    
                    ImgButton editImg = new ImgButton();  
                    editImg.setShowDown(false);  
                    editImg.setShowRollOver(false);  
                    editImg.setLayoutAlign(Alignment.CENTER);  
                    editImg.setSrc("/theme/images/icons16/delete.png");  
                    editImg.setPrompt("Delete Definition");  
                    editImg.setHeight(16);  
                    editImg.setWidth(16);  
                    editImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {  
                             //removeData(record);
                        }  
                    });
                    recordCanvas.addMember(editImg);
                    
                    return recordCanvas;
                }
                return null;
			};
		};
		resultsGrid.setWidth100();
		resultsGrid.setHeight100();
		resultsGrid.setMargin(5);
		resultsGrid.setShowRecordComponents(true);          
		resultsGrid.setShowRecordComponentsByCell(true);
		resultsGrid.setShowFilterEditor(true);
		resultsGrid.setFilterByCell(true);
		resultsGrid.setFilterOnKeypress(true);
		resultsGrid.setFixedRecordHeights(false);
		resultsGrid.setWrapCells(true);
		resultsGrid.setDataSource(documentDS);
		resultsGrid.setFields(getResultGridItems());
		
		resultsTab.setPane(resultsGrid);
		tabset.addTab(resultsTab);
		
		browserWindow = new BrowserWindow();
		
        mainLayout.setHtmlElement(DOM.getElementById("gwt"));
        mainLayout.setPosition(Positioning.RELATIVE);
        mainLayout.draw();
               
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	
	        }
        });
        
	}
	
	protected FormItem[] getForm1Fields() {
		List<FormItem> items = new ArrayList<FormItem>();
		
		TextItem name = new TextItem("name", "Name");
		name.setWidth("*");
		name.setColSpan(4);
		items.add(name);
							
		TextAreaItem url = new TextAreaItem("url", "URL");
		url.setWidth("*");
		url.setHeight(40);
		url.setColSpan(4);
		items.add(url);
						
		TextItem trailer = new TextItem("trailer", "URL Suffix");
		trailer.setColSpan(2);
		trailer.setWidth("*");
		//trailer.setRowSpan(2);
		items.add(trailer);
				
		allItems.addAll(items);
		FormItem[] fitems = new FormItem[items.size()];
		return items.toArray(fitems);
	}
	protected FormItem[] getForm2Fields() {
		List<FormItem> items = new ArrayList<FormItem>();
		
		SelectItem frequency = new SelectItem("frequency", "Frequency");
		frequency.setWidth(150);
		//frequency.setColSpan(3);
		frequency.setDefaultValue("daily");
		items.add(frequency);
		
		CheckboxItem domain = new CheckboxItem("domain", "Same Domain");
		domain.setWidth(75);
		domain.setColSpan(3);
		domain.setTitleOrientation(TitleOrientation.LEFT);
		items.add(domain);
		
		SpinnerItem pagesItem = new SpinnerItem("maxpages", "Max Pages");
		pagesItem.setDefaultValue(100);
		pagesItem.setWidth(60);
		items.add(pagesItem);
		
		SpinnerItem depthItem = new SpinnerItem("maxdepth", "Crawl Depth");
		depthItem.setDefaultValue(1);
		depthItem.setWidth(50);
		items.add(depthItem);
		
		SpinnerItem topItem = new SpinnerItem("topN", "Top N");
		topItem.setDefaultValue(25);
		topItem.setWidth(60);
		items.add(topItem);
				
		TextItem startRule = new TextItem("start", "Start Range");
		startRule.setWidth(125);
		items.add(startRule);
		
		TextItem endRule = new TextItem("end", "End Range");
		endRule.setWidth(125);
		items.add(endRule);
		
		allItems.addAll(items);
		FormItem[] fitems = new FormItem[items.size()];
		return items.toArray(fitems);
	}
	protected FormItem[] getForm3Fields() {
		List<FormItem> items = new ArrayList<FormItem>();
		
		SelectItem crawlTemplateItem = new SelectItem("crawl_type", "Crawl Type");
		crawlTemplateItem.setColSpan(3);
		crawlTemplateItem.setWidth(200);
		items.add(crawlTemplateItem);
		
		SelectItem processTemplateItem = new SelectItem("process_type", "Process Type");
		processTemplateItem.setColSpan(3);
		processTemplateItem.setWidth(200);
		items.add(processTemplateItem);
		
		TextAreaItem template1 = new TextAreaItem("crawl_script", "Crawl Script");
		template1.setWidth("*");
		template1.setHeight(200);
		template1.setColSpan(3);
		items.add(template1);
				
		TextAreaItem template2 = new TextAreaItem("process_script", "Process Script");
		template2.setWidth("*");
		template2.setHeight(200);
		template2.setColSpan(3);
		items.add(template2);
		
		ButtonItem submitItem = new ButtonItem("submit", "Save");
		submitItem.setIcon("/theme/images/icons16/disk.png");
		//submitItem.setStartRow(false);
		//submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = getData();
				if(form1.isNewRecord()) {					
					grid.addData(record);
				} else {
					grid.updateData(record);
				}
			}
		});
		items.add(submitItem);
		
		allItems.addAll(items);
		FormItem[] fitems = new FormItem[items.size()];
		return items.toArray(fitems);
	}
	protected ListGridField[] getGridItems() {
		List<ListGridField> items = new ArrayList<ListGridField>();
		
		ListGridField idField = new ListGridField("id", "ID", 35);
		idField.setAlign(Alignment.CENTER);
		idField.setHidden(true);
		items.add(idField);
		
		ListGridField typeField = new ListGridField("crawl_type", "Crawl Type", 85);
		typeField.setAlign(Alignment.CENTER);
		items.add(typeField);
		
		ListGridField processField = new ListGridField("process_type", "Process Type", 85);
		processField.setAlign(Alignment.CENTER);
		items.add(processField);
		
		ListGridField nameField = new ListGridField("name", "Name", 250);
		//monthField.setAlign(Alignment.CENTER);
		items.add(nameField);
		
		ListGridField urlField = new ListGridField("url", "URL");
		items.add(urlField);
		
		ListGridField statusField = new ListGridField("status", "Status");
		statusField.setWidth(75);
		statusField.setAlign(Alignment.CENTER);
		items.add(statusField);
		
		ListGridField lastUpdateField = new ListGridField("last_crawl", "Last Update");
		lastUpdateField.setWidth(135);
		lastUpdateField.setAlign(Alignment.CENTER);
		lastUpdateField.setCanFilter(false);
		lastUpdateField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	String createdStr = record.getAttribute("last_crawl");
            	if(createdStr == null) return "";
            	return fmt.format(new Date(Long.valueOf(createdStr)));
            }  
        });
		items.add(lastUpdateField);
		
		ListGridField nextUpdateField = new ListGridField("next_crawl", "Next Update");
		nextUpdateField.setWidth(135);
		nextUpdateField.setAlign(Alignment.CENTER);
		nextUpdateField.setCanFilter(false);
		nextUpdateField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	String lastCrawlStr = record.getAttribute("last_crawl");
            	String frequencyStr = record.getAttribute("frequency");
            	if(lastCrawlStr != null && frequencyStr != null) {
            		if(frequencyStr.equals("demand")) {
            			return "On Demand";
            		} else {
            			Long last_crawl = Long.valueOf(lastCrawlStr);
            			Long frequency = getFrequencyCount(frequencyStr);            		
            			return fmt.format(new Date(last_crawl + frequency));
            		}
            	}
            	return "";
            }  
        });
		items.add(nextUpdateField);
		
		ListGridField iconField = new ListGridField("iconField", " ", 60);
		iconField.setCanSort(false);
		iconField.setCanFreeze(false);
		iconField.setCanGroupBy(false);
		iconField.setCanFilter(false);
		items.add(iconField);
		
		ListGridField[] fitems = new ListGridField[items.size()];
		return items.toArray(fitems);
	}
	
	protected ListGridField[] getResultGridItems() {
		List<ListGridField> items = new ArrayList<ListGridField>();
		
		ListGridField statusField = new ListGridField("status", "Status");
		statusField.setWidth(85);
		statusField.setAlign(Alignment.CENTER);
		statusField.setDefaultValue("active");
		items.add(statusField);
		
		ListGridField nameField = new ListGridField("name", "Name", 300);
		//monthField.setAlign(Alignment.CENTER);
		items.add(nameField);
		
		ListGridField urlField = new ListGridField("url", "URL");
		items.add(urlField);
		
		ListGridField createdField = new ListGridField("created", "Date Created");
		createdField.setWidth(100);
		createdField.setAlign(Alignment.CENTER);
		createdField.setCanFilter(false);
		items.add(createdField);
		
		ListGridField updatedField = new ListGridField("updated", "Last Update");
		updatedField.setWidth(100);
		updatedField.setAlign(Alignment.CENTER);
		updatedField.setCanFilter(false);
		items.add(updatedField);
		
		ListGridField iconField = new ListGridField("iconField", " ", 60);
		iconField.setCanSort(false);
		iconField.setCanFreeze(false);
		iconField.setCanGroupBy(false);
		iconField.setCanFilter(false);
		items.add(iconField);
		
		ListGridField[] fitems = new ListGridField[items.size()];
		return items.toArray(fitems);
	}
	
	public Record getData() {
		Record record = new Record();
		record.setAttribute("id", form1.getValue("id"));
		for(FormItem item : allItems) {
			if(item.getForm().isVisible() && item.isVisible()) {
				if(item.getValue() != null) record.setAttribute(item.getName(), item.getValue());				
			}
		}
		return record;
	}
	
	protected long getFrequencyCount(String frequency) {
		if(frequency.equals("daily")) return 1000 * 60 * 60 * 24;
		if(frequency.equals("12hours")) return 1000 * 60 * 60 * 12;
		if(frequency.equals("4hours")) return 1000 * 60 * 60 * 4;
		if(frequency.equals("hourly")) return 1000 * 60 * 60;
		if(frequency.equals("30minutes")) return 1000 * 60 * 30;
		if(frequency.equals("15minutes")) return 1000 * 60 * 15;
		if(frequency.equals("10minutes")) return 1000 * 60 * 10;
		if(frequency.equals("5minutes")) return 1000 * 60 * 5;
		if(frequency.equals("1minute")) return 1000 * 60 * 1;
		if(frequency.equals("30seconds")) return 1000 * 30;
		return 0;
	}
}
