package org.heed.opensearch.crawling.client.search;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;


public class PagingDisplay extends HLayout {
	private HTMLFlow paging;
	private HLayout links;
	private HTMLFlow results;
	private HLayout buttons;
	
	//private SelectItem frequencyItem;
	//private SelectItem soundItem;
	//private CheckboxItem highlightItem;
	
	
	public PagingDisplay() {
		setWidth100();
		setHeight(35);
		setBorder("1px solid #A6ABB4");
               
        paging = new HTMLFlow();
        paging.setWidth(230);
        paging.setMargin(5);
        //paging.setBorder("1px solid #A6ABB4");
        addMember(paging);
        
        buttons = new HLayout();
        buttons.setWidth(730);
        //buttons.setBorder("1px solid #A6ABB4");
        buttons.setMargin(5);
        buttons.setAlign(Alignment.CENTER);
        buttons.setAlign(VerticalAlignment.CENTER);
        buttons.setMembersMargin(5);
        IButton first = new IButton();
        first.setIcon("/theme/images/icons16/resultset_first.png");
        first.setWidth(24);
        first.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.PAGING_FIRST));
			}        	
        });
        buttons.addMember(first);
        IButton previous = new IButton();
        previous.setIcon("/theme/images/icons16/resultset_previous.png");
        previous.setWidth(24);
        previous.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.PAGING_PREV));
			}        	
        });
        buttons.addMember(previous);
        
        links = new HLayout();
        links.setWidth(20);
        links.setHeight(20);
        links.setMargin(2);
        links.setMembersMargin(5);
        links.setAlign(Alignment.CENTER);
        //links.setBorder("1px solid #A6ABB4");
        buttons.addMember(links);
                
        IButton next = new IButton();
        next.setIcon("/theme/images/icons16/resultset_next.png");
        next.setWidth(24);
        next.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.PAGING_NEXT));
			}        	
        });
        buttons.addMember(next);                
        IButton last = new IButton();
        last.setIcon("/theme/images/icons16/resultset_last.png");
        last.setWidth(24);
        buttons.addMember(last);
        last.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.PAGING_LAST));
			}        	
        });
        addMember(buttons);
        
        HLayout resultsLayout = new HLayout();
        resultsLayout.setWidth100();
        resultsLayout.setAlign(Alignment.RIGHT);
        results = new HTMLFlow();
        results.setOverflow(Overflow.VISIBLE);
        results.setWidth(230);
        //results.setBorder("1px solid #A6ABB4");
        results.setMargin(5);
        resultsLayout.addMember(results);
                
        addMember(resultsLayout);
	}
	
	public void setPagingData(int page, int pageCount, int resultCount, long time) {
		links.removeMembers(links.getMembers());
		paging.setContents("<span style='color:#A6ABB4;font-weight:bold;font-size:16px;width:200px;'>Page "+page+" of "+pageCount+"</span>");
		results.setContents("<span style='float:right;color:#A6ABB4;font-weight:bold;font-size:16px;width:200px;text-align:right;'>"+resultCount+" results in "+time+" ms</span>");
		/*
		int start = page + 1;
		int end = page + 11;
		for(int i=start; i < end; i++) {
			if(pageCount >= i) {
				final HTMLFlow link = new HTMLFlow("<div style='color:#A6ABB4;font-weight:bold;font-size:16px;'>"+i+"</div>");
				link.setWidth(20);
				link.addClickHandler(new PageClickHandler(i));
		        link.addMouseOverHandler(new MouseOverHandler() {
					public void onMouseOver(MouseOverEvent event) {
						link.setStyleName("titleOver");
						link.setCursor(Cursor.HAND);
					}			
				});
		        link.addMouseOutHandler(new MouseOutHandler() {
					public void onMouseOut(MouseOutEvent event) {
						link.setStyleName("titleOut");
					}			
				});
				links.addMember(link);
				
			}
		}
		*/
		paging.show();
		buttons.show();
		results.show();
	}
	public void hidePagingControls() {
		paging.hide();
		buttons.hide();
		results.hide();
	}
	
	public class PageClickHandler implements ClickHandler {
		private int pageNumber;
		
		public PageClickHandler(int pageNumber) {
			this.pageNumber = pageNumber;
		}
		@Override
		public void onClick(ClickEvent event) {
			Record rec = new Record();
			rec.setAttribute("pageNumber", pageNumber);
			EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.PAGING, rec));
		}
		
	}
}
