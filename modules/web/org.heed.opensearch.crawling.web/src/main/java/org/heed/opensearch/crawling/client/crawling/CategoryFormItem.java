package org.heed.opensearch.crawling.client.crawling;

import org.heed.opensearch.crawling.client.crawling.datasource.CrawlingTreeJsonDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tree.TreeGrid;


public class CategoryFormItem extends StaticTextItem {
	private IndustryWindow window;
	
	private String value_id;
	
	public CategoryFormItem(String name, String title) {
		super(name, title);
		setHeight(16);
		setWrap(true);
		FormItemIcon formItemIcon = new FormItemIcon();
        setIcons(formItemIcon);
        
        addIconClickHandler(new IconClickHandler() {
            @Override
            public void onIconClick(IconClickEvent event) {
                window.show();
            }
        });
        window = new IndustryWindow();
	}
	
	public String getValueId() {
		return value_id;
	}
	public void setValueId(String id) {
		this.value_id = id;
	}
	
	public void select(Record record) {
		if(record != null) {
			try {
				Record[] target_associations = record.getAttributeAsRecordArray("target_associations");
				boolean valueSet = false;
				if(target_associations != null) {				
					for(Record assoc_record : target_associations) {
						if(assoc_record.getAttribute("qname").equals("{openapps.org_crawling_1.0}categories")) {
							Record[] sources = assoc_record.getAttributeAsRecordArray("sources");
							if(sources != null && sources.length > 0) {
								String value_id = sources[0].getAttribute("target_id");
								String name = sources[0].getAttribute("name");
								setValue(name);
								setValueId(value_id);
								fireChangedEvent();
								valueSet = true;
							}
						} 
					}
				}
				if(!valueSet) setValue("");
			} catch(ClassCastException e) {
				setValue("");
			}
		}
	}
	
	public class IndustryWindow extends Window {
		private TreeGrid tree;
		private IButton linkItem;
		
		public IndustryWindow() {
			setWidth(400);
			setHeight(400);
			setTitle("Select A Category");
			setAutoCenter(true);
			setIsModal(true);
			
			tree = new TreeGrid();
			tree.setShowHeader(false);
			tree.setMargin(2);
			tree.setDataSource(CrawlingTreeJsonDS.getInstance());
			tree.setSelectionType(SelectionStyle.SINGLE);
			tree.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					linkItem.show();
				}
			});
			addItem(tree);
						
			HLayout layout = new HLayout();
			layout.setMargin(5);
			layout.setHeight(30);
			layout.setWidth100();
			layout.setAlign(Alignment.RIGHT);
			linkItem = new IButton("Link");
			linkItem.setWidth(65);
			linkItem.hide();
			linkItem.setIcon("/theme/images/icons16/link.png");
			linkItem.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					setValueId(tree.getSelectedRecord().getAttribute("id"));
					setValue(tree.getSelectedRecord().getAttribute("name"));
					fireChangedEvent();
					window.hide();
				}
			});
			layout.addMember(linkItem);
			addItem(layout);
		}
		@Override
		public void show() {
			Criteria criteria = new Criteria();
			tree.fetchData(criteria);
			super.show();
		}
	}
	
	private native void fireChangedEvent() /*-{
		var obj = null;
		obj = this.@com.smartgwt.client.core.DataClass::getJsObj()();
		var selfJ = this;
		if(obj.getValue === undefined){
			return;
		}
		var param = {"form" : obj.form, "item" : obj, "value" : obj.getValue()};
		var event = @com.smartgwt.client.widgets.form.fields.events.ChangedEvent::new(Lcom/google/gwt/core/client/JavaScriptObject;)(param);
		selfJ.@com.smartgwt.client.core.DataClass::fireEvent(Lcom/google/gwt/event/shared/GwtEvent;)(event);
	}-*/;
}