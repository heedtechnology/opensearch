package org.heed.opensearch.crawling.client.crawling;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.opensearch.crawling.client.crawling.datasource.CrawlingTreeJsonDS;

import com.google.gwt.user.client.Timer;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.FolderDropEvent;
import com.smartgwt.client.widgets.tree.events.FolderDropHandler;

public class CrawlingTree extends VLayout {
	private NativeContentTypes model = new NativeContentTypes();
	
	private CrawlingModule application;
	private HLayout messageBar;
	private HTMLFlow message;
	private CrawlingTreeGrid tree;
	private VLayout addCategoryDialog;
	private ImgButton deleteCategoryButton;
	
	public CrawlingTree(CrawlingModule application) {
		this.application = application;
				
		HLayout treeToolbar = new HLayout();
		treeToolbar.setHeight(23);
		treeToolbar.setWidth100();
		treeToolbar.setAlign(Alignment.LEFT);
		treeToolbar.setBorder("1px solid #A7ABB4");
		addMember(treeToolbar);
		
		ImgButton addCategoryButton = new ImgButton();
		addCategoryButton.setSrc("/theme/images/icons16/add.png");
		addCategoryButton.setWidth(20);
		addCategoryButton.setHeight(20);
		addCategoryButton.setShowDown(false);
		addCategoryButton.setShowRollOver(false);
		addCategoryButton.setMargin(2);
		addCategoryButton.setPrompt("Add Category");
		addCategoryButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
        	public void onClick(ClickEvent event) {
        		addCategoryDialog.show();
			}        	
        });
		treeToolbar.addMember(addCategoryButton);
		
		deleteCategoryButton = new ImgButton();
		deleteCategoryButton.setSrc("/theme/images/icons16/delete.png");
		deleteCategoryButton.setWidth(20);
		deleteCategoryButton.setHeight(20);
		deleteCategoryButton.setShowDown(false);
		deleteCategoryButton.setShowRollOver(false);
		deleteCategoryButton.setMargin(2);
		deleteCategoryButton.setPrompt("Remove Category");
		deleteCategoryButton.hide();
		deleteCategoryButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
        	public void onClick(ClickEvent event) {
        		if(tree.getSelectedRecord() != null) {
        			Record rec = new Record();
        			String id = tree.getSelectedRecord().getAttribute("target_id");
        			if(id == null) id = tree.getSelectedRecord().getAttribute("id");
        			rec.setAttribute("id", id);
        			//rec.setAttribute("ticket", security.getTicket());
				
        			tree.removeData(rec, new DSCallback() {
        				public void execute(DSResponse response, Object rawData, DSRequest request) {
        					message.setContents("<div style='font-size:11px;font-weight:bold;text-align:center;'>Category deleted successfully</div>");
        					messageBar.show();
        					Timer clearTimer = new Timer() {
        						@Override
        						public void run() {
        							messageBar.hide();
        						}        	
        					};
        					clearTimer.schedule(15 * 1000); //15 seconds
        				}						
        			});
        		}
        	}
        });
		treeToolbar.addMember(deleteCategoryButton);
		
		addCategoryDialog = new VLayout();
		addCategoryDialog.setHeight(80);
		addCategoryDialog.setWidth100();
		addCategoryDialog.setAlign(Alignment.LEFT);
		addCategoryDialog.setBorder("1px solid #A7ABB4");
		addCategoryDialog.hide();
		addMember(addCategoryDialog);
		
		DynamicForm categoryForm = new DynamicForm();
		categoryForm.setWidth100();
		categoryForm.setHeight100();
		categoryForm.setNumCols(3);
		categoryForm.setMargin(5);
		categoryForm.setCellPadding(5);
		categoryForm.setColWidths(50, 75, 150);
		categoryForm.setAlign(Alignment.RIGHT);
		
		final TextItem categoryNameItem = new TextItem("name", "Name");
		categoryNameItem.setWidth("*");
		categoryNameItem.setColSpan(3);
		
		SpacerItem spacer = new SpacerItem();
				
		ButtonItem categorySubmitItem = new ButtonItem("add", "Add Category");
		//submitItem.setIcon("/theme/images/icons16/disk.png");
		categorySubmitItem.setStartRow(false);
		categorySubmitItem.setEndRow(false);
		categorySubmitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record parent = tree.getSelectedRecord();
				Record record = new Record();
				record.setAttribute("name", categoryNameItem.getValue());
				if(parent != null) {
					record.setAttribute("source", parent.getAttribute("id"));
				}					
				tree.addData(record, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Timer clearTimer = new Timer() {
    						@Override
    						public void run() {
    							addCategoryDialog.hide();	
    						}        	
    					};
    					clearTimer.schedule(5 * 1000); //5 seconds											
					}					
				});				
			}
		});
		ButtonItem categoryCancelItem = new ButtonItem("cancel", "Cancel");
		//submitItem.setIcon("/theme/images/icons16/disk.png");
		categoryCancelItem.setStartRow(false);
		categoryCancelItem.setEndRow(false);
		categoryCancelItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				addCategoryDialog.hide();
			}
		});
		categoryForm.setFields(categoryNameItem, spacer, categorySubmitItem, categoryCancelItem);
		addCategoryDialog.addChild(categoryForm);
		
		tree = new CrawlingTreeGrid();
		tree.setWidth100();
		tree.setBorder("1px solid #A7ABB4");
		addMember(tree);
		
		/*
		 * bottom message bar
		 */
		messageBar = new HLayout();
		//messageBar.setHeight(23);
		messageBar.setWidth100();
		messageBar.setAlign(Alignment.LEFT);
		messageBar.setStyleName("messageBar");
		messageBar.setBorder("1px solid #A7ABB4");
		messageBar.hide();
		addMember(messageBar);
		
		message = new HTMLFlow();
		message.setWidth100();message.setPadding(2);
		messageBar.addMember(message);
		
	}
	public void fetchData(Criteria criteria) {
		tree.fetchData(criteria);
	}
	
	public class CrawlingTreeGrid extends TreeGrid {
		private Menu emptyContextMenu;
		private Menu addContextMenu;
		private Menu fullContextMenu;
		
		public CrawlingTreeGrid() {
			setHeight100();
			setWidth100();
			setShowHeader(false);
			setBorder("0px");
			setWrapCells(true);
			setFixedRecordHeights(false);
			setCanReorderRecords(true);  
	        setCanAcceptDroppedRecords(true);
	        setSelectionType(SelectionStyle.SINGLE);
	        CrawlingTreeJsonDS ds = CrawlingTreeJsonDS.getInstance();
	        setDataSource(ds);
	        
	        addCellClickHandler(new CellClickHandler() {
				public void onCellClick(CellClickEvent event) {
					//select(event.getRecord());
					if(event.getRecord().getAttribute("localName").equals("category_root")) {
						application.search(null, 1);
						deleteCategoryButton.hide();
					} else {
						application.search("target_assoc:"+event.getRecord().getAttribute("id"), 1);
						deleteCategoryButton.show();
					}
				}			
			});
	        addFolderDropHandler(new FolderDropHandler() {
	        	public void onFolderDrop(FolderDropEvent event) {        		
	        		TreeNode child = event.getNodes()[0];
	        		TreeNode parent = event.getFolder();
	        		String parentType = parent.getAttribute("localName");
	        		String childType = child.getAttribute("localName"); 
	        		if((parentType.equals("accessions") && childType.equals("accession")) ||
	        				(parentType.equals("categories") && childType.equals("category")) ||
	        				(parentType.equals("categories") && isItem(childType)) ||
	        				(parentType.equals("category") && childType.equals("category")) ||
	        				(parentType.equals("category") && isItem(childType))) {
	        			//System.out.println("drop accepted");
	        			Record record = new Record();
	        			String parentId = parent.getAttribute("id");
	        			if(parentType.equals("accessions") || parentType.equals("categories")) 
	        				parentId = parentId.substring(0,  parentId.length()-11);
	            		record.setAttribute("id", child.getAttribute("id"));
	            		record.setAttribute("parent", parentId);
	            		updateData(record);
	        		} else {
	        			SC.say("sorry, you cannot relocate your item to that folder");    			
	        		}
	        		event.cancel();
				}
	        });     
	        
	        MenuItem addItem = new MenuItem("Add New Item");
	        addItem.addClickHandler(new ClickHandler() {
	        	public void onClick(MenuItemClickEvent event) {
	        		EventBus.fireEvent(new OpenAppsEvent(EventTypes.ADD, new Record()));
				}
	        });
	        MenuItem delItem = new MenuItem("Delete Item");
	        delItem.addClickHandler(new ClickHandler() {
	        	public void onClick(MenuItemClickEvent event) {
	        		EventBus.fireEvent(new OpenAppsEvent(EventTypes.DELETE, new Record()));
				}
	        });
	              
	        addContextMenu = new Menu(); 
	        addContextMenu.setItems(addItem);
	        
	        emptyContextMenu = new Menu();
	        fullContextMenu = new Menu();
		}
		public void select(Record record) {
			if(record != null) {
				String type = record.getAttribute("localName");
				if(type != null) {
					if(type.equals("accessions") || type.equals("categories") || type.equals("collection")) 
						setContextMenu(addContextMenu);
					else if(type.equals("category") || isItem(type)) setContextMenu(fullContextMenu);
					else setContextMenu(emptyContextMenu);
				}
			}
		}
		protected boolean isItem(String type) {
			return model.getContentTypes().containsKey("{openapps.org_repository_1.0}"+type);
		}
	}	
	
}
