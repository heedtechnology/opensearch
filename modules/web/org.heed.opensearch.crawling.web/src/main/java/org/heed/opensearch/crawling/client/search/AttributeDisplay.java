package org.heed.opensearch.crawling.client.search;

import com.smartgwt.client.types.Side;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class AttributeDisplay extends TabSet {

	
	public AttributeDisplay() {
		setTabBarPosition(Side.LEFT);  
		
		Tab collectionTab = new Tab("Attributes");
				
		VLayout attributeLayout = new VLayout();
		attributeLayout.setWidth100();
		attributeLayout.setHeight100();
		
		collectionTab.setPane(attributeLayout);
		addTab(collectionTab);
		
	}
}
