package org.heed.opensearch.crawling.client.crawling.datasource;


public class DefinitionsDS extends DefinitionDS {
	private static DefinitionsDS instance = null;  
	
    public static DefinitionsDS getInstance() {  
        if (instance == null) {  
            instance = new DefinitionsDS("definitionsDS");  
        }  
        return instance;  
    }
    
	public DefinitionsDS(String id) {
		super(id);
        
        setAddDataURL("/service/entity/create.json");
		setFetchDataURL("/service/crawling/definitions.json");
		setRemoveDataURL("/service/entity/remove.json");
		setUpdateDataURL("/service/entity/update.json");
	}
	
	public String getDomain(String url) {
		if(url == null || url.length() < 4) return "";
		try {
			String domain = url.replace("http://", "");
			if(domain != null) {
				if(domain.startsWith("www.")) domain = domain.substring(4);
				if(domain.startsWith("google2.")) domain = domain.replace("google2.", "");
				int index = domain.indexOf("/");
				if(index > -1) domain = domain.substring(0, index);
				return domain;
			}
		} catch(Exception e) {
			//log.error("", e);
		}
		return "";
	}
}
