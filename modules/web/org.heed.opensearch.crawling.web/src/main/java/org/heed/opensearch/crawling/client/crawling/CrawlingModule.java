package org.heed.opensearch.crawling.client.crawling;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.form.TimeItem;
import org.heed.opensearch.crawling.client.EventTypes;
import org.heed.opensearch.crawling.client.LoginWindow;
import org.heed.opensearch.crawling.client.crawling.datasource.DefinitionDS;
import org.heed.opensearch.crawling.client.crawling.datasource.DefinitionsDS;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class CrawlingModule extends VLayout {
	private DefinitionsDS definitionsDS = DefinitionsDS.getInstance();
	private DefinitionDS definitionDS = DefinitionDS.getInstance();
	private DateTimeFormat fmt = DateTimeFormat.getFormat("MMM dd yyyy, hh:mm aaa");
	
	private Toolbar toolbar;
	private CrawlingTree tree;
	private ListGrid grid;
	
	private VLayout formLayout;
	private DynamicForm form1;
	private DynamicForm form2;
	//private DynamicForm form3;
	private DynamicForm searchForm;
	private CategoryFormItem category;
	
	private Label pagingLabel;
		
	private LoginWindow loginWindow;
	
	private SelectItem domain;
		
	private List<FormItem> allItems = new ArrayList<FormItem>();
	
	private int page = 1;
	private int count = 0;
	
	private static final int pageSize = 20;
	
	public CrawlingModule() {
		setHeight100();  
		setWidth100();
		setMembersMargin(1);
		
		toolbar = new Toolbar(30);
		toolbar.setBorder("1px solid #A7ABB4");
		addMember(toolbar);
		
		toolbar.addButton("search", "/theme/images/icons32/find.png", "Search", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.SEARCH_VIEW));
			}  
		});
		
		toolbar.addButton("new", "/theme/images/icons32/add.png", "Add Crawl Definition", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				form1.editNewRecord();
				form2.editNewRecord();
				formLayout.show();
			}  
		});
		
		searchForm = new DynamicForm();
		searchForm.setWidth100();
		//searchForm.setHeight(20);
		//searchForm.setMargin(5);
		searchForm.setNumCols(8);
		//searchForm.setCellPadding(5);
		searchForm.setFields(getSearchFormFields());
		toolbar.addToLeftCanvas(searchForm);
		
		VLayout mainLayout = new VLayout();
		mainLayout.setHeight100();
		mainLayout.setWidth100();
		mainLayout.setMembersMargin(1);
		//mainLayout.setShowResizeBar(true);
		addMember(mainLayout);
		
		HLayout bodyLayout = new HLayout();
		bodyLayout.setWidth100();
		bodyLayout.setHeight100();
		bodyLayout.setMembersMargin(1);
		mainLayout.addMember(bodyLayout);
		
		
		/*
		 * Left side tree
		 */
		tree = new CrawlingTree(this);
		tree.setWidth(300);
		tree.setHeight100();
		tree.setMembersMargin(1);
		bodyLayout.addMember(tree);
		
		
		/*
		 * Main Grid
		 */
		grid = new ListGrid() {
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
                if(fieldName.equals("iconField")) {  
                    HLayout recordCanvas = new HLayout(8);  
                    recordCanvas.setHeight100();
                    recordCanvas.setWidth100();
                    recordCanvas.setAlign(Alignment.CENTER);  
                    
                    ImgButton statusImg = new ImgButton();
                    statusImg.setHeight(16);  
                    statusImg.setWidth(16);
                    statusImg.setShowDown(false);  
                    statusImg.setShowRollOver(false); 
                    statusImg.setLayoutAlign(Alignment.CENTER);  
                    if(record.getAttribute("status") == null || record.getAttribute("status").equals("paused")) {
                    	statusImg.setSrc("/theme/images/icons16/control_play_blue.png");  
                    	statusImg.setPrompt("Start");
                    	statusImg.addClickHandler(new ClickHandler() {  
                            public void onClick(ClickEvent event) {  
                                 //Record record = form.getValuesAsRecord();
                                 //record.setAttribute("id", record.getAttribute("id"));
                                 record.setAttribute("status", "running");
                                 //record.setAttribute("ticket", security.getTicket());
                                 definitionDS.updateData(record, new DSCallback() {
                                	 @Override
                                	 public void execute(DSResponse response, Object rawData, DSRequest request) {
                                		 Timer timer = new Timer() {
                                			 @Override
                                			 public void run() {
                                				 Criteria criteria = new Criteria();
                                				 criteria.addCriteria("t", System.currentTimeMillis());
                                				 grid.fetchData(criteria);		        		
                                			 }        	
                                		 };
                                		 timer.schedule(10000);
                                	 }                      
                                 });
                            }  
                        });
                    } else {
                    	statusImg.setSrc("/theme/images/icons16/control_pause_blue.png");  
                        statusImg.setPrompt("Pause");
                        statusImg.addClickHandler(new ClickHandler() {  
                            public void onClick(ClickEvent event) {  
                            	//Record record = form.getValuesAsRecord();
                            	//record.setAttribute("id", record.getAttribute("id"));
                                record.setAttribute("status", "paused");
                                //record.setAttribute("ticket", security.getTicket());
                                definitionDS.updateData(record);
                            }  
                        });
                    }
                    recordCanvas.addMember(statusImg);
                    
                    ImgButton editImg = new ImgButton();  
                    editImg.setShowDown(false);  
                    editImg.setShowRollOver(false);  
                    editImg.setLayoutAlign(Alignment.CENTER);  
                    editImg.setSrc("/theme/images/icons16/delete.png");  
                    editImg.setPrompt("Delete Definition");  
                    editImg.setHeight(16);  
                    editImg.setWidth(16);  
                    editImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {
                        	Record rec = new Record();
        					String id = record.getAttribute("target_id");
        					if(id == null) id = record.getAttribute("id");
        					rec.setAttribute("id", id);
        					//rec.setAttribute("ticket", security.getTicket());
        					EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
        						public void execute(DSResponse response, Object rawData, DSRequest request) {
        							removeData(record);
        						}						
        					});
                        	
                        }  
                    });
                    recordCanvas.addMember(editImg);
                    
                    return recordCanvas;
                } else if(fieldName.equals("nameField")) {
                	
                	
                }
                return null;
			}
		};
		grid.addRecordClickHandler(new RecordClickHandler() {  
            public void onRecordClick(RecordClickEvent event) {  
                form1.reset();
                form2.reset();
                //form3.reset();
                Record record = grid.getSelectedRecord();                
                
                final Criteria criteria = new Criteria();
                criteria.addCriteria("id", record.getAttribute("id"));
                definitionDS.fetchData(criteria, new DSCallback() {
        			@Override
        			public void execute(DSResponse response, Object rawData, DSRequest request) {
        				if(response.getData() != null && response.getData().length > 0) {
        					select(response.getData()[0]);
        				}
        			}
        		});
                formLayout.show();
            }  
        });
		
		grid.setWidth100();
		grid.setHeight100();
		grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
        //grid.setAutoFitData(Autofit.VERTICAL);
		grid.setFixedRecordHeights(false);
		grid.setWrapCells(true);
		grid.setFields(getGridItems());
		//grid.setDataSource(DefinitionGridDS.getInstance());		
		bodyLayout.addMember(grid);
        
		
		/*
		 * Paging
		 */
		HLayout paging = new HLayout();
		paging.setWidth100();
		paging.setHeight(30);
		paging.setBorder("1px solid #A7ABB4");
		paging.setAlign(Alignment.CENTER);
		mainLayout.addMember(paging);
		
		IButton previous = new IButton();
        previous.setIcon("/theme/images/icons16/resultset_previous.png");
        previous.setWidth(28);
        previous.setHeight(28);
        previous.setMargin(2);
        previous.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		if(page > 1) {
        			page--;
        			search(null, page);
        		}
			}        	
        });
        paging.addMember(previous);
        
        pagingLabel = new Label();
        pagingLabel.setWidth(100);
        pagingLabel.setAlign(Alignment.CENTER);
        paging.addMember(pagingLabel);
                
        IButton next = new IButton();
        next.setIcon("/theme/images/icons16/resultset_next.png");
        next.setWidth(28);
        next.setHeight(28);
        next.setMargin(2);
        next.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		if(page < count) {
        			page++;
        			search(null, page);
        		}
			}        	
        });
        paging.addMember(next);
		
        
        /*
         * Crawl Form
         */
		formLayout = new VLayout();
		formLayout.setWidth100();
		formLayout.setHeight(140);
		formLayout.setBorder("1px solid #A6ABB4");
		formLayout.setVisible(false);
		addMember(formLayout);
		
		HLayout topFormLayout = new HLayout();
		topFormLayout.setWidth100();
		formLayout.addMember(topFormLayout);
		
		form1 = new DynamicForm();
		form1.setWidth100();
		form1.setHeight100();
		form1.setMargin(5);
		form1.setNumCols(4);
		form1.setCellPadding(5);
		form1.setDataSource(definitionDS);
		form1.setAutoFetchData(false);
		form1.setFields(getForm1Fields());
		topFormLayout.addMember(form1);
		
		form2 = new DynamicForm();
		form2.setWidth(450);
		form2.setHeight100();
		form2.setMargin(10);
		form2.setNumCols(2);
		form2.setCellPadding(5);
		form2.setDataSource(definitionDS);
		form2.setAutoFetchData(false);
		form2.setFields(getForm2Fields());
		topFormLayout.addMember(form2);
		
		VLayout buttons = new VLayout();
		buttons.setWidth(100);
		buttons.setMargin(5);
		buttons.setHeight100();
		buttons.setMembersMargin(10);
		//buttons.setBorder("1px solid #A6ABB4");
		buttons.addMembers(getButtons());
		topFormLayout.addMember(buttons);
		/*
		if(security.getUser().hasRole("administrator")) {
			form3 = new DynamicForm();
			form3.setWidth100();
			form3.setHeight100();
			form3.setMargin(10);
			form3.setNumCols(8);
			form3.setCellPadding(5);
			form3.setDataSource(definitionDS);
			form3.setAutoFetchData(false);
			form3.setFields(getForm3Fields());
			formLayout.addMember(form3);
		}
		*/
		
		loginWindow = new LoginWindow();
		
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	
	        }
        });
        Criteria criteria = new Criteria();
        //criteria.addCriteria("ticket", security.getTicket());
        tree.fetchData(criteria);
        search("", 1);
	}
	
	protected void search(String query, final int page) {
		final Criteria criteria = new Criteria();
		
		if(query != null) {
			if(searchForm.getValue("status") != null && searchForm.getValueAsString("status").length() > 0) 
				query += "status:" + searchForm.getValueAsString("status")+" ";
			if(searchForm.getValue("domain") != null && searchForm.getValueAsString("domain").length() > 0) 
				query += "domain:" + searchForm.getValueAsString("domain")+" ";
			if(searchForm.getValue("name") != null && searchForm.getValueAsString("name").length() > 0) 
				query += "name:" + searchForm.getValueAsString("name")+" ";
		}
		if(query != null && query.length() > 0) criteria.addCriteria("query", query.trim());
		criteria.addCriteria("_startRow", (page * pageSize) - pageSize);
		criteria.addCriteria("_endRow", page * pageSize);
		criteria.addCriteria("size", 20);
		criteria.addCriteria("sources", false);
		definitionsDS.fetchData(criteria, new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				int total = response.getTotalRows();
				double ratio = (double)total / pageSize;
				count = (int)(Math.ceil(ratio));
				pagingLabel.setContents("<label style='font-size:12px;font-weight:bold;'>Page "+page+" of "+count+"</label>");
				if(response.getData() != null && response.getData().length > 0) {
					String query = criteria.getAttribute("query");
					if(query == null || query.length() == 0) {
						LinkedHashMap<String,String> domainValues = new LinkedHashMap<String,String>();
						domainValues.put("", "");
						for(Record record : response.getData()) {
							String domain = record.getAttribute("domain");
							if(domain != null && domain.length() > 0 && !domainValues.containsKey(domain))
								domainValues.put(domain, domain);
						}				
						domain.setValueMap(domainValues);
					}
					grid.setData(response.getData());
				} else {
					grid.getField("domain").setValueMap(new LinkedHashMap<String,String>(0));
					grid.setData(new Record[0]);
				}
			}
        	
        });
	}
	protected void select(Record record) {
		 String frequency = record.getAttribute("frequency");
         TimeItem timeItem = (TimeItem)form2.getItem("time1");
         if(frequency != null && frequency.length() > 0) {
         	if(frequency.equals("daily")) {
         		String timeOfDay = record.getAttribute("time1");                	
         		timeItem.setValue(timeOfDay);
         		timeItem.show();
         	} else {
         		timeItem.setValue("");
             	timeItem.hide();
         	}
         } else {
         	timeItem.setValue("");
         	timeItem.hide();
         }
                 
         form1.editRecord(record);
         form2.editRecord(record);
         
         category.select(record);
         //if(security.getUser().hasRole("administrator")) {
         	//form3.editSelectedData(grid);
         //}
	}
	protected FormItem[] getSearchFormFields() {
		List<FormItem> items = new ArrayList<FormItem>();
		
		SelectItem status = new SelectItem("status", "Status");
		status.setWidth(150);
		LinkedHashMap<String,String> values = new LinkedHashMap<String,String>();
		values.put("", "");
		values.put("paused", "Paused");
		values.put("running", "Running");
        status.setValueMap(values);
		items.add(status);
		
		domain = new SelectItem("domain", "Domain");
		domain.setWidth(150);
		items.add(domain);
		
		TextItem name = new TextItem("name", "Name");
		name.setWidth(200);
		name.setShowTitle(false);
		items.add(name);
		
		ButtonItem submitItem = new ButtonItem("search", "Search");
		//submitItem.setIcon("/theme/images/icons16/disk.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				search(null, 1);
			}
		});
		items.add(submitItem);
		
		FormItem[] fitems = new FormItem[items.size()];
		return items.toArray(fitems);
	}
	protected FormItem[] getForm1Fields() {
		List<FormItem> items = new ArrayList<FormItem>();
		
		category = new CategoryFormItem("category", "Category");
		category.setColSpan(3);
		category.setWidth("*");
		category.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				
			}			
		});
		items.add(category);
		
		TextItem name = new TextItem("name", "Name");
		name.setWidth("*");
		name.setColSpan(3);
		items.add(name);
		
		TextAreaItem url = new TextAreaItem("url", "URL");
		url.setWidth("*");
		url.setHeight(50);
		url.setColSpan(3);
		items.add(url);
		
		TextAreaItem description = new TextAreaItem("description", "Description");
		description.setWidth("*");
		description.setHeight(50);
		description.setColSpan(3);
		items.add(description);
		/*			
		TextItem trailer = new TextItem("trailer", "URL Suffix");
		trailer.setColSpan(2);
		trailer.setWidth("*");
		//trailer.setRowSpan(2);
		items.add(trailer);
		*/	
		allItems.addAll(items);
		FormItem[] fitems = new FormItem[items.size()];
		return items.toArray(fitems);
	}
	protected FormItem[] getForm2Fields() {
		List<FormItem> items = new ArrayList<FormItem>();
				
		SelectItem frequency = new SelectItem("frequency", "Frequency");
		frequency.setWidth(150);
		//frequency.setColSpan(3);
		frequency.setDefaultValue("demand");
		frequency.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				String frequency = (String)event.getValue();
		         TimeItem timeItem = (TimeItem)form2.getItem("time1");
		         if(frequency != null && frequency.length() > 0) {
		         	if(frequency.equals("daily"))
		         		timeItem.show();
		         	else
		         		timeItem.hide();
		         }
			}
			
		});
		items.add(frequency);
		
		TimeItem timeOfDayItem = new TimeItem("time1", "Time of Day");
		items.add(timeOfDayItem);
        
		/*
		TextItem startRule = new TextItem("start", "Start Range");
		startRule.setWidth(125);
		items.add(startRule);
		
		TextItem endRule = new TextItem("end", "End Range");
		endRule.setWidth(125);
		items.add(endRule);
		*/
		
		allItems.addAll(items);
		FormItem[] fitems = new FormItem[items.size()];
		return items.toArray(fitems);
	}
	protected FormItem[] getForm3Fields() {
		List<FormItem> items = new ArrayList<FormItem>();
		
		SelectItem crawlTemplateItem = new SelectItem("crawl_type", "Crawl Type");
		crawlTemplateItem.setColSpan(3);
		crawlTemplateItem.setWidth(200);
		items.add(crawlTemplateItem);
				
		allItems.addAll(items);
		FormItem[] fitems = new FormItem[items.size()];
		return items.toArray(fitems);
	}
	protected IButton[] getButtons() {
		List<IButton> items = new ArrayList<IButton>();
		
		IButton submitItem = new IButton("Save");
		submitItem.setIcon("/theme/images/icons16/disk.png");
		submitItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = getData();
				//record.setAttribute("ticket", security.getTicket());
				if(record.getAttributeAsString("url") != null) {
					record.setAttribute("domain", definitionDS.getDomain(record.getAttributeAsString("url")));
				}
				if(form1.isNewRecord()) {
					record.setAttribute("status", "paused");					
					definitionDS.addData(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {								
								grid.addData(response.getData()[0]);
							}
						}						
					});					
				} else {
					definitionDS.updateData(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {								
								Record activity = response.getData()[0];
								if(activity != null) {
									String act_id = activity.getAttribute("id");
									for(Record record : grid.getRecords()) {
										String rec_id = record.getAttribute("id");
										String rec_tgt = record.getAttribute("target_id");
										if(rec_tgt != null && rec_tgt.equals(act_id)) {
											activity.setAttribute("id", rec_id);
											activity.setAttribute("target_id", rec_tgt);
											grid.removeSelectedData();
											grid.addData(activity);
										} else if(rec_id.equals(act_id)){
											grid.removeData(record);
											grid.addData(activity);
										}
									}
									select(activity);
									grid.refreshFields();
								}							
							} else {
								//SC.say("Sorry we were unable to update this Crawl Definition");
								loginWindow.show();
							}							
						}						
					});	
				}
			}
		});
		items.add(submitItem);
		
		IButton cancelItem = new IButton("Cancel");
		cancelItem.setIcon("/theme/images/icons16/delete.png");
		cancelItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				form1.reset();
                form2.reset();
                form1.editSelectedData(grid); 
                form2.editSelectedData(grid);
                //if(security.getUser().hasRole("administrator")) {
                	//form3.editSelectedData(grid);
                //}
                formLayout.hide();
			}
		});
		items.add(cancelItem);
		
		IButton[] fitems = new IButton[items.size()];
		return items.toArray(fitems);
	}
	protected ListGridField[] getGridItems() {
		List<ListGridField> items = new ArrayList<ListGridField>();
		
		ListGridField idField = new ListGridField("id", "ID", 35);
		idField.setAlign(Alignment.CENTER);
		idField.setHidden(true);
		items.add(idField);
		/*
		ListGridField typeField = new ListGridField("crawl_type", "Crawl Type", 85);
		typeField.setAlign(Alignment.CENTER);
		items.add(typeField);
		
		ListGridField statusField = new ListGridField("status", "Status");
		statusField.setWidth(75);
		statusField.setAlign(Alignment.CENTER);
		LinkedHashMap<String,String> status = new LinkedHashMap<String,String>();
        status.put("paused", "Paused");
        status.put("running", "Running");
        //status.put("hit", "Hit");
        statusField.setValueMap(status);
		items.add(statusField);
		
		ListGridField domainField = new ListGridField("domain", "Domain");
		domainField.setWidth(125);
		items.add(domainField);
		*/
		ListGridField domainField = new ListGridField("domain", "Domain", 200);
		items.add(domainField);
		
		ListGridField urlField = new ListGridField("name", "Name");
		items.add(urlField);
				
		ListGridField lastUpdateField = new ListGridField("last_crawl", "Last Update");
		lastUpdateField.setWidth(135);
		lastUpdateField.setAlign(Alignment.CENTER);
		lastUpdateField.setCanFilter(false);
		lastUpdateField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	String createdStr = record.getAttribute("last_crawl");
            	if(createdStr == null) return "";
            	return fmt.format(new Date(Long.valueOf(createdStr)));
            }  
        });
		items.add(lastUpdateField);
		
		ListGridField nextUpdateField = new ListGridField("next_crawl", "Next Update");
		nextUpdateField.setWidth(135);
		nextUpdateField.setAlign(Alignment.CENTER);
		nextUpdateField.setCanFilter(false);
		nextUpdateField.setCellFormatter(new CellFormatter() {  
            public String format(Object value, ListGridRecord record, int rowNum, int colNum) {  
            	String lastCrawlStr = record.getAttribute("last_crawl");
            	String frequencyStr = record.getAttribute("frequency");
            	if(lastCrawlStr != null && frequencyStr != null) {
            		if(frequencyStr.equals("demand")) {
            			return "On Demand";
            		} else {
            			Long last_crawl = Long.valueOf(lastCrawlStr);
            			Long frequency = getFrequencyCount(frequencyStr);            		
            			return fmt.format(new Date(last_crawl + frequency));
            		}
            	}
            	return "";
            }  
        });
		items.add(nextUpdateField);
		
		ListGridField iconField = new ListGridField("iconField", " ", 60);
		iconField.setCanSort(false);
		iconField.setCanFreeze(false);
		iconField.setCanGroupBy(false);
		iconField.setCanFilter(false);
		items.add(iconField);
		
		ListGridField[] fitems = new ListGridField[items.size()];
		return items.toArray(fitems);
	}
	
	public Record getData() {
		Record record = new Record();
		for(FormItem item : allItems) {
			if(item.getForm().isVisible() && item.isVisible()) {
				if(item.getValue() != null) {
					if(item.getName().equals("category"))
						record.setAttribute(item.getName(), ((CategoryFormItem)item).getValueId());
					else
						record.setAttribute(item.getName(), item.getValue());	
				}
			}
		}
		record.setAttribute("crawl_type", "standard");
		if(form1.getValue("id") != null) record.setAttribute("id", form1.getValue("id"));
		return record;
	}
	
	protected long getFrequencyCount(String frequency) {
		if(frequency.equals("daily")) return 1000 * 60 * 60 * 24;
		if(frequency.equals("12hours")) return 1000 * 60 * 60 * 12;
		if(frequency.equals("4hours")) return 1000 * 60 * 60 * 4;
		if(frequency.equals("hourly")) return 1000 * 60 * 60;
		if(frequency.equals("30minutes")) return 1000 * 60 * 30;
		if(frequency.equals("15minutes")) return 1000 * 60 * 15;
		if(frequency.equals("10minutes")) return 1000 * 60 * 10;
		if(frequency.equals("5minutes")) return 1000 * 60 * 5;
		if(frequency.equals("1minute")) return 1000 * 60 * 1;
		if(frequency.equals("30seconds")) return 1000 * 30;
		return 0;
	}
	
}
