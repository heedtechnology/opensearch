package org.heed.opensearch.crawling.client.crawling.datasource;
import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class CrawlingTreeJsonDS extends RestDataSource {
	private static CrawlingTreeJsonDS instance = null;  
	private static DateTimeFormat dateParser = DateTimeFormat.getFormat("yyyy-MM-dd");
	private static DateTimeFormat dateFormatter = DateTimeFormat.getFormat("EEEE MMMM d, yyyy");
	
	
    public static CrawlingTreeJsonDS getInstance() {  
        if (instance == null) {  
            instance = new CrawlingTreeJsonDS("crawlingTreeDS");  
        }  
        return instance;  
    }
    
	public CrawlingTreeJsonDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Name");	        
        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        setFetchDataURL("/service/crawling/categories.json");
        setAddDataURL("/service/crawling/category/add.json");
        setUpdateDataURL("/service/crawling/category/update.json");
        setRemoveDataURL("/service/entity/remove.json");
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		//JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		try {
			if(response.getData() != null && response.getData().length > 0) {
				for(int i=0; i < response.getData().length; i++) {
					Record entity = response.getData()[i];
					if(entity != null) {
						String parent = entity.getAttribute("parent");
						String type = entity.getAttribute("localName");
						if(parent != null && type.equals("accession")) {
							String date = entity.getAttribute("date");
							entity.setAttribute("parent", parent+"-accessions");
							entity.setAttribute("icon", "/theme/images/icons16/drawer.png");
							entity.setAttribute("isFolder", "false");
							entity.setAttribute("title", getFormattedDate(date));
						} else if(parent != null && type.equals("category")) {
							String parentLocalName = entity.getAttribute("parentLocalName");
							if(parentLocalName != null && parentLocalName.equals("collection"))
								entity.setAttribute("parent", parent+"-categories");						
						} else if(type != null && !type.equals("accessions") && !type.equals("categories") && !type.equals("collection")) {
							//entity.setAttribute("isFolder", false);
							entity.setAttribute("icon", "/theme/images/tree_icons/"+type+".png");
						}
					}
				}
			}
		} catch(ClassCastException e) {
				
		}
		super.transformResponse(response, request, data);
	}
	
	public static String getShortFormattedDate(Date in) {
		return dateParser.format(in);
	}
	public static String getFormattedDate(Date in) {
		return dateFormatter.format(in);
	}
	public static Date getDate(String in) {
		try {
			return dateParser.parse(in);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getFormattedDate(String date) {
		try {
			return dateFormatter.format(dateParser.parse(date));
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return "";
	}
}