package org.heed.opensearch.crawling.client.search;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.opensearch.crawling.client.SplashScreen;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Element;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.XMLTools;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.VLayout;


public class SearchModule extends VLayout {
	private DataSource ds = new DataSource();
	
	private SearchToolbar toolbar;
	private SplashScreen splash;
	private ResultsDisplay searchResults;
	private PagingDisplay paging;
	
	private int pageNumber = 1;
	private int pageCount = 1;
	private String query = "";
	
	
	public SearchModule() {
		setWidth100();
		setHeight100();
		setMembersMargin(1);
		
		toolbar = new SearchToolbar();
		addMember(toolbar);
		
		paging = new PagingDisplay();
        addMember(paging);
        
		Canvas canvas = new Canvas();
        addMember(canvas);
        
		splash = new SplashScreen();        
        canvas.addChild(splash);
        
        searchResults = new ResultsDisplay();
        canvas.addChild(searchResults);
        
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(SearchEventTypes.SEARCH)) {
	        		searchResults.hide();
	        		searchResults.reset();
	        		Boolean displaySplash = event.getRecord().getAttributeAsBoolean("splash");
		        	if(displaySplash == null || displaySplash)
		        		splash.loading();
	            	pageNumber = 1;
	            	//boolean sis = event.getRecord().getAttributeAsBoolean("sis");
	            	event.getRecord().setAttribute("size", "10");
	            	event.getRecord().setAttribute("qname", "openapps_org_content_1_0_document");
	            	query = event.getRecord().getAttribute("query") != null ? event.getRecord().getAttribute("query") : "";
	            	
	            	paging.hidePagingControls();
	            	
	            	Record record = event.getRecord();
	            	record.setAttribute("query", query);
	            	//record.setAttribute("ticket", security.getTicket());
	            	get("/service/crawling/document/search.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							String count = XMLTools.selectString(rawData, "//response/data/results/@pageCount");
							if(count != null && !count.equals("")) pageCount = Integer.valueOf(count);
							setRawResultData(query, rawData);
							searchResults.show();
							splash.hide();
						}						
					});
	            		            	
	            } else if(event.isType(SearchEventTypes.PAGING)) {
	            	searchResults.hide();
		        	splash.loading();
	            	pageNumber = event.getRecord().getAttributeAsInt("pageNumber");
	            	Record record = new Record();
					record.setAttribute("query", query);
	            	record.setAttribute("page", pageNumber);
	            	record.setAttribute("size", "10");
	            	get("/service/crawling/document/search.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							setRawResultData(query, rawData);
							splash.hide();
							searchResults.show();
						}						
					});
	            } else if(event.isType(SearchEventTypes.PAGING_NEXT)) {
	            	if(pageNumber < pageCount) {
	            		searchResults.hide();
	            		splash.loading();
	            		pageNumber++;
	            		Record record = new Record();
	            		record.setAttribute("query", query);
	            		record.setAttribute("page", pageNumber);
	            		record.setAttribute("size", "10");
	            		get("/service/crawling/document/search.xml", record, new DSCallback() {
	            			public void execute(DSResponse response, Object rawData, DSRequest request) {
	            				setRawResultData(query, rawData);
	            				splash.hide();
	            				searchResults.show();
	            			}						
	            		});
	            	}
	            } else if(event.isType(SearchEventTypes.PAGING_PREV)) {
	            	if(pageNumber > 1) {
	            		searchResults.hide();
	            		splash.loading();
	            		pageNumber--;
	            		Record record = new Record();
	            		record.setAttribute("query", query);
	            		record.setAttribute("page", pageNumber);
	            		record.setAttribute("size", "10");
	            		get("/service/crawling/document/search.xml", record, new DSCallback() {
	            			public void execute(DSResponse response, Object rawData, DSRequest request) {
	            				setRawResultData(query, rawData);
	            				splash.hide();
	            				searchResults.show();
	            			}						
	            		});
	            	}
	            } else if(event.isType(SearchEventTypes.PAGING_FIRST)) {
	            	searchResults.hide();
		        	splash.loading();
	            	pageNumber = 1;
	            	Record record = new Record();
					record.setAttribute("query", query);
	            	record.setAttribute("page", pageNumber);
	            	record.setAttribute("size", "10");
	            	get("/service/crawling/document/search.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							setRawResultData(query, rawData);
							splash.hide();
							searchResults.show();
						}						
					});
	            } else if(event.isType(SearchEventTypes.PAGING_LAST)) {
	            	searchResults.hide();
		        	splash.loading();
	            	pageNumber = pageCount;
	            	Record record = new Record();
					record.setAttribute("query", query);
	            	record.setAttribute("page", pageNumber);
	            	record.setAttribute("size", "10");
	            	get("/service/crawling/document/search.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							setRawResultData(query, rawData);
							splash.hide();
							searchResults.show();
						}						
					});
	            } else if(event.isType(SearchEventTypes.ATTRIBUTE)) {
	            	searchResults.hide();
		        	splash.loading();
	            	pageNumber = 1;
	            	String attrQuery = event.getRecord().getAttribute("query");
	            	query = query+"//"+attrQuery;
	            	event.getRecord().setAttribute("query", query);
	            	event.getRecord().setAttribute("size", "10");
	            	get("/service/crawling/document/search.xml", event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							String count = XMLTools.selectString(rawData, "//response/data/results/@pageCount");
							pageCount = Integer.valueOf(count);
							setRawResultData(query, rawData);
							searchResults.show();
							splash.hide();
						}						
					});	
	            
	            } else if(event.isType(SearchEventTypes.RESULTS)) {
	            	searchResults.show();
	            	splash.hide();
	            }
	        }
	    });
	}
	public void get(String url, Record record, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url)+"?"+toParameterString(record));
		//builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						String message = XMLTools.selectString(data, "//message");
						DSResponse res = new DSResponse();
						res.setAttribute("message", message);
						DSRequest req = new DSRequest();
						callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public String toParameterString(Record record) {
		String requestData = new String();
		for(String key : record.getAttributes()) {
			if(!key.equals("__ref")) requestData += key+"="+record.getAttribute(key)+"&";
		}
		if(requestData.endsWith("&")) requestData = requestData.substring(0, requestData.length()-1);
		return URL.encode(requestData);
	}
	public void setRawResultData(String query, Object rawData) {
		try {
			JsArray<Element> nodes = ((JavaScriptObject)XMLTools.selectNodes(rawData, "//response/data/results/result")).cast();
			Record[] results = ds.recordsFromXML(nodes);
			if(results != null && results.length > 0) {
				searchResults.setResults(query, results);
				String page = XMLTools.selectString(rawData, "//response/data/request/@page");
				String pageCount = XMLTools.selectString(rawData, "//response/data/results/@pageCount");
				String resultCount = XMLTools.selectString(rawData, "//response/data/results/@resultCount");
				String time = XMLTools.selectString(rawData, "//response/data/results/@time");
				paging.setPagingData(Integer.valueOf(page), Integer.valueOf(pageCount), Integer.valueOf(resultCount), Long.valueOf(time));
			} else {
				searchResults.setResults("", new Record[0]);
			}
		} catch(ClassCastException e) {	}
	}
}
