package org.heed.opensearch.crawling.client.search;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.util.NumberUtility;
import org.heed.opensearch.crawling.client.crawling.datasource.DocumentDS;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.MouseOutEvent;
import com.smartgwt.client.widgets.events.MouseOutHandler;
import com.smartgwt.client.widgets.events.MouseOverEvent;
import com.smartgwt.client.widgets.events.MouseOverHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ResultsDisplay extends VLayout {
	private DocumentDS documentDS = DocumentDS.getInstance();
	private DateTimeFormat fmt = DateTimeFormat.getFormat("MMMM dd, yyyy");
	
	private List<String> dates = new ArrayList<String>();
		
	public ResultsDisplay() {
		setWidth100();
		setHeight100();
		setBorder("1px solid #A6ABB4");
		setOverflow(Overflow.AUTO);
	}
	
	public void setResults(String query, Record[] results) {
		removeMembers(getMembers());
		if(results != null && results.length > 0) {
			for(Record result : results) {
				String score = result.getAttribute("score");
				final String id = result.getAttribute("id");
				String name = result.getAttribute("title");
				String description = result.getAttribute("summary");
				String url = result.getAttribute("url");
				String createdStr = result.getAttribute("timestamp");
				String created = NumberUtility.isLong(createdStr) ? fmt.format(new Date(Long.valueOf(createdStr))) : "";
				String domain = result.getAttribute("domain");
				String content_type = result.getAttribute("content_type");
				
				int idx = dates.indexOf(created);
				if(idx == -1) {
					dates.add(created);
					idx = dates.size()-1;
				}
				
				HLayout display = new HLayout();
				display.setMargin(3);
				display.setWidth100();
				display.setHeight(52);
				display.setBorder("1px solid #A6ABB4");
				if(query == null || query.length() == 0) display.setStyleName("day"+idx);
				addMember(display);
				
				VLayout leftIcons = new VLayout();
				leftIcons.setHeight100();
				leftIcons.setWidth(50);
				leftIcons.setMargin(8);
				leftIcons.setMembersMargin(3);
				display.addMember(leftIcons);
				
				Canvas spacer = new Canvas();
				spacer.setWidth100();
				spacer.setHeight(16);
				leftIcons.addMember(spacer);
				
				/*
				if(query != null && query.length() > 0) {
					Canvas relevance = getRelevanceImage(Integer.valueOf(score));
					leftIcons.addMember(relevance);
				} else {
					Canvas relevance = new Canvas();
					relevance.setWidth100();
					relevance.setHeight(10);
					leftIcons.addMember(relevance);
				}
				*/
				
				Canvas contentType = getContentTypeImage(url, content_type);
				leftIcons.addMember(contentType);
				
				VLayout bodyLayout = new VLayout();				
				display.addMember(bodyLayout);
				
				Canvas createdRow = getCreatedRow(id, url, name, created, domain);
				bodyLayout.addMember(createdRow);
				
				HLayout mainLayout = new HLayout();
				
				bodyLayout.addMember(mainLayout);
				
				VLayout titleDescLayout = new VLayout();
				mainLayout.addMember(titleDescLayout);
				
				Canvas titleRow = getTitleRow(id, url, name, created);
				titleDescLayout.addMember(titleRow);
				
				Canvas descriptionRow = getDescriptionRow(description);
				titleDescLayout.addMember(descriptionRow);
				
				VLayout rightSide = new VLayout();
				rightSide.setHeight(30);
				rightSide.setWidth(30);
				rightSide.setPadding(5);
				rightSide.setAlign(Alignment.CENTER);
				//rightSide.setBorder("1px solid #A6ABB4");
				mainLayout.addMember(rightSide);
				
				Button deleteDocumentButton = new Button("Delete");
				//deleteDocumentButton.setSrc("/theme/images/icons16/remove.png");
				deleteDocumentButton.setWidth(45);
				deleteDocumentButton.setHeight(18);
				deleteDocumentButton.setAlign(Alignment.CENTER);
				//deleteDocumentButton.setShowDown(false);
				//deleteDocumentButton.setShowRollOver(false);
				//deleteDocumentButton.setMargin(2);
				deleteDocumentButton.setPrompt("Delete");
				deleteDocumentButton.addClickHandler(new ClickHandler() {
		        	public void onClick(ClickEvent event) {
		        		Record rec = new Record();
		        		rec.setAttribute("id", id);
		        		documentDS.removeData(rec, new DSCallback() {
							@Override
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								Record record = new Record();
								//record.setAttribute("splash", false);
								EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.SEARCH, record));
							}		        			
		        		});
					}        	
		        });
				rightSide.addMember(deleteDocumentButton);
			}
		} else {
			final HTMLFlow canvas = new HTMLFlow("<div style='font-size:16px;font-weight:bold;text-align:center;'>No results were found...</div>");			
			canvas.setWidth100();
			canvas.setHeight(10);
			canvas.setMargin(250);
			addMember(canvas);
		}
	}
	public void reset() {
		dates.clear();
	}
	
	protected Canvas getContentTypeImage(String url, String type) {
		Canvas canvas = new Canvas();
		canvas.setWidth100();
		canvas.setHeight(35);
		
		String extension = null;
		if(type.equals("application/pdf")) 
			extension = "pdf";
		else if(type.equals("text/html") || type.equals("application/xhtml+xml") || url.endsWith(".html") || url.endsWith(".htm")) 
			extension = "html";
		else if(url.endsWith(".doc") || url.endsWith(".docx")) 
			extension = "doc";
		else if(url.endsWith(".xls") || url.endsWith(".xlsx")) 
			extension = "xls";
		else if(url.endsWith(".txt")) 
			extension = "txt";
		if(extension != null) {
			Img img = new Img("/theme/images/icons32/file_extension/file_extension_"+extension+".png", 32, 32);
			img.setPrompt("Content Type");
			canvas.addChild(img);
		}
		return canvas;
	}
	protected Canvas getRelevanceImage(int normalizedScore) {
		int score = 100;
		if(normalizedScore < 9) score = 8;
		else if(normalizedScore < 17) score = 16;
		else if(normalizedScore < 25) score = 24;
		else if(normalizedScore < 33) score = 32;
		else if(normalizedScore < 41) score = 40;
		else if(normalizedScore < 49) score = 48;
		else if(normalizedScore < 57) score = 56;
		else if(normalizedScore < 64) score = 64;
		else if(normalizedScore < 73) score = 72;
		else if(normalizedScore < 81) score = 80;
		else if(normalizedScore < 89) score = 88;
		else if(normalizedScore < 97) score = 96;
		Canvas canvas = new Canvas();
		canvas.setWidth100();
		Img img = new Img("/theme/images/progress/"+score+"percent.png", 30, 10);
		img.setPrompt(normalizedScore+" / 100");
		canvas.addChild(img);
		return canvas;
	}
	protected Canvas getCreatedRow(final String id, final String url, String name, String created, String domain) {
		HLayout layout = new HLayout();
		layout.setMembersMargin(5);
		final HTMLFlow canvas = new HTMLFlow(created);
		canvas.setWidth100();
		canvas.setHeight(12);
		canvas.setMargin(3);
		layout.addMember(canvas);		
		
		final HTMLFlow domainCanvas = new HTMLFlow("<div style='font-weight:bold;text-align:right;'>"+domain+"</div>");
		domainCanvas.setWidth(125);
		domainCanvas.setHeight(10);
		domainCanvas.setMargin(3);
		domainCanvas.setAlign(Alignment.RIGHT);
		layout.addMember(domainCanvas);
		
		return layout;
	}
	protected Canvas getTitleRow(final String id, final String url, String name, String created) {
		HLayout layout = new HLayout();
		layout.setMembersMargin(5);
		
		final HTMLFlow canvas = new HTMLFlow("<b>"+name+"</b>");
		canvas.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = new Record();
				record.setAttribute("id", id);
				record.setAttribute("url", url);
				EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.DETAIL, record));
			}
		});
		canvas.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				canvas.setStyleName("titleOver");
				canvas.setCursor(Cursor.HAND);
			}			
		});
		canvas.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				canvas.setStyleName("titleOut");
			}			
		});
		canvas.setWidth100();
		canvas.setHeight(10);
		canvas.setMargin(5);
		layout.addMember(canvas);
		
		//canvas.setBorder("1px solid black");
		return layout;
	}
	protected Canvas getDescriptionRow(String name) {
		HLayout layout = new HLayout();
		HTMLFlow canvas = new HTMLFlow("<div style='text-align:justify;'>"+name+"</div>");
		canvas.setWidth(1000);
		canvas.setHeight(10);
		canvas.setMargin(3);
		//canvas.setBorder("1px solid black");
		Canvas leftSpacer = new Canvas();
		leftSpacer.setWidth(25);
		layout.addMember(leftSpacer);
		layout.addMember(canvas);
		return layout;
	}
}
