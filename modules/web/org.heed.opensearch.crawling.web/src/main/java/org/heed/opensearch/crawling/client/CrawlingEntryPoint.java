package org.heed.opensearch.crawling.client;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.opensearch.crawling.client.crawling.CrawlingModule;
import org.heed.opensearch.crawling.client.search.SearchModule;
import org.heed.opensearch.crawling.client.volume.VolumeModule;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.VLayout;


public class CrawlingEntryPoint extends VLayout implements EntryPoint {
	private static final int height = Window.getClientHeight() - 110;
	private static final int width = 1200;
			
	private SearchModule searchModule;
	private CrawlingModule crawlingModule;
	private VolumeModule volumeModule;
	//private BrowserWindow browserWindow;
	
	
	public void onModuleLoad() {
		setHeight(height);  
        setWidth(width);
        		
        Canvas canvas = new Canvas();
        addMember(canvas);
                
        searchModule = new SearchModule();
        canvas.addChild(searchModule);
        
        crawlingModule = new CrawlingModule();
        crawlingModule.hide();
        canvas.addChild(crawlingModule);
        
        volumeModule = new VolumeModule();
        volumeModule.hide();
        canvas.addChild(volumeModule);
        //browserWindow = new BrowserWindow();
        
        setHtmlElement(DOM.getElementById("gwt"));
        setPosition(Positioning.RELATIVE);
        draw();
        
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.CRAWLING_VIEW)) {
	        		searchModule.hide();
	        		crawlingModule.show();
	        	} else if(event.isType(EventTypes.SEARCH_VIEW)) {
	        		crawlingModule.hide();
	        		searchModule.show();
	        	} else if(event.isType(SearchEventTypes.DETAIL)) {
	        		//browserWindow.setUrl(event.getRecord().getAttribute("url"));
	        		//browserWindow.show();
	        		String url = event.getRecord().getAttribute("url");
	        		com.google.gwt.user.client.Window.open(url, "_blank", null);
	        	}
	        }
        });
        Record record = new Record();
        record.setAttribute("query", "");
        EventBus.fireEvent(new OpenAppsEvent(EventTypes.SEARCH, record));
	}
		
}
