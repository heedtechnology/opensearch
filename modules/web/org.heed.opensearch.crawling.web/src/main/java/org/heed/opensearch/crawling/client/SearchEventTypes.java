package org.heed.opensearch.crawling.client;

import org.heed.openapps.gwt.client.EventTypes;

public class SearchEventTypes extends EventTypes {
	//Client
	
	public static final int ATTRIBUTE = 100;
	public static final int DETAIL = 101;
	public static final int RESULTS = 102;
		
	//Manager
	public static final int MGR_SEARCHER = 120;
	public static final int MGR_ATTRIBUTE = 121;
	public static final int MGR_PROPERTIES = 122;
}
