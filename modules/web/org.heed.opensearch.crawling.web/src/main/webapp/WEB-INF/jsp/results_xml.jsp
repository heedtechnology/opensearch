<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%
response.setContentType("text/xml");
response.setHeader("Cache-Control", "no-cache");
response.setHeader("pragma","no-cache");
%>
<response>
	<status><c:out value="${status}" /></status>
	<message></message>
	<data><c:out value="${data}" escapeXml="false" /></data>
</response>