package org.heed.opensearch.web;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.security.SecurityService;
import org.heed.openapps.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


@Controller
public class AdministrationController extends ControllerSupport {
	@Autowired SecurityService securityService;
	
	@RequestMapping(value="/administration", method = RequestMethod.GET)
	public ModelAndView administration(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		
		User user = securityService.currentUser();
		if(user.isGuest()) return new ModelAndView(new RedirectView("/login"));
		parms.put("user", getUser(user));
		parms.put("user_roles", getUserRoles(user));
		
		return new ModelAndView("administration", parms);
	}
	
	@RequestMapping(value="/administration/security", method = RequestMethod.GET)
	public ModelAndView security(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		
		User user = securityService.currentUser();
		if(user.isGuest()) return new ModelAndView(new RedirectView("/login"));
		parms.put("user", getUser(user));
		parms.put("user_roles", getUserRoles(user));
		
		return new ModelAndView("security", parms);
	}
	
	@RequestMapping(value="/administration/entity", method = RequestMethod.GET)
    public ModelAndView entity(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		
		User user = securityService.currentUser();
		if(user.isGuest()) return new ModelAndView(new RedirectView("/login"));
		if(!user.hasRole("administrator") && !user.hasRole("Administrator")) 
			return new ModelAndView(new RedirectView("/"));
    	
		return new ModelAndView("entity_manager").addAllObjects(parms);
	}
	
}
