<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
			
<div id="webtop-footer">
	<shiro:hasRole name="Administrator">
		<a href="/administration/security">Security</a>
		<a href="/administration/entity">Entities</a>
	</shiro:hasRole>
	<shiro:authenticated>
		<a href="/crawling">Crawling</a>
		<a href="/profile">Profile</a>
		<a href="/logout">Logout</a>
	</shiro:authenticated>
	<shiro:notAuthenticated>
		<a href="/login">Login</a>
	</shiro:notAuthenticated>								
	<!--
	<a href="/about">About Us</a>
	-->  									
</div>
							