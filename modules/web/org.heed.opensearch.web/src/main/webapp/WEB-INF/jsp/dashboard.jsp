<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>OpenSearch</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<link rel="stylesheet" href="/styles/dashboard.css" type="text/css" />
	<link rel="stylesheet" href="/theme/styles/smoothness/jquery-ui-1.10.1.custom.css" type="text/css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="/theme/script/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="/theme/script/jquery.autocomplete.min.js"></script>
	<script SRC="/script/search.js"></script>
</head>
	
<body>
	<table cellspacing="0" cellpadding="0" style="width:1000px;margin: 0 auto;">
		<tr valign="top">		
			<td width="100%">
				<div class="webtop">
					<div class="section">
						<div id="webtop-header">
							<a href="/"> 
								<img src='/theme/images/logo/opensearch_164x50.png' border="0" alt="OpenSearch" title="OpenSearch"/>
							</a>
						</div>
						<div id="webtop-body" style="margin-top:25px;margin-bottom:75px;">
							<div style="width:65%;margin:0 auto;">
								<div class="webtop-section" style="width:100%;">
    								<h4>Already Know What You Are Looking For?<br/>Search While You Type</h4>
        							<div style="width:500px;margin:0 auto;">
        								<input type="text" name="country" id="autocomplete" style="width:500px;margin:0 auto;"/>        
        							</div>
        							<div style="height:15px;width:100%;">
        								
        							</div>        
   								</div>
   								<div class="clearfix"></div>
   							</div>
   							
   							<div style="height:35px;width:100%;"></div>
   							
   							<div class="col12 clearfix round-bottom fill-denim dark">
						      <div class="col4 pad2y pad4x center">
						        <span class="icon pencil big dark"></span>
						        <h3>Build</h3>
						        <small class="quiet">Build a world-class search engine with OpenSearch.</small>
						      </div>
						      <div class="col4 pad2y pad4x center">
						        <span class="icon sun big dark"></span>
						        <h3>Design</h3>
						        <small class="quiet">Use our management tools to build and maintain your data.</small>
						      </div>
						      <div class="col4 pad2y pad4x center">
						        <span class="icon cloud big dark"></span>
						        <h3>Publish</h3>
						        <small class="quiet">Publish your searches on our fast and reliable cloud platform.</small>
						      </div>
						      <div class="clearfix"></div>
						    </div>
    
						</div>
						<%@include file='include/footer.jsp'%>
					</div>
				</div>					
			</td>
		</tr>
	</table>

</body>