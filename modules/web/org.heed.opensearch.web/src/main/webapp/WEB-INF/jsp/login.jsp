<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>OpenSearch</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<script SRC="/theme/script/core.js"></script>	
</head>
	
<body>
	<table cellspacing="0" cellpadding="0" style="width:1000px;margin: 0 auto;">
		<tr valign="top">		
			<td width="100%">
				<div class="webtop">
					<div class="section">
						<div id="webtop-header">
							<a href="/"> 
								<img src='/theme/images/logo/opensearch.png' border="0" alt="OpenSearch" title="OpenSearch"/>
							</a>
						</div>
						<div id="webtop-body" style="margin-top:75px;margin-bottom:75px;">
	
							<div style="width:350px;margin:0 auto;">
								<div class="webtop-signin">
   									<div style="margin:25px 5px 10px 50px;font-weight:bold;">
   										Returning user login below
   									</div>
   								
   									<form action="/login" method="post">
   										<table>
   											<tr>
   												<td>Username</td>
   												<td><input style="width:175px;" name="username" type="text"/></td>
   											</tr>
   											<tr>
   												<td>Password</td>
   												<td><input style="width:175px;" name="password" type="password" /></td>
   											</tr>
   											<tr>
   												<td><input type="submit" value="submit"/></td>
   												<td>
						   						<!-- 
						   							<input type="checkbox" name="remember" style="margin-left:25px;"/>
						   							<span style="font-size:15px;">remember me</span>
						   						-->
   												</td>
					   						</tr>
					   					</table>
					   				</form>
					   			</div>
   							</div> 
						</div>
					</div>
				</div>					
			</td>
		</tr>
	</table>

</body>