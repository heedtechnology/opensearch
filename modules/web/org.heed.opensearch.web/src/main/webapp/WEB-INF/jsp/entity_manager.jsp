<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>OpenSearch</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<script SRC="/theme/script/core.js"></script>
	
	<script>
		var isomorphicDir = '/entity/EntityManager/sc/';
     	var templates = '';
	</script>	
</head>
	
<body>
	<table cellspacing="0" cellpadding="0" style="width:1000px;margin: 0 auto;">
		<tr valign="top">		
			<td width="100%">
				<div class="webtop">
					<div class="section">
						<div id="webtop-header">
							<a href="/"> 
								<img src='/theme/images/logo/opensearch_164x50.png' border="0" alt="OpenSearch" title="OpenSearch"/>
							</a>
						</div>
						<div id="webtop-body" style="">
														
							<iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>
							<div id="gwt" style="width:100%;margin-bottom:10px;min-height:600px;"></div>
							<script type="text/javascript" language="javascript" src="/entity/EntityManager/EntityManager.nocache.js"></script>
							
						</div>
						<%@include file='include/footer.jsp'%>
					</div>
				</div>					
			</td>
		</tr>
	</table>

</body>