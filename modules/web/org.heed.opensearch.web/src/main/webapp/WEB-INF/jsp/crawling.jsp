<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>OpenSearch</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<script SRC="/theme/script/core.js"></script>
	
	<style>
	.day0, .day10 {background-color:rgba(144, 202, 119, .15);}
	.day1, .day11 {background-color:rgba(129, 198, 221, .15);}
	.day2, .day12 {background-color:rgba(233, 182, 77, .15);}
	.day3, .day13 {background-color:rgba(228, 135, 67, .15);}
	.day4, .day14 {background-color:rgba(158, 59, 51, .15);}
	.day5, .day15 {background-color:rgba(171, 152, 139, .15);}
	.day6, .day16 {background-color:rgba(116, 166, 189, .15);}
	.day7, .day17 {background-color:rgba(235, 133, 64, .15);}
	.day8, .day18 {background-color:rgba(141, 58, 97, .15);}
	.day9, .day19 {background-color:rgba(195, 178, 116, .15);}
	</style>
	<script>
		var isomorphicDir = '/crawling/app/CrawlingApplication/sc/';
     	var templates = <c:out value="${templates}" escapeXml="false" />;
     	var ticket = '<c:out value="${ticket}" />';
	</script>	
</head>
	
<body>
	<table cellspacing="0" cellpadding="0" style="width:1000px;margin: 0 auto;">
		<tr valign="top">		
			<td width="100%">
				<div class="webtop">
					<div class="section">
						<div id="webtop-header">
							<a href="/"> 
								<img src='/theme/images/logo/opensearch_164x50.png' border="0" alt="OpenSearch" title="OpenSearch"/>
							</a>
						</div>
						<div id="webtop-body" style="">
														
							<iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>
							<div id="gwt" style="width:100%;min-height:600px;"></div>
							<script type="text/javascript" language="javascript" src="/crawling/app/CrawlingApplication/CrawlingApplication.nocache.js"></script>
							
						</div>
						<%@include file='include/footer.jsp'%>
					</div>
				</div>					
			</td>
		</tr>
	</table>

</body>