<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>






<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>OpenSearch</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<script SRC="/theme/script/core.js"></script>
	
	<script>var isomorphicDir = "/theme/smartclient/"</script>
	<%@include file='include/smartclient.jsp'%>
	<script SRC="/theme/smartclient/skins/Enterprise/load_skin.js"></script>
	<script SRC="/script/core.js"></script>
	<script SRC="/theme/script/core.js"></script>
	<script src="/script/library/contact_library.js"></script>
	<script src="/script/library/repository_library.js"></script>
</head>
	
<body>
	<table cellspacing="0" cellpadding="0" style="width:1000px;margin: 0 auto;">
		<tr valign="top">		
			<td width="100%">
				<div class="webtop">
					<div class="section">
						<div id="webtop-header">
							<a href="/"> 
								<img src='/theme/images/logo/opensearch_164x50.png' border="0" alt="OpenSearch" title="OpenSearch"/>
							</a>
						</div>
						<div id="webtop-body" style="">
														
							<div class="body_container">
								<table cellspacing="0" cellpadding="0" style="width:100%;">
									<tr><td class="top-left"></td><td class="top-mid"></td><td class="top-right"></td></tr>
									<tr>
										<td class="mid-left"></td>
										<td class="mid-mid">
											<div style="width:100%">
												<div><script src="/script/user.js"></script></div>
        									</div> 
										</td>
										<td class="mid-right"></td>
									</tr>
									<tr><td class="bot-left"></td><td class="bot-mid"></td><td class="bot-right"></td></tr>
								</table>
							</div>
						</div>
						<%@include file='include/footer.jsp'%>
					</div>
				</div>					
			</td>
		</tr>
	</table>

</body>