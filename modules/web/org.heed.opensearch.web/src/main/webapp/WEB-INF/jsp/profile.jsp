<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>OpenSearch</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script SRC="/theme/script/core.js"></script>
	
	<style>
	.webtop-registration {
		width:800px;
		height:300px;
		margin:0 auto;
		padding: 14px 16px 30px;
		background: none repeat scroll 0 0 #FFFFFF;
    	border-radius: 4px 4px 4px 4px;
    	box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
    	color: #333333;
    	display: block;
    	font-size: 13px;
	}
	.webtop-row {
		height:35px;
		width:100%;
		margin-left:50px;
	}
	.webtop-cell {
		float:left;
		height:100%;
		width:100%;
	}
	.webtop-label {
		float:left;
		width:90px;
		font-size:15px;
	}
	</style>
	<script>
	$(document).ready(function() {
		$("#submit").click(function() {
			var pwd = $('input[name=password]').val();
			var userid = '<c:out value="${user.id}" />';
  			$.ajax({
    			"url" : "/service/entity/update.xml?qname={openapps.org_system_1.0}user&id=" + userid + "&password=" + pwd,
    			"type" : 'POST',
    			"data" : "",
    			"contentType" : "application/json; charset=utf-8",
    			"dataType" : "json",
    			"success" : function(data) {
    				
    			},
    			"statusCode": {
  					202: function() { 
  						
  					}
				}
			});
		});
	});
	</script>	
</head>

<body>
	<table cellspacing="0" cellpadding="0" style="width:1000px;margin: 0 auto;">
		<tr valign="top">		
			<td width="100%">
				<div class="webtop">
					<div class="section">
						<div id="webtop-header">
							<a href="/"> 
								<img src='/theme/images/logo/opensearch_164x50.png' border="0" alt="OpenSearch" title="OpenSearch"/>
							</a>
						</div>
						<div id="webtop-body" style="margin-top:50px;margin-bottom:75px;">
							
							<div class="section">
								<div class="webtop-registration">
   									<div style="width:150px;float:left;">
   										<img src="/theme/images/icons128/profile.png" />
   									</div>
   									<div style="width:640px;height:100%;float:left;">
   										<div class="webtop-row">
   											<div class="webtop-cell" style="margin-top:75px;">
   												<span style="font-weight:bold;">Update your profile</span>
   											</div>
   										</div>
   										<div class="webtop-row">
   											<div class="webtop-cell">
   												<div class="webtop-label" style="">Username</div>
   												<div class="webtop-label" style=""><c:out value="${user.username}" /></div>
   											</div>
   											<div class="webtop-cell">
   												<div class="webtop-label" style="">Password</div>
   												<input name="password" size="55" value="" />
   												<img src="/theme/images/icons16/asterisk_yellow.png" />
   											</div>
   										</div>
   			
   										<div class="webtop-row">
   											<input id="submit" type="submit" name="submitted" value="update"/>
   											<input id="home" type="submit" name="home" value="home" onclick="window.location='/'" />
   											<div style="height:15px;"></div>
   											<c:if test="${registration_message != null}">
   												<div class="webtop-cell"><c:out value="${registration_message}" /></div>
   											</c:if>
   										</div>
   									</div>
   								</div>
							</div>
						</div>
						<%@include file='include/footer.jsp'%>
					</div>
				</div>					
			</td>
		</tr>
	</table>

</body>