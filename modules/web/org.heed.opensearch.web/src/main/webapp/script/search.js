function search(query) {
	$.ajax({
		"url" : '/service/opensearch/results.json?query='+query,
		"type" : 'GET',
		"data" : "",
		"contentType" : "application/json; charset=utf-8",
		"dataType" : "json",
		"success" : function(data) {
			if(data) {
				$('#webtop-body').html('<p id="resultMsg">Page x of '+data.response.totalRows+' results (0.21 seconds)</p>');
			} else {
				$('#webtop-body').html('<p>A problem occurred searching for '+suggestion+'</p>');
			}
		}
	});	
}
$(document).ready(function() {
	$('#autocomplete').autocomplete({
    	serviceUrl: '/service/opensearch/queries.json',
    	formatResult : function (suggestion, currentValue) {
    		var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g'),
            	pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
        	var label = suggestion.value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
        	return label + ' (' + suggestion.type + ')';
    	},
    	onSelect: function (suggestion) {
    		search(suggestion.data);
    	}
	});
	$('#autocomplete').keypress(function (e) {
		if(e.which == 13) {
		    search($('#autocomplete').val());
		    return false;
		}
	});
});