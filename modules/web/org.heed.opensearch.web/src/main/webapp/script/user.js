var currNode = 0;
var activityValueMap = {'Correspondence Sent':'Correspondence Sent'};
var noteValueMap = {'General':'General','User Request':'User Request'};
var contactRelationshipMap = {'Spouse':'Spouse','Sibling':'Sibling','Assistant':'Assistant'};
function updateEntityData(xmlDoc, xmlText) {
	contactXml = contact.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
	isc.ResultSet.create({ID:"contactRS",dataSource:"contact",initialData:contactXml});
	contactDetailsForm.setValues(contactXml);
	var roleXml = contact.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/roles/node"));
	roleRelationList.setData(roleXml);
	//var groupXml = contact.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/groups/node"));
	//groupRelationList.setData(groupXml);
}
function save() {
	if(contactDetailsForm.valuesHaveChanged()) contactDetailsForm.saveData();
}
function load_contactsList(node) {
	//contactDetailsForm.editRecord(node);
	//save();
	if(currNode !== null && node !== currNode) {
		isc.XMLTools.loadXML("/service/entity/get/"+node.id+".xml", "updateEntityData(xmlDoc, xmlText)");
		currNode = node;
		entityId = node.id;
	}
}
function add_contactsList() {
	var parms = userAddWindowForm.getValues();
	parms['qname'] = '{openapps.org_system_1.0}user';
	contactsListList.addData(parms);
	userAddWindow.hide();
}
function save_contactsList() {
	//if(contactDetailsForm.valuesHaveChanged()) {
		var parms = contactDetailsForm.getValues();
  		parms['qname'] = '{openapps.org_system_1.0}user';
 		isc.XMLTools.loadXML("/service/entity/update.xml", function(xmlDoc, xmlText) {
 			/*
 			var addressXml = contact.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
 			var updatedRecord = isc.addProperties(addressList.getSelectedRecord(),addressXml[0]); 
 			if(addressList.getRecordIndex(updatedRecord) > -1) addressList.updateData(updatedRecord);
				else addressList.addData(updatedRecord);
 			addressList.refreshFields();
 			contactsListAddWindow.hide();
 			*/
 		},{httpMethod:"POST",params:parms});
	//}
}
function remove_contactsList() {
	contactsListList.removeSelectedData();
	contactDetailsForm.clearValues();
}
function alphaSearch1(query) {
	contactsListList.fetchData({qname:'{openapps.org_system_1.0}user',query:query,field:'username',sort:'username'});
}

isc.MessagingDataSource.create({ID:'contact',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'last_update', type:'date'},{name:'last_wrote', type:'date'},{name:'birth_date', type:'date'},{name:'death_date', type:'date'},{name:'last_ship', type:'date'},{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'contacts',fetchDataURL:'/service/entity/search.xml',addDataURL:'/service/entity/create.xml',updateDataURL:'/service/entity/update.xml',removeDataURL:'/service/entity/remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.Window.create({ID:'userAddWindow',title:'Add User',width:400,height:185,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
	items:[
	    isc.DynamicForm.create({ID:'userAddWindowForm',width:'100%',margin:10,numCols:2,cellPadding:7,
	    	fields:[
	    	    {name:'username',width:'*',title:'UserName'},
	    	    {name:'email',width:'*',required:true,title:'Email Address'},
	    	    {name:'password',width:'*',required:true,type:'password',title:'Password'},
	    	    {name:'validateBtn',title:'Save',type:'button',click:'add_contactsList()'}
	    	]
	    })
	]
});
isc.ListGrid.create({ID:'contactsListList',dataSource:'contacts',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,recordClick:'load_contactsList(record)',dateFormatter:'toLocaleString',
	fields:[
	    {name:'username',title:'User Name',width:'*',align:'left'}
	]
});

isc.HLayout.create({ID:'contactsListButtonPanel',height:'25',width:'100%',autoDraw:false,layoutLeftMargin:2,membersMargin:5,
	members:[
	     isc.IButton.create({ID:'addcontactsListButton',autoDraw:false,title:'Add',width:65,icon:"/theme/images/icons/add.png",click:'userAddWindow.show();'}),
	     isc.IButton.create({ID:'savecontactsListButton',autoDraw:false,title:'Save',width:65,icon:"/theme/images/icons/save.gif",click:'save_contactsList();'}), 
	     isc.IButton.create({ID:'deletecontactsListButton',autoDraw:false,title:'Delete',width:75,icon:"/theme/images/icons/remove.png",click:'remove_contactsList()'})
	]
});
isc.DynamicForm.create({ID:'contactsListSearch',width:'100%',margin:0,numCols:2,cellPadding:2,
	fields:[
	    {name:'query',width:'245',type:'text',showTitle:false},
	    {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
	    	click:function() {
	    		alphaSearch1(contactsListSearch.getValue('query'));
	    	}
	    }
	]
});
isc.VLayout.create({ID:'contactsList',autoDraw:false,height:'100%',width:'300',
	members:[
	    contactsListButtonPanel,
	    contactsListSearch,
	    contactsListList
	]
});
isc.HLayout.create({ID:"detailsPanel",autoDraw:false,height:'100%',width:'100%',membersMargin:5,margin:10,
	members:[
	    isc.DynamicForm.create({ID:'contactDetailsForm',dataSource:'contact',width:'400',height:200,border:'1px solid #C0C3C7',cellPadding:5,numCols:'2',colWidths:[100,'*'],visibility:'visible',autoFocus:false,
	    	fields:[
	    	    {name:'username',width:'*',type:'staticText',title:'UserName'},
	    	    {name:'email',width:'*',required:true,title:'Email Address'},
	    	    {name:'first_name',width:'*',title:'First Name'},
	    	    {name:'last_name',width:'*',title:'Last Name'},
	    	    {name:'organization',width:'*',title:'Organization'},
	    	    {name:'terms',width:'*',type:'checkbox',title:'Agreed To Terms of Use'},
	    	    {name:'active',width:'*',type:'checkbox',title:'Active'},
	    	    //{name:'password',width:'*',required:true,type:'hidden'},
	    	    {name:"submit",title:"Change Password",type:"button",startRow:false,endsRow:false,
         	    	click:function () {
         	    		passwordWindow.show();
         	    	}
         	    }
	    	]
	    }),
	    isc.Canvas.create({width:50}),
	    isc.VLayout.create({membersMargin:15,
	    	members:[
	    	    getUserRolesPanel("100%", 200)//,
	    	    //getUserGroupsPanel("100%", 200)
	    	]
	    })
	    
	]
});

//isc.VLayout.create({ID:"notesAndActivitiesPanel",autoDraw:false,height:'100%',width:'100%',margin:10,membersMargin:10,
	//members:[
	    //getNotesPanel(noteValueMap),
	    //getActivitiesPanel(activityValueMap)
	//]
//});
isc.VLayout.create({ID:"relationshipsPanel",autoDraw:false,height:'100%',width:'100%',margin:10,membersMargin:10,
	members:[
	    getContactRelationPanel(contactRelationshipMap,'100%','50%')
	]
});  
isc.TabSet.create({ID:'rightTabSet',tabBarPosition:'top',width:'100%',height:'100%',autoDraw:false,
	tabs:[	
	      {title:'User Details',pane:detailsPanel},
	      //{title:'Notes and Activities',pane:notesAndActivitiesPanel},
	      //{title:'Contacts',pane:relationshipsPanel}
	]
});
function getUserRolesPanel(width,height) {
	isc.MessagingDataSource.create({ID:'roleRelationshipsDS',fetchDataURL:'/service/entity/search.xml',updateDataURL:'/service/entity/update.xml',addDataURL:'/service/entity/create.xml',removeDataURL:'/service/entity/remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
	isc.Window.create({ID:'roleAddWindow',title:'Add/Update Role',width:400,height:150,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items:[
		    isc.DynamicForm.create({ID:'roleAddWindowForm',width:'100%',datasource:'contacts',margin:10,numCols:2,cellPadding:7,
		    	fields:[
		    	    {name:'name',width:'*',required:true,title:'Name'},
		    	    {name:'description',width:'*',title:'Description'},
		    	    {name:'validateBtn',title:'Save',type:'button',
		    	    	click:function () {
		    	    		var parms = roleAddWindowForm.getValues();
		    	    		parms['qname'] = '{openapps.org_system_1.0}role';
		    	    		if(parms['id']) editRoleRelationWindowList.updateData(parms);
		    	    		else editRoleRelationWindowList.addData(parms);
		    	    		roleAddWindowForm.clearValues();
		    	    		roleAddWindow.hide();
	 	         	   	}
		    	    }
		    	]
		    })
		]
	});
	isc.Window.create({ID:"editRoleRelationWindow",title:"Add/Update User Roles",width:480,height:350,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,margin:5,
		items: [
		    isc.ListGrid.create({ID:'editRoleRelationWindowList',dataSource:'roleRelationshipsDS',height:'100%',width:'100%',margin:5,alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
		    	fields:[
		    	    {name:'name',title:'Name',width:'150',align:'left'},
		    	    {name:'description',title:'Description',width:'*',align:'left'}
		    	]
		    }),
		    isc.HLayout.create({autoDraw:false,width:'100%',height:'25',margin:5,membersMargin:5,
		   	 	members:[	        	          
		   	 	    isc.Button.create({width:75,icon:"/theme/images/icons/add.png",title:"add role",click:'roleAddWindow.show();'}),
		   	 	    isc.Button.create({width:75,icon:"/theme/images/icons/edit.png",title:"edit role",
		   	 	    	click:function() {
		   	 	    		var record = editRoleRelationWindowList.getSelectedRecord();
		   	 	    		roleAddWindowForm.editRecord(record);
		   	 	    		roleAddWindow.show();
		   	 	    	}
		   	 	    }),
		   	 	    isc.Button.create({width:85,icon:"/theme/images/icons/remove.png",title:"delete role",click:function() {
		   	 	    	editRoleRelationWindowList.removeSelectedData(); 		        	        	
		   	 	    }}),
		   	 	    isc.IButton.create({width:60,autoDraw:false,title:'link',icon:"/theme/images/icons16/link.png",
		   	 	    	click:function () {
		   	 	    		if(entityId) {
		   	 	    			var parms = {};
		   	 	    			parms['qname'] = '{openapps.org_system_1.0}roles';
		   	 	    			parms['source'] = entityId;
		   	 	    			parms['target'] = editRoleRelationWindowList.getSelectedRecord().id;
		   	 	    			isc.XMLTools.loadXML("/service/entity/association/add.xml", function(xmlDoc, xmlText) {
		   	 	    				updateEntityData(xmlDoc, xmlText);
		   	 	    				//editRoleRelationWindowForm.clearValues();
		   	 	    				editRoleRelationWindow.hide();
		   	 	    			},{httpMethod:"POST",params:parms});
		   	 	    		}
		   	 	    	}
		   	 	    })
		   	 	]
		    })
	    ]
	});
	return isc.VLayout.create({ID:"roleRelationPanel",autoDraw:false,width:width,height:height,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	    	 members: [
   	    	     isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Roles</span>"}),
   	    	     isc.Button.create({width:60,height:20,icon:"/theme/images/icons/add.png",title:"add",
   	    	    	 click:function(){
   	    	    		 editRoleRelationWindowList.fetchData({qname:'{openapps.org_system_1.0}role',query:'',field:'name',sort:'name'});
   	    	    		 editRoleRelationWindow.show();
   	    	    	 }
   	    	     }),
   	    	     isc.Button.create({width:75,height:20,icon:"/theme/images/icons/remove.png",title:"delete",click:function() {
   	    	    	 parms = {};
   	    	    	 parms['id'] = roleRelationList.getSelectedRecord().id;
   	    	    	 //parms['target'] = roleRelationList.getSelectedRecord().target_id;
   	    	    	 isc.XMLTools.loadXML("/service/entity/association/remove.xml", function(xmlDoc, xmlText) {
   	    	    		 roleRelationList.removeSelectedData();
   	    	    	 },{httpMethod:"POST",params:parms});	    		        	        	
   	    	     }})
   	    	 ]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'roleRelationList',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	dateFormatter:'toLocaleString',
   	 	    	fields:[
   	 	    	    {name:'name',title:'Name',width:'150',align:'left'},
   	    	        {name:'description',title:'Description',width:'*',align:'left'},
	            ]
   	 	    })
   	 	]
    });
}
function getUserGroupsPanel(width,height) {
	isc.MessagingDataSource.create({ID:'groupRelationshipsDS',fetchDataURL:'/service/entity/search.xml',updateDataURL:'/service/entity/update.xml',addDataURL:'/service/entity/create.xml',removeDataURL:'/service/entity/remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
	isc.Window.create({ID:'groupAddWindow',title:'Add/Update Group',width:400,height:150,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items:[
		    isc.DynamicForm.create({ID:'groupAddWindowForm',width:'100%',datasource:'groups',margin:10,numCols:2,cellPadding:7,
		    	fields:[
		    	    {name:'name',width:'*',required:true,title:'Name'},
		    	    {name:'description',width:'*',title:'Description'},
		    	    {name:'validateBtn',title:'Save',type:'button',
		    	    	click:function () {
		    	    		var parms = groupAddWindowForm.getValues();
		    	    		parms['qname'] = '{openapps.org_system_1.0}group';
		    	    		if(parms['id']) editGroupRelationWindowList.updateData(parms);
		    	    		else editGroupRelationWindowList.addData(parms);
		    	    		groupAddWindowForm.clearValues();
		    	    		groupAddWindow.hide();
	 	         	   	}
		    	    }
		    	]
		    })
		]
	});
	isc.Window.create({ID:"editGroupRelationWindow",title:"Add/Update User Groups",width:480,height:350,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,margin:5,
		items: [
		    isc.ListGrid.create({ID:'editGroupRelationWindowList',dataSource:'groupRelationshipsDS',height:'100%',width:'100%',margin:5,alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
		    	fields:[
		    	    {name:'name',title:'Name',width:'150',align:'left'},
		    	    {name:'description',title:'Description',width:'*',align:'left'}
		    	]
		    }),
		    isc.HLayout.create({autoDraw:false,width:'100%',height:'25',margin:5,membersMargin:5,
		   	 	members:[	        	          
		   	 	    isc.Button.create({width:80,icon:"/theme/images/icons/add.png",title:"add group",click:'groupAddWindow.show();'}),
		   	 	    isc.Button.create({width:80,icon:"/theme/images/icons/edit.png",title:"edit group",
		   	 	    	click:function() {
		   	 	    		var record = editGroupRelationWindowList.getSelectedRecord();
		   	 	    		groupAddWindowForm.editRecord(record);
		   	 	    		groupAddWindow.show();
		   	 	    	}
		   	 	    }),
		   	 	    isc.Button.create({width:90,icon:"/theme/images/icons/remove.png",title:"delete group",click:function() {
		   	 	    	editGroupRelationWindowList.removeSelectedData(); 		        	        	
		   	 	    }}),
		   	 	    isc.IButton.create({width:65,autoDraw:false,title:'link',icon:"/theme/images/icons16/link.png",
		   	 	    	click:function () {
		   	 	    		if(entityId) {
		   	 	    			var parms = {};
		   	 	    			parms['qname'] = '{openapps.org_system_1.0}groups';
		   	 	    			parms['source'] = entityId;
		   	 	    			parms['target'] = editGroupRelationWindowList.getSelectedRecord().id;
		   	 	    			isc.XMLTools.loadXML("/service/entity/association/add.xml", function(xmlDoc, xmlText) {
		   	 	    				updateEntityData(xmlDoc, xmlText);
		   	 	    				//editRoleRelationWindowForm.clearValues();
		   	 	    				editGroupRelationWindow.hide();
		   	 	    			},{httpMethod:"POST",params:parms});
		   	 	    		}
		   	 	    	}
		   	 	    })
		   	 	]
		    })
	    ]
	});
	return isc.VLayout.create({ID:"groupRelationPanel",autoDraw:false,width:width,height:height,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	    	 members: [
   	    	     isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Groups</span>"}),
   	    	     isc.Button.create({width:65,height:20,icon:"/theme/images/icons/add.png",title:"add",
   	    	    	 click:function(){
   	    	    		 editGroupRelationWindowList.fetchData({qname:'{openapps.org_system_1.0}group',query:'',field:'name',sort:'name'});
   	    	    		 editGroupRelationWindow.show();
   	    	    	 }
   	    	     }),
   	    	     isc.Button.create({width:80,height:20,icon:"/theme/images/icons/remove.png",title:"delete",click:function() {
   	    	    	 parms = {};
   	    	    	 parms['id'] = groupRelationList.getSelectedRecord().id;
   	    	    	 //parms['target'] = roleRelationList.getSelectedRecord().target_id;
   	    	    	 isc.XMLTools.loadXML("/service/association/remove.xml", function(xmlDoc, xmlText) {
   	    	    		groupRelationList.removeSelectedData();
   	    	    	 },{httpMethod:"POST",params:parms});	    		        	        	
   	    	     }})
   	    	 ]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'groupRelationList',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	dateFormatter:'toLocaleString',
   	 	    	fields:[
   	 	    	    {name:'name',title:'Name',width:'150',align:'left'},
   	    	        {name:'description',title:'Description',width:'*',align:'left'},
	            ]
   	 	    })
   	 	]
    });
}
isc.HLayout.create({ID:'pageLayout',width:smartWidth,height:smartHeight,position:'relative',visibility:'visible',members:[contactsList,rightTabSet]});

isc.Window.create({ID:"addressWindow",title:"Add/Update Address",width:500,height:190,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"addressWindowForm",width:100,numCols:"2",cellPadding:7,autoDraw:false,
         	fields: [
         	    {name:'street',width:'*',title:'Street Address'},
	    	    {name:'address1',width:'*',title:'Address #1'},
	    	    {name:'city',width:'*',title:'City'},
	    	    {name:'state',width:'*',title:'State'},
	    	    {name:'country',width:'*',type:'select',valueMap:{'United States':'United States','United Kingdom':'United Kingdom','France':'France','Germany':'Germany','Italy':'Italy','Spain':'Spain','Ireland':'Ireland','Poland':'Poland'},width:'100',title:'Country'},
	    	    {name:'zip',width:'*',title:'Zip Code'},
         	    {name:"submit",title:"Save",type:"button",
         	    	click:function () {
         	    		if(addResourceWin2Form.validate()) {
         	    			var repo = repositoryTree.getSelectedRecord().id;
         	    			var title = addResourceWin2Form.getValue('title');
         	    			repositoryTree.addData({repo:repo,title:title},function(dsResponse, data, dsRequest) {
         	    				//isc.XMLTools.loadXML("fetch.xml?_dataSource=resources&repo="+repo, "populateComponent(xmlDoc, xmlText)");
         	    				addCollectionWin.hide();
         	    			});
         	    		}
         	    	}
         	    }
           	]
        })
    ]
});
isc.Window.create({ID:"passwordWindow",title:"Change Password",width:350,height:105,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"changePasswordWindowForm",width:"100%",height:"100%",cellPadding:7,autoDraw:false,
         	fields: [
         	    {name:'password',width:'*',title:'Password'},
	    	    {name:"submit",title:"Save",type:"button",
         	    	click:function () {
         	    		contactDetailsForm.setValue('password', changePasswordWindowForm.getValue('password'));
         	    		passwordWindow.hide();
         	    	}
         	    }
           	]
        })
    ]
});