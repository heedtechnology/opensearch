<#if associations??>
	<#list associations as association>
		<node id="${association.target?c}" parent="${association.source?c}" qname="${association.targetName!}" localName="${association.targetName.localName!}" parentQName="${association.sourceName!}" parentLocalName="${association.sourceName.localName!}">
		<#list association.targetEntity.properties as property>
			<#if !property.empty>
				<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
			</#if>
		</#list>
		<title>${association.targetEntity.getPropertyValue(name_qname)}</title>
		<#if printSources>
			<source_associations>
			<#list association.targetEntity.sourceAssociationQNames as qname>
				<${qname.localName} qname="${qname}">
					<#list association.targetEntity.getSourceAssociations(qname) as source_association>
						<node id="${source_association.id?c}" qname="${source_association.targetName!}" localName="${source_association.targetName.localName!}" target_id="${source_association.target?c}" />
					</#list>
				</${qname.localName}>
			</#list>
			</source_associations>
		</#if>
		<#if printTargets>
			<target_associations>
			<#list association.targetEntity.targetAssociationQNames as qname>
				<${qname.localName} qname="${qname}">
					<#list association.targetEntity.getTargetAssociations(qname) as target_association>
						<node id="${target_association.id?c}" qname="${target_association.sourceName!}" locaName="${target_association.sourceName.localName!}" source_id="${target_association.source?c}" />
					</#list>
				</${qname.localName}>
			</#list>
			</target_associations>
		</#if>
	</node>
	</#list>
</#if>





<#if association??>
	<node id="${association.target?c}" parent="${association.source?c}" qname="${association.targetName!}" localName="${association.targetName.localName!}" parentQName="${association.sourceName!}" parentLocalName="${association.sourceName.localName!}">
		<#list association.targetEntity.properties as property>
			<#if !property.empty>
				<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
			</#if>
		</#list>
		<title>${association.targetEntity.getPropertyValue(name_qname)}</title>
		<#if printSources>
			<source_associations>
			<#list association.targetEntity.sourceAssociationQNames as qname>
				<${qname.localName} qname="${qname}">
					<#list association.targetEntity.getSourceAssociations(qname) as source_association>
						<node id="${source_association.id?c}" qname="${source_association.targetName!}" localName="${source_association.targetName.localName!}" target_id="${source_association.target?c}" />
					</#list>
				</${qname.localName}>
			</#list>
			</source_associations>
		</#if>
		<#if printTargets>
			<target_associations>
			<#list association.targetEntity.targetAssociationQNames as qname>
				<${qname.localName} qname="${qname}">
					<#list association.targetEntity.getTargetAssociations(qname) as target_association>
						<node id="${target_association.id?c}" qname="${target_association.sourceName!}" locaName="${target_association.sourceName.localName!}" source_id="${target_association.source?c}" />
					</#list>
				</${qname.localName}>
			</#list>
			</target_associations>
		</#if>
	</node>
</#if>





<#if entities??>
	<#list entities as entity>
		<node id="${entity.id?c}" uid="${entity.uid}" qname="${entity.qname}" localName="${entity.qname.localName}" deleted="${entity.deleted?string}">
			<#list entity.properties as property>
				<#if !property.empty>
					<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
				</#if>
			</#list>
			<#list entity.properties as property>
				<#if !property.empty && property.qname = "{openapps.org_system_1.0}name">
					<title>${property.toXml()}</title>
				</#if>
			</#list>
			<#if printSources>
				<source_associations>
					<#list entity.sourceAssociationQNames as qname>
					<${qname.localName} qname="${qname}">
						<#list entity.getSourceAssociations(qname) as source_association>
						<node id="${source_association.id?c}" qname="${source_association.targetName!}" localName="${source_association.targetName.localName!}" target_id="${source_association.target?c}">
							<#if source_association.targetEntity?? >
							<#list source_association.targetEntity.properties as property>
								<#if !property.empty>
								<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
								</#if>
							</#list>
							</#if>
							<#list source_association.properties as property>
								<#if !property.empty>
								<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
								</#if>
							</#list>
						</node>
						</#list>
					</${qname.localName}>
					</#list>
				</source_associations>
			</#if>
			<#if printTargets>
				<target_associations>
					<#list entity.targetAssociationQNames as qname>
					<${qname.localName} qname="${qname}">
						<#list entity.getTargetAssociations(qname) as target_association>
						<node id="${target_association.id?c}" qname="${target_association.sourceName!}" locaName="${target_association.sourceName.localName!}" source_id="${target_association.source?c}">
							<#if target_association.sourceEntity?? >
							<#list target_association.sourceEntity.properties as property>
								<#if !property.empty>
								<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
								</#if>
							</#list>
							</#if>
							<#list target_association.properties as property>
								<#if !property.empty>
								<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
								</#if>
							</#list>
						</node>
						</#list>
					</${qname.localName}>
					</#list>
				</target_associations>
			</#if>
			<#if entity.created??>
			<created>${entity.created?datetime?string("MMMM d, yyyy h:mm a")}</created>
			</#if>
			<#if entity.modified??>
			<modified>${entity.modified?datetime?string("MMMM d, yyyy h:mm a")}</modified>
			</#if>
			<creator>${entity.creator!}</creator>
			<modifier>${entity.modifier!}</modifier>
		</node>
	</#list>
</#if>






<#if entity??>
<node id="${entity.id?c}" uid="${entity.uid}" qname="${entity.qname}" localName="${entity.qname.localName}" deleted="${entity.deleted?string}">
	<#list entity.properties as property>
		<#if !property.empty>
			<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
		</#if>
	</#list>
	<#list entity.properties as property>
		<#if !property.empty && property.qname = "{openapps.org_system_1.0}name">
			<title>${property.toXml()}</title>
		</#if>
	</#list>
	<#if exportProcessor??>
		<data>${exportProcessor.export("xml", entity)}</data>
	<#/if>
	<#if printSources>
		<source_associations>
	<#list entity.sourceAssociationQNames as qname>
		<${qname.localName} qname="${qname}">
		<#list entity.getSourceAssociations(qname) as source_association>
			<node id="${source_association.id?c}" qname="${source_association.targetName!}" localName="${source_association.targetName.localName!}" target_id="${source_association.target?c}">
				<#if source_association.targetEntity?? >
				<#list source_association.targetEntity.properties as property>
					<#if !property.empty>
						<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
					</#if>
				</#list>
				</#if>
				<#list source_association.properties as property>
					<#if !property.empty>
						<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
					</#if>
				</#list>
			</node>
		</#list>
		</${qname.localName}>
	</#list>
		</source_associations>
	</#if>
	<#if printTargets>
		<target_associations>
	<#list entity.targetAssociationQNames as qname>
		<${qname.localName} qname="${qname}">
			<#list entity.getTargetAssociations(qname) as target_association>
				<node id="${target_association.id?c}" qname="${target_association.sourceName!}" locaName="${target_association.sourceName.localName!}" source_id="${target_association.source?c}">
				<#if target_association.sourceEntity?? >
					<#list target_association.sourceEntity.properties as property>
						<#if !property.empty>
							<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
						</#if>
					</#list>
				</#if>
				<#list target_association.properties as property>
					<#if !property.empty>
						<${property.qname.localName}>${property.toXml()}</${property.qname.localName}>
					</#if>
				</#list>
				</node>
			</#list>
		</${qname.localName}>
	</#list>
</target_associations>
	</#if>
	<#if entity.created??>
	<created>${entity.created?datetime?string("MMMM d, yyyy h:mm a")}</created>
</#if>
<#if entity.modified??>
	<modified>${entity.modified?datetime?string("MMMM d, yyyy h:mm a")}</modified>
</#if>
<creator>${entity.creator!}</creator>
<modifier>${entity.modifier!}</modifier>
</node>
</#if>