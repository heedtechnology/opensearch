Element content = document.select("meta[name=dc.title]").first();
if(content != null) {
	String title = content.attr("content");
	if(title != null && title.length() > 0) {
		seed.setTitle(title);
	    seed.setStatus("hit");
    }
}