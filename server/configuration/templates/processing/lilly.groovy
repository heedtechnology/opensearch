Element middle = document.select("div#wsh_middle").first();
if(middle != null) {
	Element container = middle.select("div.wsh_printContainer").first();
	if(container != null) {
	    Element titleEl = container.select("h1").first();
	    if(titleEl != null) {
	       seed.setTitle(titleEl.text());
	    }
    }
	if(seed.getTitle() == null || seed.getTitle().length() > 0) {
	    seed.setStatus("hit");
	} else {
	    seed.setStatus("");
	}
}