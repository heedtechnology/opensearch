
Dear  ${email},

        This e-mail has been sent in response to your request that we send you a reset password.
        
        ${password}

        Please note: This e-mail was sent from an auto-notification system that cannot accept incoming e-mail. Please do not reply to this message