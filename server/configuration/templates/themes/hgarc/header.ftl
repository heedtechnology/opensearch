<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><c:out value="${title}" /> Cataloguing and Electronic Finding Aid Project</title>

<meta name="keywords" content="Howard Gotlieb Archival Research Center, manuscripts, history, preserve, research, journalism, poetry, literature and criticism, dance, music, theater, film, television, political, religious" />

<style type="text/css" title="defaultStyle">
  @import url(http://www.bu.edu//phpbin/archives-cc/interface/css/admin_browse.css);
  @import url(http://www.bu.edu//phpbin/archives-cc/interface/css/public_browse.css);
</style>

<style type="text/css" media="screen">
@import "/style/hgarcnew.css"; /* Mostly just text styling. */
@import "/style/hgarc_home.css"; /* Homepage lightbox. */
@import "/style/hgarc_content.css"; /* Content. */
@import "/style/thickbox.css"; /* Lightbox. */
@import "/style/mlkjr_search.css"; /* Lightbox. */
@import "/style/accordionview/assets/skins/hgarc/reset-fonts.css"; 
@import "/style/accordionview/assets/skins/hgarc/accordionview.css"; 
</style>

<script type="text/javascript" src="/script/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="/script/thickbox.js"></script>
<script type="text/javascript" src="/script/accordionview/utilities.js"></script>
</head>
<body>	
<div id="header">
<h1 class="logo" >
<a href="/dbin/archives"><img src="/images/hgarc_logo.jpg" alt="HGARC_logo" /></a>
<a href="/dbin/mlkjr/"><img src="/images/mlkjr_logo.jpg" alt="HGARC_logo" /></a></h1>
</div>

<div class="menu">
<ul>
<li><a title="Homepage" href="/dbin/mlkjr/">HOME</a></li> 
<li><a title="About" href="/dbin/mlkjr/about/">ABOUT THE ARCHIVE</a>
<ul>
<li><a href="/dbin/mlkjr/about/">About the Center</a></li>
<li><a href="/dbin/mlkjr/about/index.php?id=mission">Mission</a></li>
<li><a href="/dbin/mlkjr/about/index.php?id=staff">Center Staff</a></li>
<li><a href="/dbin/mlkjr/about/index.php?id=archival">Archival Project Team</a></li>
<li><a href="/dbin/mlkjr/about/index.php?id=partnerships">Partnerships</a></li>
</ul>
</li>

<li><a title="Collections" href="/dbin/mlkjr/collection/index.php?id=scope">THE COLLECTION</a>
<ul>
<li><a href="/dbin/mlkjr/collection/index.php?id=scope">Scope and Content</a></li>
<li><a href="/dbin/mlkjr/collection/index.php?id=biography">Biography</a></li>
<li><a href="/dbin/mlkjr/collection/search.php">Search Finding Aid</a></li>
</ul>
</li> 

<li><a title="Events News" href="/dbin/mlkjr/events/">EVENTS &amp; NEWS</a></li> 
<li><a title="Exhibition Room" href="/dbin/mlkjr/exhibition/">EXHIBITION ROOM</a></li> 
</ul>
</div><div id="content2">

<script type="text/javascript">
function processSearchForm(form) {
	if(form.sis.checked) {
		var q = form.query.value;
		form.query.value = '//'+q;
	}
}
</script>
<div id="search_collection">
	<form action="search.php" onsubmit="processSearchForm(this);">
		<table id="formtable">
			<tr>
				<td><input name="query" type="text" class="searchtext1" /></td>
				<td><input type="image" src="images/collection_button_search.gif" alt="Submit button" /></td>
				<td><input name="sis" type="checkbox" /></td>
				<td>Search In Search &nbsp;</td>
				<td>&nbsp;&nbsp;? <a href="">Help</a></td>
			</tr>
		</table>
	</form>
</div>
<style>
	.collection{font-size:17px;font-weight:bold;padding:20px 0 5px 0;}
	.series{font-weight:bold;padding:0 0 0 10px;line-height:2em;}
	.note{text-align:justify;padding:2px 10px 5px 20px;display:none;}
	.arrow{cursor:pointer;position:relative;top:6px;}
</style>
<script>
function showDetail(id) {
	if($('#'+id).is(":visible")) {
		$('#'+id).hide(400);
		$('#detail'+id).html('<img src="images/arrow_closed.gif" />');
	} else {
		$('#'+id).show(400);
		$('#detail'+id).html('<img src="images/arrow_open.gif" />');
	}
}
</script>