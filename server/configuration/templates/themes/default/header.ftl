<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Archive Manager</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<script SRC="/theme/script/core.js"></script>
</head>
<body>
<table cellspacing="0" cellpadding="0" style="width:1200px;margin: 0 auto;">
	<tr>
		<td colspan="2">
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td style="text-align:bottom;">
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td style="padding-right:4px;">
									<a href="/"> 
										<img src='/theme/images/logo/ArchiveManager200.png' width="200" height="36" border="0" alt="Archive Manager" title="Archive Manager"/>
									</a>
								</td>
							</tr>
						</table>
					</td>
					
					<td style="width:100%;" style="text-align:bottom;">
						
					</td>
					
					<td style="text-align:bottom;">
						<table width="100%">
							<tr>
								
                            </tr>
                        </table>
                    </td>
				</tr>
			</table>
		</td>
	</tr>
	<tr valign="top">		
		<td width="100%">
			<div class="webtop">
				<#if loggedInUsername??>
					<span style="font-weight:bold;font-size:14px;">You are signed in as ${loggedInUsername}</span>
                </#if>