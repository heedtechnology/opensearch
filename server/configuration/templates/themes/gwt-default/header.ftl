<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Archive Manager</title>
	<link rel="stylesheet" href="/theme/styles/main.css" type="text/css" />
	<script SRC="/theme/script/core.js"></script>
</head>
<body>
<table cellspacing="0" cellpadding="0" style="width:1200px;margin: 0 auto;">
	<tr>
		<td colspan="2">
			<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
					<td style="text-align:bottom;">
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td style="padding-right:4px;">
									<a href="/"> 
										<img src='/theme/images/logo/ArchiveManager200.png' width="200" height="36" border="0" alt="Archive Manager" title="Archive Manager"/>
									</a>
								</td>
							</tr>
						</table>
					</td>
					
					<td style="width:100%;" style="text-align:bottom;">
						
					</td>
					
					<td style="text-align:bottom;">
						<table width="100%">
							<tr>
								<#if loggedInUsername??>
								<td style="text-align:center;">
                            		<span style="font-weight:bold;font-size:14px;">${loggedInUsername}</span>
                            	</td>
                            	</#if>
                            </tr>
                        </table>
                    </td>
					<td style=vertical-align:middle>
						<table width="100%">
							<tr>
								<#if loggedInUsername??>
									<td style="">									
										<a href="/profile">
											<img style="vertical-align:bottom;border:0;" src='/theme/images/icons32/config.png'  alt='Profile' title='Profile' />
										</a>
                            		</td>
             						<td style="">									
										<a href="/logout">
											<img style="vertical-align:bottom;border:0;" src='/theme/images/icons32/logout.png'  alt='Logout (${loggedInUsername})' title='Logout (${loggedInUsername})' />
										</a>
                            		</td>
								</#if>
							</tr>
						</table>
					</td>
					<td>
						<div class="searchbox">
							<input id='searchInput1' type='text' onKeyPress="return submitenter(this,event);" onmaxlength='1024' style='float:left;width:200px;margin:7px;font-size:10px' value="" />
							<img src="/theme/images/icons/search_icon.gif" alt='Go' title='Go' onclick="searchSubmit('searchInput1');" style="float:left;width:15px,height:15px;margin:7px;border-width:0px;cursor:pointer;);"/>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr valign="top">		
		<td width="100%">
			<div class="webtop">
				